<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class confirmation extends Model
{
    protected $primaryKey = 'confid';
    protected $table = 'confirmations';

    public function User() {
        return $this->belongsTo(User::class,'uid','uid');
    }
}
