<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicationSubmission extends Model
{
    protected $primaryKey = 'asid';
    protected $table = 'application_submissions';

    public function Application() {
        return $this->belongsTo(application::class,'aid','aid');
    }
}
