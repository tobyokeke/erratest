<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class secform2B extends Model
{
    protected $primaryKey = 'sf2bid';
    protected $table = 'sec-form-2B';
}
