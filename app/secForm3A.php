<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class secForm3A extends Model
{
    protected $primaryKey = 'sf3aid';
    protected $table = 'sec-form-3A';
}
