<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicationUpdate extends Model
{
    protected $primaryKey = 'auid';
    protected $table = 'application_updates';

    public function Application() {
        return $this->belongsTo(application::class,'aid','aid');
    }

    public function User() {
        return $this->belongsTo(User::class,'uid','uid');
    }
}
