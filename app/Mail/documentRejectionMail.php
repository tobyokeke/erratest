<?php

namespace App\Mail;

use App\applicationDocument;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class documentRejectionMail extends Mailable
{
    use Queueable, SerializesModels;

    public $document;


    public function __construct(applicationDocument $document)
    {
        $this->document = $document;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.documentRejectionMailView')->subject('ATTENTION REQUIRED: APPLICATION #' . $this->document->aid);
    }
}
