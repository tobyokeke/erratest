<?php

namespace App\Mail;

use App\confirmation;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Str;

class emailVerificationMail extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    public $user;
    public $token;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($user)
    {

        $token = Str::random(30);
        while(confirmation::where('token')->count() > 0){
            $token = Str::random(30);
        }

        $confirmation = new confirmation();
        $confirmation->uid = $user->uid;
        $confirmation->email = $user->email;
        $confirmation->token = $token;
        $confirmation->save();

        $this->user = $user;
        $this->token = $token;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.emailVerificationMailView')->onQueue('emails')->subject('VERIFY YOUR EMAIL');
    }
}
