<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class inviteUserMail extends Mailable
{
    use Queueable, SerializesModels;

    public $email;
    public $role;
    public $name;
    public $fname;
    public $sname;
    public $phone;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email,$role,$name,$fname,$sname,$phone)
    {
        $this->email = $email;
        $this->role = $role;
        $this->name = $name;
        $this->sname = $sname;
        $this->fname = $fname;
        $this->phone = $phone;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.inviteUserMailView')->subject('INVITATION TO REGISTER - SEC');
    }
}
