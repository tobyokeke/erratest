<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class payment extends Model
{
    protected $primaryKey = 'pid';
    protected $table = 'payments';

    public function Application() {
        return $this->belongsTo(application::class,'aid','aid');
    }

    public function User() {
        return $this->belongsTo(User::class,'uid','uid');
    }
}
