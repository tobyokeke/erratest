<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class userCompany extends Model
{
    protected $primaryKey = 'ucid';
    protected $table = 'user_companies';

    public function User() {
        return $this->belongsTo(User::class,'uid','uid');
    }

    public function Company() {
        return $this->belongsTo(company::class,'cid','cid');
    }
}
