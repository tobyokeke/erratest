<?php

namespace App\Http\Controllers;

use App\application;
use App\applicationDocument;
use App\applicationUpdate;
use App\funct;
use App\functionForm;
use App\Mail\documentRejectionMail;
use App\payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;

class DeskOfficerController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }


    public function dashboard() {

        $applications = application::orderBy('created_at','desc')->where('assignedTo',auth()->user()->uid)->get()->take(5);


        //get user info
        $allUsers = User::all()->count();
        $usersToday = User::where('created_at','>',Carbon::today())->count();
        $usersThisWeek = User::where('created_at','>',Carbon::now()->startOfWeek())->count();
        $usersThisMonth = User::where('created_at','>',Carbon::now()->startOfMonth())->count();

        //payment info
        $allPayments = payment::all()->where('status','paid')->sum('amount');
        $paymentsToday = payment::where('created_at','>',Carbon::today())->where('status','paid')->sum('amount');
        $paymentsThisWeek = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfWeek())->sum('amount');
        $paymentsThisMonth = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfMonth())->sum('amount');

        //assigned applications
        $allApplications = application::where('assignedTo',auth()->user()->uid)->count();
        $applicationsToday = application::where('assignedTo',auth()->user()->uid)->where('created_at','>',Carbon::today())->count();
        $applicationsThisWeek = application::where('assignedTo',auth()->user()->uid)->where('created_at','>',Carbon::now()->startOfWeek())->count();
        $applicationsThisMonth = application::where('assignedTo',auth()->user()->uid)->where('created_at','>',Carbon::now()->startOfMonth())->count();

        //approved applications
        $approvedApplications = application::where('status','approved')->where('assignedTo',auth()->user()->uid)->count();
        $rejectedApplications = application::where('status','rejected')->where('assignedTo',auth()->user()->uid)->count();

        //functions
        $allFunctions = funct::all()->count();
        $functionsToday = funct::where('created_at','>',Carbon::today())->count();
        $functionsThisWeek = funct::where('created_at','>',Carbon::now()->startOfWeek())->count();
        $functionsThisMonth = funct::where('created_at','>',Carbon::now()->startOfMonth())->count();


        return view('deskofficer.dashboard',[
            // user info
            'applications' => $applications,

            'allUsers' => $allUsers,
            'usersToday' => $usersToday,
            'usersThisWeek' => $usersThisWeek,
            'usersThisMonth' => $usersThisMonth,

            //payment info

            'allPayments' => $allPayments,
            'paymentsToday' => $paymentsToday,
            'paymentsThisWeek' => $paymentsThisWeek,
            'paymentsThisMonth' => $paymentsThisMonth,

            //applications info

            'allApplications' => $allApplications,
            'applicationsToday' => $applicationsToday,
            'applicationsThisWeek' => $applicationsThisWeek,
            'applicationsThisMonth' => $applicationsThisMonth,

            'approvedApplications' => $approvedApplications,
            'rejectedApplications' => $rejectedApplications,

             //functions info

            'allFunctions' => $allFunctions,
            'functionsToday' => $functionsToday,
            'functionsThisWeek' => $functionsThisWeek,
            'functionsThisMonth' => $functionsThisMonth,


        ]);
    }


    public function payments() {
        $payments = payment::orderBy('created_at','desc')->paginate(10);
        return view('deskofficer.payments.manage',[
            'payments' => $payments
        ]);
    }

    public function applications() {
        $applications = application::where('assignedTo',auth()->user()->uid)->orderBy('created_at','desc')->paginate(20);
        return view('deskofficer.applications.manage',[
            'applications' => $applications
        ]);
    }

    public function applicationDetails(application $application) {
        return view('deskofficer.applications.details',[
            'application' => $application
        ]);
    }

    public function applicationUpdates(application $application) {

        $updates = applicationUpdate::where('aid',$application->aid)->get();
        return view('deskofficer.updates.details',[
            'application' => $application,
             'updates' => $updates
        ]);
    }

    public function postApplicationUpdate(Request $request, application $application) {


        $update = new applicationUpdate();
        $update->aid = $application->aid;
        $update->uid = auth()->user()->uid;
        $update->update = $request->input('update');
        $update->save();

        session()->flash('success','Updated Posted.');
        return redirect()->back();

    }

    public function postReviewDocument(Request $request) {

        $adid = $request->input('adid');
        $status = $request->input('status');

        $document = applicationDocument::find($adid);
        $document->status = $status;
        $document->date_do_vetted = Carbon::now();
        $document->review_message = $request->input('review_message');
        $document->save();

        $application = application::find($document->aid);
        $application->status = 'pending';
        $application->save();

        if($status == 'rejected'){
            Mail::to($document->Application->User->email)->send(new documentRejectionMail($document));
        }

        session()->flash('success','Document Reviewed.');
        return redirect()->back();
    }


    public function interviews() {
        return view('deskofficer.interviews.manage');
    }

}
