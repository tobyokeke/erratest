<?php

namespace App\Http\Controllers;

use App\confirmation;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PublicController extends Controller
{
    public function verifyEmail($email, $token) {

        if(User::where('email',$email)->count() <= 0 ){
            session()->flash('error','User Does not exist');
            return view('auth.verify');
        }

        $user = User::where('email',$email)->first();
        if($user->isEmailVerified == 1){

            session()->flash('error','User Does not exist');
            return view('auth.verify');

        }


        DB::beginTransaction();
        $confirmation = confirmation::where('email',$email)->where('token',$token)->first();

        $confirmation->delete();

        $user = User::where('email',$email)->first();
        $user->isEmailVerified = 1;
        $user->emailVerifiedAt = Carbon::now();
        $user->save();

        DB::commit();


        auth()->login($user);
        session()->flash('success','Email Verified.');

        return redirect('dashboard');



    }
}
