<?php

namespace App\Http\Controllers;

use App\application;
use App\applicationUpdate;
use App\Mail\assignmentNotificationMail;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Mail;


class DhController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function dashboard() {
        $applications = application::orderBy('created_at','desc')->where('status','submitted')->get()->take(5);

        $allApplications = application::all()->where('status','submitted')->count();
        $applicationsToday = application::all()->where('status','submitted')->where('created_at','>',Carbon::today())->count();
        $applicationsThisWeek = application::all()->where('status','submitted')->where('created_at','>',Carbon::now()->startOfWeek())->count();
        $applicationsThisMonth = application::all()->where('status','submitted')->where('created_at','>',Carbon::now()->startOfMonth())->count();

        $allUnassignedApplications = application::where('status','submitted')->where('assignedTo',null)->count();
        $unassignedApplicationsToday = application::where('status','submitted')->where('assignedTo',null)->where('created_at','>',Carbon::today())->count();
        $unassignedApplicationsThisWeek = application::where('status','submitted')->where('created_at','>',Carbon::now()->startOfWeek())->count();
        $unassignedApplicationsThisMonth = application::where('status','submitted')->where('assignedTo',null)->where('created_at','>',Carbon::now()->startOfMonth())->count();


        return view('dh.dashboard',[
            'applications' => $applications,
            'allApplications' => $allApplications,
            'applicationsToday' => $applicationsToday,
            'applicationsThisWeek' => $applicationsThisWeek,
            'applicationsThisMonth' => $applicationsThisMonth,

            'allUnassignedApplications' => $allUnassignedApplications,
            'unassignedApplicationsToday' => $unassignedApplicationsToday,
            'unassignedApplicationsThisWeek' => $unassignedApplicationsThisWeek,
            'unassignedApplicationsThisMonth' => $unassignedApplicationsThisMonth,

        ]);

    }

    public function applications() {
        $allApplications = application::paginate(20);

        return view('dh.applications.manage',[
            'applications' => $allApplications
        ]);
    }

    public function applicationDetails(application $application) {
        $users = User::where('role','deskofficer')->get();
        return view('dh.applications.details',[
            'application' => $application,
            'users' => $users
        ]);
    }

    public function applicationUpdates(application $application) {
        $updates = applicationUpdate::where('aid',$application->aid)->get();
        return view('dh.updates.details',[
            'application' => $application,
            'updates' => $updates
        ]);
    }

    public function postAssignApplication(Request $request,application $application) {
        $uid = $request->input('uid');

        $application->assignedTo = $uid;
        $application->save();

//        try{
            Mail::to($application->AssignedTo->email)->send(new assignmentNotificationMail($application));
//        }catch (\Exception $exception){}


        session()->flash('success','Application Assigned.');
        return redirect()->back();
    }

    public function updates() {
        $updates = applicationUpdate::orderBy('created_at','desc')->paginate(20);
        return view('dh.updates.manage',[
            'updates' => $updates
        ]);
    }


}
