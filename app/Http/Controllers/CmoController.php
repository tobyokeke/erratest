<?php

namespace App\Http\Controllers;

use App\application;
use App\applicationDocument;
use App\applicationFunction;
use App\applicationSubmission;
use App\company;
use App\companyFunction;
use App\funct;
use App\Mail\hodNotificationMail;
use App\Mail\inviteUserMail;
use App\payment;
use App\secform2;
use App\secform2A;
use App\secform2B;
use App\secform2D;
use App\User;
use App\userCompany;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CmoController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function dashboard() {

        if(empty(auth()->user()->lastLogin)) return redirect()->route('cmo.applications.start');

        $applications = application::orderBy('created_at','desc')->where('uid',auth()->user()->uid)->get()->take(5);

        $allApplications = application::where('uid',auth()->user()->uid)->count();
        $applicationsToday = application::where('uid',auth()->user()->uid)->where('created_at','>',Carbon::today())->count();
        $applicationsThisWeek = application::where('uid',auth()->user()->uid)->where('created_at','>',Carbon::now()->startOfWeek())->count();
        $applicationsThisMonth = application::where('uid',auth()->user()->uid)->where('created_at','>',Carbon::now()->startOfMonth())->count();

        $payments = payment::where('uid',auth()->user()->uid)->where('status','paid')->sum('amount');
        $paymentsToday = payment::where('uid',auth()->user()->uid)->where('created_at','>',Carbon::today())->where('status','paid')->sum('amount');
        $paymentsThisWeek = payment::where('uid',auth()->user()->uid)->where('status','paid')->where('created_at','>',Carbon::now()->startOfWeek())->sum('amount');
        $paymentsThisMonth = payment::where('uid',auth()->user()->uid)->where('status','paid')->where('created_at','>',Carbon::now()->startOfMonth())->sum('amount');

        return view('cmo.dashboard',[
            'applications' => $applications,
            'allApplications' => $allApplications,
            'applicationsToday' => $applicationsToday,
            'applicationsThisWeek' => $applicationsThisWeek,
            'applicationsThisMonth' => $applicationsThisMonth,

            'payments' => $payments,
            'paymentsToday' => $paymentsToday,
            'paymentsThisWeek' => $paymentsThisWeek,
            'paymentsThisMonth' => $paymentsThisMonth


        ]);

    }

    public function startApplication() {
        $functions = funct::all();
        return view('cmo.applications.start',[
            'functions' => $functions
        ]);
    }

    public function applications() {
        $applications = application::where('uid',auth()->user()->uid)->paginate(20);
        return view('cmo.applications.manage',[
            'applications' => $applications
        ]);
    }

    public function applicationDetails(application $application) {
        $totalDocumentCount = 0;
        $filledCount = 0;

        foreach($application->Functions as $function){
            foreach($function->Forms as $form){
                $totalDocumentCount++;
                if(
                    applicationSubmission::where('aid',$application->aid)->where('fid',$function->fid)->where('uid',auth()->user()->uid)->where('name',$form->name)->count() >  0
                ){ $filledCount++;}
            }
        }

        if($filledCount == $totalDocumentCount && $filledCount > 0){
            $showSubmit = true;
        } else $showSubmit = false;

        return view('cmo.applications.details',[
            'application' => $application,
            'showSubmit' => $showSubmit
        ]);
    }

    public function paymentDetails(payment $payment) {
        return view('cmo.payments.details',[
            'payment' => $payment
        ]);
    }

    public function submit(application $application) {
        $application->status = 'submitted';
        $application->save();

        $hods = User::where('role','hod')->get();

        if(empty($application->assignedTo)){ // only notify if the application hasn't been previously assigned
            foreach($hods as $hod){
                $this->sendSms($hod->phone,"A new application has been submitted. The ID is $application->aid");
                Mail::to($hod->email)->send(new hodNotificationMail($application));
            }
        }


        session()->flash('success','Submitted To SEC');
        return redirect()->back();
    }

    public function applications1()
    {
        return view('cmo.application1');
    }

    public function applications2()
    {
        return view('cmo.application2');
    }

    public function secForm1()
    {
        return view('cmo.sec-form-1');
    }

//    public function secForm2()
//    {
//        /**
//         * Type is to take the type of submission which is application or transfer
//         */
//        $type = Input::get('type');
//        $function = Input::get('function');
//        $application = Input::get('application');
//
//        $application =  application::find($application);
//
//        $function = funct::find($function);
//
//        return view('cmo.sec-form-2',[
//            'type' => $type,
//            'function' => $function,
//            'application' => $application
//        ]);
//
//    }

//    public function postSecForm2(Request $request){
//
//        $aid  = $request->input('aid');
//        $fid  = $request->input('fid');
//        $type = $request->input('type'); // type would be application
//        $name = 'SEC FORM 2';
//
//
//        $secform2 = new secform2();
//        $secform2->capital_market = $request->input('capital_market');
//        $secform2->surname = $request->input('surname');
//        $secform2->othernames = $request->input('othernames');
//        $secform2->alias = $request->input('alias');
//        $secform2->residential_address = $request->input('residential_address');
//        $secform2->office_address = $request->input('office_address');
//        $secform2->official_position = $request->input('official_position');
//        $secform2->employment = $request->input('employment');
//        $secform2->email = $request->input('email');
//        $secform2->dob = $request->input('dob');
//        $secform2->pob = $request->input('pob');
//        $secform2->sob = $request->input('sob');
//        $secform2->nationality = $request->input('nationality');
//        $secform2->soo = $request->input('soo');
//        $secform2->number = $request->input('number');
//        $secform2->sex = $request->input('sex');
//        $secform2->marital_status = $request->input('marital_status');
//        $secform2->visible_mark = $request->input('visible_mark');
//        $secform2->license_no = $request->input('license_no');
//        $secform2->fresh = $request->input('fresh');
//        $secform2->poi = $request->input('poi');
//        $secform2->doi = $request->input('doi');
//        $secform2->doe = $request->input('doe');
//        $secform2->father_name = $request->input('father_name');
//        $secform2->mother_name = $request->input('mother_name');
//        $secform2->maiden_name = $request->input('maiden_name');
//        $secform2->spouse_name = $request->input('spouse_name');
//        $secform2->employment_spouse = $request->input('employment_spouse');
//        $secform2->name_employer = $request->input('name_employer');
//        $secform2->employer_address = $request->input('employer_address');
//        $secform2->banker_name = $request->input('banker_name');
//        $secform2->banker_address = $request->input('banker_address');
//        $secform2->branch = $request->input('branch');
//        $secform2->account_type = $request->input('account_type');
//        $secform2->account_number = $request->input('account_number');
//        $secform2->date_op = $request->input('date_op');
//        $secform2->name_of_employer = $request->input('name_of_employer');
//        $secform2->emp_address = $request->input('emp_address');
//        $secform2->date_from = $request->input('date_from');
//        $secform2->date_to = $request->input('date_to');
//        $secform2->part_full = $request->input('part_full');
//        $secform2->position_held = $request->input('position_held');
//        $secform2->pre_name_employer = $request->input('pre_name_employer');
//        $secform2->pre_address = $request->input('pre_address');
//        $secform2->pre_date_from = $request->input('pre_date_from');
//        $secform2->pre_date_to = $request->input('pre_date_to');
//        $secform2->pre_part_full = $request->input('pre_part_full');
//        $secform2->pre_position_held = $request->input('pre_position_held');
//        $secform2->pre_reasons_leaving = $request->input('pre_reasons_leaving');
//        $secform2->edu_institution = $request->input('edu_institution');
//        $secform2->edu_address = $request->input('edu_address');
//        $secform2->edu_date_from = $request->input('edu_date_from');
//        $secform2->edu_date_to = $request->input('edu_date_to');
//        $secform2->edu_course_study = $request->input('edu_course_study');
//        $secform2->edu_qualification_obtained = $request->input('edu_qualification_obtained');
//        $secform2->awarding_institute = $request->input('awarding_institute');
//        $secform2->certificate = $request->input('certificate');
//        $secform2->year = $request->input('year');
//        $secform2->bro_date_exam = $request->input('bro_date_exam',Carbon::now());
//        $secform2->subject = $request->input('subject');
//        $secform2->test_scores = $request->input('test_scores');
//        $secform2->ref_name = $request->input('ref_name');
//        $secform2->ref_address = $request->input('ref_address');
//        $secform2->ref_occupation = $request->input('ref_occupation');
//
//
//
//
//
//        // checkbox fields
//
//        $registration = $request->input('registration');
//        $registration2 = $request->input('registration2');
//        $registration3 = $request->input('registration3');
//        $registration4 = $request->input('registration4');
//        $change_of_name = $request->input('change_of_name');
//        $offences1 = $request->input('offences1');
//        $offences2 = $request->input('offences2');
//        $judgement = $request->input('judgement');
//        $surety = $request->input('surety');
//        $investment_adviser = $request->input('investment_adviser');
//        $number14a = $request->input('number14a');
//        $number14b = $request->input('number14b');
//        $number15 = $request->input('number15');
//        $number16 = $request->input('number16');
//
//        $secform2->registration = $this->checkboxValue($registration);
//        $secform2->registration2 = $this->checkboxValue($registration2);
//        $secform2->registration3 = $this->checkboxValue($registration3);
//        $secform2->registration4 = $this->checkboxValue($registration4);
//        $secform2->change_of_name = $this->checkboxValue($change_of_name);
//        $secform2->offences1 = $this->checkboxValue($offences1);
//        $secform2->offences2 = $this->checkboxValue($offences2);
//        $secform2->judgement = $this->checkboxValue($judgement);
//        $secform2->surety = $this->checkboxValue($surety);
//        $secform2->investment_adviser = $this->checkboxValue($investment_adviser);
//        $secform2->number14a = $this->checkboxValue($number14a);
//        $secform2->number14b = $this->checkboxValue($number14b);
//        $secform2->number15 = $this->checkboxValue($number15);
//        $secform2->number16 = $this->checkboxValue($number16);
//
//        $secform2->registration_details = $request->input('registration_details');
//        $secform2->registration_details2 = $request->input('registration_details2');
//        $secform2->registration_details3 = $request->input('registration_details3');
//        $secform2->registration_details4 = $request->input('registration_details4');
//        $secform2->change_of_name_details = $request->input('change_of_name_details');
//        $secform2->offences1_details = $request->input('offences1_details');
//        $secform2->offences2_details = $request->input('offences2_details');
//
//        $secform2->judgement_copy = $request->input('judgement_copy');
//        $secform2->judgement_details = $request->input('judgement_details');
//
//        $secform2->surety_details = $request->input('surety_details');
//        $secform2->business_activities1 = $request->input('business_activities1');
//        $secform2->business_activities2 = $request->input('business_activities2');
//        $secform2->business_activities3 = $request->input('business_activities3');
//        $secform2->investment_adviser_details = $request->input('investment_adviser_details');
//
//        $secform2->number14a_details = $request->input('number14a_details');
//
//        $secform2->number14b_details = $request->input('number14b_details');
//
//        $secform2->number15_details = $request->input('number15_details');
//
//        $secform2->number16_details = $request->input('number16_details');
//        $secform2->affidavit_name = $request->input('affidavit_name');
//        $secform2->affidavit_of = $request->input('affidavit_of', false);
//        $secform2->first_input = $request->input('first_input');
//        $secform2->second_input = $request->input('second_input');
//        $secform2->third_input = $request->input('third_input');
//        $secform2->fourth_input = $request->input('fourth_input');
//        $secform2->fifth_input = $request->input('fifth_input');
//        $secform2->despondent_sig = $request->input('despondent_sig');
//        $secform2->sponsoring_firm = $request->input('sponsoring_firm');
//        $secform2->applicant_sig = $request->input('applicant_sig');
//        $secform2->director_sig = $request->input('director_sig');
//
//        $secform2->save();
//
//        // associate this submission with an application if its for
//        // an application
//
//        if($type == 'application'){
//            $appliationSubmission = new applicationSubmission();
//            $appliationSubmission->aid = $aid;
//            $appliationSubmission->fid = $fid;
//            $appliationSubmission->uid = auth()->user()->uid;
//            $appliationSubmission->id = $secform2->sf2id;
//            $appliationSubmission->name = $name;
//            $appliationSubmission->save();
//        }
//
//        $request->session()->flash('success','Form Submitted');
//
//        return redirect()->route('cmo.application.details',['application' => $aid]);
//
//
//
//    }


    public function checkboxValue($data)
    {
        if($data == 'on') $data = 'yes'; else $data = 'no';

        return $data;
    }

//    public function secForm2A()
//    {
//        /**
//         * Type is to take the type of submission which is application or transfer
//         */
//        $type = Input::get('type');
//        $function = Input::get('function');
//        $application = Input::get('application');
//
//        $application =  application::find($application);
//
//        $function = funct::find($function);
//
//        return view('cmo.sec-form-2A',[
//            'type' => $type,
//            'function' => $function,
//            'application' => $application
//        ]);
//
//    }


    public function postSecForm2A(Request $request){


        $aid  = $request->input('aid');
        $fid  = $request->input('fid');
        $type = $request->input('type'); // type would be application
        $name = 'SEC FORM 2A';



        $secform2A = new secform2A();
        $secform2A->applicant_company = $request->input('applicant_company');
        $secform2A->applicant_company_address = $request->input('applicant_company_address');
        $secform2A->officer_replaced_name = $request->input('officer_replaced_name');
        $secform2A->current_reg = $request->input('current_reg');
        $secform2A->officer_replaced_period_service = $request->input('officer_replaced_period_service');
        $secform2A->reasons_for_leaving = $request->input('reasons_for_leaving');
        $secform2A->comment_on_integrity = $request->input('comment_on_integrity');
        $secform2A->sub_officer_name = $request->input('sub_officer_name');
        $secform2A->reg_sought = $request->input('reg_sought');
        $secform2A->sub_officer_period_service = $request->input('sub_officer_period_service');



        // checkbox fields

        $form_sec2_2b_submitted = $request->input('form_sec2_2b_submitted');
        $secform2A->form_sec2_2b_submitted = $this->checkboxValue($form_sec2_2b_submitted);

        $secform2A->sig = $request->input('sig');
        $secform2A->designation = $request->input('designation');
        $secform2A->director_name = $request->input('director_name');
        $secform2A->director_sig = $request->input('director_sig');
        $secform2A->secretary_name = $request->input('secretary_name');
        $secform2A->sec_sig = $request->input('sec_sig');
        $secform2A->company_seal = $request->input('company_seal');
        $secform2A->affidavit_name = $request->input('affidavit_name');
        $secform2A->dir_sec = $request->input('dir_sec');
        $secform2A->affidavit_of = $request->input('affidavit_of', false);
        $secform2A->first_input = $request->input('first_input');
        $secform2A->second_input = $request->input('second_input');
        $secform2A->third_input = $request->input('third_input');
        $secform2A->fourth_input = $request->input('fourth_input');
        $secform2A->fifth_input = $request->input('fifth_input');
        $secform2A->despondent_sig = $request->input('despondent_sig');



        $secform2A->save();

        $request->session()->flash('success','Form Submitted');

        // associate this submission with an application if its for
        // an application

        if($type == 'application'){
            $appliationSubmission = new applicationSubmission();
            $appliationSubmission->aid = $aid;
            $appliationSubmission->fid = $fid;
            $appliationSubmission->uid = auth()->user()->uid;
            $appliationSubmission->id = $secform2A->sf2aid;
            $appliationSubmission->name = $name;
            $appliationSubmission->save();
        }

        $request->session()->flash('success','Form Submitted');

        return redirect()->route('cmo.application.details',['application' => $aid]);



    }

//
//    public function secForm2B()
//    {
//        /**
//         * Type is to take the type of submission which is application or transfer
//         */
//        $type = Input::get('type');
//        $function = Input::get('function');
//        $application = Input::get('application');
//
//        $application =  application::find($application);
//
//        $function = funct::find($function);
//
//        return view('cmo.sec-form-2B',[
//            'type' => $type,
//            'function' => $function,
//            'application' => $application
//        ]);
//
//    }
//
//    public function postSecForm2B(Request $request){
//
//
//
//        $aid  = $request->input('aid');
//        $fid  = $request->input('fid');
//        $type = $request->input('type'); // type would be application
//        $name = 'SEC FORM 2B';
//
//
//        $secform2B = new secform2B();
//        $secform2B->applicant_name = $request->input('applicant_name');
//        $secform2B->applicant_reg_as = $request->input('applicant_reg_as');
//        $secform2B->res_address = $request->input('res_address');
//        $secform2B->employer_name = $request->input('employer_name');
//        $secform2B->bus_address = $request->input('bus_address');
//
//
//
//        $applying_for = $request->input('applying_for');
//        $secform2B->applying_for = $this->checkboxValue2($applying_for);
//
//        $secform2B->trans_previous_employer = $request->input('trans_previous_employer');
//        $secform2B->date_of_resignation = $request->input('date_of_resignation');
//        $secform2B->resig_letter = $request->input('resig_letter');
//        $secform2B->reg_sought = $request->input('reg_sought');
//        $secform2B->sub_officer_period_service = $request->input('sub_officer_period_service');
//        $secform2B->applicable_exam = $request->input('applicable_exam');
//        $secform2B->date_passed = $request->input('date_passed');
//        $secform2B->proof_passing = $request->input('proof_passing', false);
//        $secform2B->date_signed = $request->input('date_signed');
//        $secform2B->applicant_sig = $request->input('applicant_sig');
//
//
//        $secform2B->director_name = $request->input('director_name');
//        $secform2B->dir_sig = $request->input('dir_sig');
//        $secform2B->affidavit_name = $request->input('affidavit_name');
//        $secform2B->affidavit_of = $request->input('affidavit_of', false);
//        $secform2B->first_input = $request->input('first_input');
//        $secform2B->second_input = $request->input('second_input');
//        $secform2B->third_input = $request->input('third_input');
//        $secform2B->fourth_input = $request->input('fourth_input');
//        $secform2B->fifth_input = $request->input('fifth_input');
//        $secform2B->despondent_sig = $request->input('despondent_sig');
//
//
//        // checkbox fields
//
//        $refusal = $request->input('refusal');
//        $secform2B->refusal = $this->checkboxValue($refusal);
//
//
//        $change_of_name = $request->input('change_of_name');
//        $secform2B->change_of_name = $this->checkboxValue($change_of_name);
//
//
//        $offences = $request->input('offences');
//        $secform2B->offences = $this->checkboxValue($offences);
//
//
//        $civil_procedures = $request->input('civil_procedures');
//        $secform2B->civil_procedures = $this->checkboxValue($civil_procedures);
//
//
//        $bankruptcy = $request->input('bankruptcy');
//        $secform2B->bankruptcy = $this->checkboxValue($bankruptcy);
//
//
//        $judgement = $request->input('judgement');
//        $secform2B->judgement = $this->checkboxValue($judgement);
//
//
//        $surety = $request->input('surety');
//        $secform2B->surety = $this->checkboxValue($surety);
//
//
//
//        $business_activities = $request->input('business_activities');
//        $secform2B->business_activities = $this->checkboxValue($business_activities);
//
//
//
//        $secform2B->save();
//
//        $request->session()->flash('success','Form Submitted');
//
//        // associate this submission with an application if its for
//        // an application
//
//        if($type == 'application'){
//            $appliationSubmission = new applicationSubmission();
//            $appliationSubmission->aid = $aid;
//            $appliationSubmission->fid = $fid;
//            $appliationSubmission->uid = auth()->user()->uid;
//            $appliationSubmission->id = $secform2B->sf2bid;
//            $appliationSubmission->name = $name;
//            $appliationSubmission->save();
//        }
//
//        $request->session()->flash('success','Form Submitted');
//
//        return redirect()->route('cmo.application.details',['application' => $aid]);
//
//    }
//
//    public function secForm2C()
//    {
//        /**
//         * Type is to take the type of submission which is application or transfer
//         */
//        $type = Input::get('type');
//        $function = Input::get('function');
//        $application = Input::get('application');
//
//        $application =  application::find($application);
//
//        $function = funct::find($function);
//
//        return view('cmo.sec-form-2C',[
//            'type' => $type,
//            'function' => $function,
//            'application' => $application
//        ]);
//
//    }
//
//    public function secForm2D()
//    {
//        /**
//         * Type is to take the type of submission which is application or transfer
//         */
//        $type = Input::get('type');
//        $function = Input::get('function');
//        $application = Input::get('application');
//
//        $application =  application::find($application);
//
//        $function = funct::find($function);
//
//        return view('cmo.sec-form-2D',[
//            'type' => $type,
//            'function' => $function,
//            'application' => $application
//        ]);
//
//    }
//
//    public function postSecForm2D(Request $request){
//
//
//        $aid  = $request->input('aid');
//        $fid  = $request->input('fid');
//        $type = $request->input('type'); // type would be application
//        $name = 'SEC FORM 2D';
//
//
//
//        $secform2D = new secform2D();
//        $secform2D->name = $request->input('name');
//        $secform2D->sponsoring_company = $request->input('sponsoring_company');
//        $secform2D->res_address = $request->input('res_address');
//        $secform2D->perm_address = $request->input('perm_address');
//        $secform2D->mail_address = $request->input('mail_address');
//        $secform2D->official_email = $request->input('official_email');
//        $secform2D->personal_email = $request->input('personal_email');
//        $secform2D->gsm_number = $request->input('gsm_number');
//        $secform2D->landline_number = $request->input('landline_number');
//        $secform2D->dob = $request->input('dob');
//        $secform2D->pob = $request->input('pob');
//        $secform2D->sob = $request->input('sob',false);
//        $secform2D->nationality = $request->input('nationality');
//        $secform2D->sex = $request->input('sex');
//        $secform2D->marital_status = $request->input('marital_status');
//        $secform2D->next_kin_name = $request->input('next_kin_name');
//        $secform2D->next_kin_number = $request->input('next_kin_number');
//        $secform2D->next_kin_address = $request->input('next_kin_address');
//        $secform2D->next_kin_relationship = $request->input('next_kin_relationship');
//        $secform2D->next_kin_other = $request->input('next_kin_other');
//
//
//
//        // checkbox fields
//
//        $q1 = $request->input('q1');
//        $q2 = $request->input('q2');
//        $q3 = $request->input('q3');
//        $q4 = $request->input('q4');
//        $q5 = $request->input('q5');
//        $q6 = $request->input('q6');
//        $q7 = $request->input('q7');
//        $q8 = $request->input('q8');
//        $q9 = $request->input('q9');
//        $q10 = $request->input('q10');
//        $q11 = $request->input('q11');
//        $q12 = $request->input('q12');
//        $q13 = $request->input('q13');
//        $q14 = $request->input('q14');
//        $q15 = $request->input('q15');
//        $q16 = $request->input('q16');
//        $q17 = $request->input('q17');
//        $q18 = $request->input('q18');
//        $q19 = $request->input('q19');
//        $q20 = $request->input('q20');
//
//        $secform2D->q1 = $this->checkboxValue($q1);
//        $secform2D->q2 = $this->checkboxValue($q2);
//        $secform2D->q3 = $this->checkboxValue($q3);
//        $secform2D->q4 = $this->checkboxValue($q4);
//        $secform2D->q5 = $this->checkboxValue($q5);
//        $secform2D->q6 = $this->checkboxValue($q6);
//        $secform2D->q7 = $this->checkboxValue($q7);
//        $secform2D->q8 = $this->checkboxValue($q8);
//        $secform2D->q9 = $this->checkboxValue($q9);
//        $secform2D->q10 = $this->checkboxValue($q10);
//        $secform2D->q11 = $this->checkboxValue($q11);
//        $secform2D->q12 = $this->checkboxValue($q12);
//        $secform2D->q13 = $this->checkboxValue($q13);
//        $secform2D->q14 = $this->checkboxValue($q14);
//        $secform2D->q15 = $this->checkboxValue($q15);
//        $secform2D->q16 = $this->checkboxValue($q16);
//        $secform2D->q17 = $this->checkboxValue($q17);
//        $secform2D->q18 = $this->checkboxValue($q18);
//        $secform2D->q19 = $this->checkboxValue($q19);
//        $secform2D->q20 = $this->checkboxValue($q20);
//
//        $secform2D->q1_details = $request->input('q1_details');
//        $secform2D->q2_details = $request->input('q2_details');
//        $secform2D->q3_details = $request->input('q3_details');
//        $secform2D->q4_details = $request->input('q4_details');
//        $secform2D->q5_details = $request->input('q5_details');
//        $secform2D->q6_details = $request->input('q6_details');
//        $secform2D->q7_details = $request->input('q7_details');
//        $secform2D->q8_details = $request->input('q8_details');
//        $secform2D->q9_details = $request->input('q9_details');
//        $secform2D->q10_details = $request->input('q10_details');
//        $secform2D->q11_details = $request->input('q11_details');
//        $secform2D->q12_details = $request->input('q12_details');
//        $secform2D->q13_details = $request->input('q13_details');
//        $secform2D->q14_details = $request->input('q14_details');
//        $secform2D->q15_details = $request->input('q15_details');
//        $secform2D->q16_details = $request->input('q16_details');
//        $secform2D->q17_details = $request->input('q17_details');
//        $secform2D->q18_details = $request->input('q18_details');
//        $secform2D->q19_details = $request->input('q19_details');
//        $secform2D->q20_details = $request->input('q20_details');
//        $secform2D->affidavit_name = $request->input('affidavit_name');
//        $secform2D->affidavit_address = $request->input('affidavit_address');
//        $secform2D->affidavit_full_name = $request->input('affidavit_full_name');
//        $secform2D->first_input = $request->input('first_input');
//        $secform2D->second_input = $request->input('second_input');
//        $secform2D->third_input = $request->input('third_input');
//        $secform2D->fourth_input = $request->input('fourth_input');
//        $secform2D->fifth_input = $request->input('fifth_input', false);
//        $secform2D->despondent_sig = $request->input('despondent_sig');
//
//
//
//        $secform2D->save();
//
//        $request->session()->flash('success','Form Submitted');
//
//
//        // associate this submission with an application if its for
//        // an application
//
//        if($type == 'application'){
//            $appliationSubmission = new applicationSubmission();
//            $appliationSubmission->aid = $aid;
//            $appliationSubmission->fid = $fid;
//            $appliationSubmission->uid = auth()->user()->uid;
//            $appliationSubmission->id = $secform2D->sf2did;
//            $appliationSubmission->name = $name;
//            $appliationSubmission->save();
//        }
//
//        $request->session()->flash('success','Form Submitted');
//
//        return redirect()->route('cmo.application.details',['application' => $aid]);
//
//    }
//
//
//    public function secForm3()
//    {
//        return view('cmo.sec-form-3');
//    }
//
//    public function secForm3A()
//    {
//        return view('cmo.sec-form-3A');
//    }
//
//    public function secForm3B()
//    {
//        return view('cmo.sec-form-3B');
//    }

    public function pay(Request $request) {
        $uid = $request->input('uid');
        $amount = $request->input('amount');
        $functions = $request->input('functions');

        $reference = "SEC". Str::random('10');

        while(payment::where('reference',$reference)->count() > 0){
            $reference = "SEC". Str::random('10');
        }

        $payment = new payment();
        $payment->uid = $uid;
        $payment->amount = $amount;
        $payment->status = "initiated";
        $payment->reference = strtoupper($reference);
        $payment->functions = json_encode($functions);
        $payment->save();

        return response( array( "message" => "Successful", "data" => $payment  ), 200 );
    }
    public function createApplication(Request $request) {
        $reference = $request->input('reference');

        $payment = payment::where('reference',$reference)->first();


        $application = new application();
        $application->uid = $payment->uid;
        $application->save();

        // associate the payment with this application
        $payment->aid = $application->aid;
        $payment->save();


        $functions = json_decode($payment->functions);

        foreach($functions as $function){
            $applicationFunction = new applicationFunction();
            $applicationFunction->aid = $application->aid;
            $applicationFunction->fid = $function;
            $applicationFunction->save();
        }

        //check if last login is not set, then set it
        // to enable the user see the dashboard

        if(empty(auth()->user()->lastLogin)){
            $user = User::find(auth()->user()->uid);
            $user->lastLogin = $user->created_at;
            $user->save();

        }

        return response( array( "message" => "Successful", "data" => $application  ), 200 );

    }


    public function addCompany() {
        $functions = funct::all();
        return view('cmo.companies.add',[
            'functions' => $functions
        ]);
    }

    public function postAddCompany(Request $request) {
        $functions = $request->input('functions');
        $fileNumber = $request->input('fileNumber');

//        if(company::where('fileNumber',$fileNumber)->count() > 0 ) {
//            session()->flash('error','File number exists');
//            return redirect()->back();
//        }

        $company = new company();
        $company->fileNumber = $fileNumber ;
        $company->operatorName = $request->input('operatorName');
        $company->rcNumber = $request->input('rcNumber');
        $company->cacNumber = $request->input('cacNumber');
        $company->registrationDate = $request->input('registrationDate');
        $company->fidelityBond = $request->input('fidelityBond');
        $company->paidUpCapital = $request->input('paidUpCapital');
        $company->save();

        foreach ($functions as $function){
            $companyFunction = new companyFunction();
            $companyFunction->cid = $company->cid;
            $companyFunction->fid = $function;
            $companyFunction->save();
        }

        $userCompany = new userCompany();
        $userCompany->uid = auth()->user()->uid;
        $userCompany->cid= $company->cid;
        $userCompany->save();

        session()->flash('success','Submitted. Registration in progress.');
        return redirect()->back();
    }

    public function companies() {
        $companies = auth()->user()->Companies;

        return view('cmo.companies.manage',[
                'companies' => $companies
            ]);
    }

    public function companyDetails(company $company) {
        return view('cmo.companies.details',[
            'company' => $company
        ]);
    }

    public function users() {
        return view('cmo.users.manage');
    }

    public function userDetails(User $user) {
        return view('cmo.users.details',[
            'user' => $user
        ]);
    }

    public function addUser() {
        return view('cmo.users.add');
    }

    public function transferUser() {
        return view('cmo.users.transfer');
    }

    public function postAddUser(Request $request) {
        $fname = $request->input('fname');
        $sname = $request->input('sname');
        $name = $request->input('fname') . " " . $request->input('sname');
        $email = $request->input('email');
        $phone = $request->input(' phone');
        $role = $request->input('role');

        $message = "You have been invited to register on SEC portal as a $role by $name Please follow the link below to start the process". '/n/n'. url('/register?email=' . $email . "&fname=$fname&sname=$sname&phone=$phone") ;

        $this->sendSms($phone,$message);
        Mail::to($email)->send(new inviteUserMail($email,$role,auth()->user()->name,$fname,$sname,$phone));

        session()->flash('success','User Invited.');
        return redirect()->back();

    }


    public function sendSms($phone,$message) {
        $message = urlencode($message);
        file_get_contents("https://www.bulksmsnigeria.com/api/v1/sms/create?dnd=4&api_token=VZvbzHIG2KvbNwNq7iQTNf63dq5nxiPk63kS6ORKqaJ1fbt0KrvlU5LGT0o8&from=SECNIGERIA&to=$phone&body=$message");

    }

    public function deleteDocument($adid) {
        $appDocument = applicationDocument::find($adid);

        Storage::disk('public')->delete($appDocument->url);

        $appDocument->delete();

        session()->flash('error','Document Deleted');
        return redirect()->back();
    }

    public function postApplicationDocument(Request $request) {

        $aid = $request->input('aid');
        $fid = $request->input('fid');
        $name = $request->input('name');

        if($request->hasFile('document')){
            $document = $request->file('document');
            $rand = auth()->user()->uid . Str::random(5) . \Carbon\Carbon::now()->timestamp;
            $inputFileName = $document->getClientOriginalName();

            $filepath = 'uploads/documents/' . $rand . $inputFileName;

            Storage::disk('public')->put($filepath, file_get_contents($document));

            $url = asset("storage/" . $filepath);

            $appDocument = new applicationDocument();
            $appDocument->fid = $fid;
            $appDocument->aid = $aid;
            $appDocument->uid = auth()->user()->uid;
            $appDocument->name = $name;
            $appDocument->url = $url;
            $appDocument->save();

            session()->flash('success','Document Added.');
            return redirect()->back();

        } else{
            session()->flash('error','No document attached.');
            return redirect()->back();
        }


    }


}
