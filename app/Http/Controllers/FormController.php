<?php

namespace App\Http\Controllers;

use App\application;
use App\applicationSubmission;
use App\funct;
use App\secform2;
use App\secform2A;
use App\secform2B;
use App\secform2D;
use App\secform3;
use App\secform3A;
use App\secform3B;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Input;

class FormController extends Controller
{
    public function secForm1()
    {
        return view('cmo.sec-form-1');
    }


    public function secForm2()
    {
        /**
         * Type is to take the type of submission which is application or transfer
         */
        $type = Input::get('type');
        $function = Input::get('function');
        $application = Input::get('application');

        $application =  application::find($application);

        $function = funct::find($function);

        return view('cmo.sec-form-2',[
            'type' => $type,
            'function' => $function,
            'application' => $application
        ]);

    }

    public function postSecForm2(Request $request){

        $aid  = $request->input('aid');
        $fid  = $request->input('fid');
        $type = $request->input('type'); // type would be application
        $name = 'SEC FORM 2';



        $secform2 = new secform2();
        $secform2->capital_market = $request->input('capital_market');
        $secform2->surname = $request->input('surname');
        $secform2->othernames = $request->input('othernames');
        $secform2->alias = $request->input('alias');
        $secform2->residential_address = $request->input('residential_address');
        $secform2->office_address = $request->input('office_address');
        $secform2->official_position = $request->input('official_position');
        $secform2->employment = $request->input('employment');
        $secform2->email = $request->input('email');
        $secform2->dob = $request->input('dob');
        $secform2->pob = $request->input('pob');
        $secform2->sob = $request->input('sob');
        $secform2->nationality = $request->input('nationality');
        $secform2->soo = $request->input('soo');
        $secform2->number = $request->input('number');
        $secform2->sex = $request->input('sex');
        $secform2->marital_status = $request->input('marital_status');
        $secform2->visible_mark = $request->input('visible_mark');
        $secform2->license_no = $request->input('license_no');
        $secform2->fresh = $request->input('fresh');
        $secform2->poi = $request->input('poi');
        $secform2->doi = $request->input('doi');
        $secform2->doe = $request->input('doe');
        $secform2->father_name = $request->input('father_name');
        $secform2->mother_name = $request->input('mother_name');
        $secform2->maiden_name = $request->input('maiden_name');
        $secform2->spouse_name = $request->input('spouse_name');
        $secform2->employment_spouse = $request->input('employment_spouse');
        $secform2->name_employer = $request->input('name_employer');
        $secform2->employer_address = $request->input('employer_address');
        $secform2->banker_name = $request->input('banker_name');
        $secform2->banker_address = $request->input('banker_address');
        $secform2->branch = $request->input('branch');
        $secform2->account_type = $request->input('account_type');
        $secform2->account_number = $request->input('account_number');
        $secform2->date_op = $request->input('date_op');
        $secform2->name_of_employer = $request->input('name_of_employer');
        $secform2->emp_address = $request->input('emp_address');
        $secform2->date_from = $request->input('date_from');
        $secform2->date_to = $request->input('date_to');
        $secform2->part_full = $request->input('part_full');
        $secform2->position_held = $request->input('position_held');
        $secform2->pre_name_employer = $request->input('pre_name_employer');
        $secform2->pre_address = $request->input('pre_address');
        $secform2->pre_date_from = $request->input('pre_date_from');
        $secform2->pre_date_to = $request->input('pre_date_to');
        $secform2->pre_part_full = $request->input('pre_part_full');
        $secform2->pre_position_held = $request->input('pre_position_held');
        $secform2->pre_reasons_leaving = $request->input('pre_reasons_leaving');
        $secform2->edu_institution = $request->input('edu_institution');
        $secform2->edu_address = $request->input('edu_address');
        $secform2->edu_date_from = $request->input('edu_date_from');
        $secform2->edu_date_to = $request->input('edu_date_to');
        $secform2->edu_course_study = $request->input('edu_course_study');
        $secform2->edu_qualification_obtained = $request->input('edu_qualification_obtained');
        $secform2->awarding_institute = $request->input('awarding_institute');
        $secform2->certificate = $request->input('certificate');
        $secform2->year = $request->input('year');
        $secform2->bro_date_exam = $request->input('bro_date_exam',Carbon::now());
        $secform2->subject = $request->input('subject');
        $secform2->test_scores = $request->input('test_scores');
        $secform2->ref_name = $request->input('ref_name');
        $secform2->ref_address = $request->input('ref_address');
        $secform2->ref_occupation = $request->input('ref_occupation');





        // checkbox fields

        $registration = $request->input('registration');
        $registration2 = $request->input('registration2');
        $registration3 = $request->input('registration3');
        $registration4 = $request->input('registration4');
        $change_of_name = $request->input('change_of_name');
        $offences1 = $request->input('offences1');
        $offences2 = $request->input('offences2');
        $judgement = $request->input('judgement');
        $surety = $request->input('surety');
        $investment_adviser = $request->input('investment_adviser');
        $number14a = $request->input('number14a');
        $number14b = $request->input('number14b');
        $number15 = $request->input('number15');
        $number16 = $request->input('number16');

        $secform2->registration = $this->checkboxValue($registration);
        $secform2->registration2 = $this->checkboxValue($registration2);
        $secform2->registration3 = $this->checkboxValue($registration3);
        $secform2->registration4 = $this->checkboxValue($registration4);
        $secform2->change_of_name = $this->checkboxValue($change_of_name);
        $secform2->offences1 = $this->checkboxValue($offences1);
        $secform2->offences2 = $this->checkboxValue($offences2);
        $secform2->judgement = $this->checkboxValue($judgement);
        $secform2->surety = $this->checkboxValue($surety);
        $secform2->investment_adviser = $this->checkboxValue($investment_adviser);
        $secform2->number14a = $this->checkboxValue($number14a);
        $secform2->number14b = $this->checkboxValue($number14b);
        $secform2->number15 = $this->checkboxValue($number15);
        $secform2->number16 = $this->checkboxValue($number16);

        $secform2->registration_details = $request->input('registration_details');
        $secform2->registration_details2 = $request->input('registration_details2');
        $secform2->registration_details3 = $request->input('registration_details3');
        $secform2->registration_details4 = $request->input('registration_details4');
        $secform2->change_of_name_details = $request->input('change_of_name_details');
        $secform2->offences1_details = $request->input('offences1_details');
        $secform2->offences2_details = $request->input('offences2_details');

        $secform2->judgement_copy = $request->input('judgement_copy');
        $secform2->judgement_details = $request->input('judgement_details');

        $secform2->surety_details = $request->input('surety_details');
        $secform2->business_activities1 = $request->input('business_activities1');
        $secform2->business_activities2 = $request->input('business_activities2');
        $secform2->business_activities3 = $request->input('business_activities3');
        $secform2->investment_adviser_details = $request->input('investment_adviser_details');

        $secform2->number14a_details = $request->input('number14a_details');

        $secform2->number14b_details = $request->input('number14b_details');

        $secform2->number15_details = $request->input('number15_details');

        $secform2->number16_details = $request->input('number16_details');
        $secform2->affidavit_name = $request->input('affidavit_name');
        $secform2->affidavit_of = $request->input('affidavit_of');
        $secform2->first_input = $request->input('first_input');
        $secform2->second_input = $request->input('second_input');
        $secform2->third_input = $request->input('third_input');
        $secform2->fourth_input = $request->input('fourth_input');
        $secform2->fifth_input = $request->input('fifth_input');
        $secform2->despondent_sig = $request->input('despondent_sig');
        $secform2->sponsoring_firm = $request->input('sponsoring_firm');
        $secform2->applicant_sig = $request->input('applicant_sig');
        $secform2->director_sig = $request->input('director_sig');

        $secform2->save();

        // associate this submission with an application if its for
        // an application

        if($type == 'application'){
            $appliationSubmission = new applicationSubmission();
            $appliationSubmission->aid = $aid;
            $appliationSubmission->fid = $fid;
            $appliationSubmission->uid = auth()->user()->uid;
            $appliationSubmission->id = $secform2->sf2id;
            $appliationSubmission->name = $name;
            $appliationSubmission->save();
        }

        $request->session()->flash('success','Form Submitted');

        return redirect()->route('cmo.application.details',['application' => $aid]);



        //   }catch (\Exception $exception){

        //        $request->session()->flash('error','Sorry an error occurred. Please try again');
        //        return redirect( 'cmo.sec-form-2');

        //   }


    }

    public function checkboxValue($data)
    {
        if($data == 'on') $data = 'yes'; else $data = 'no';

        return $data;
    }

    public function checkboxValue2($value)
    {
        if($value == 'on') $value = 'transfer'; else $value = 'change_of_status';

        return $value;
    }

    public function secForm2A()
    {
        /**
         * Type is to take the type of submission which is application or transfer
         */
        $type = Input::get('type');
        $function = Input::get('function');
        $application = Input::get('application');

        $application =  application::find($application);

        $function = funct::find($function);

        return view('cmo.sec-form-2A',[
            'type' => $type,
            'function' => $function,
            'application' => $application
        ]);

    }

    public function postSecForm2A(Request $request){

        $aid  = $request->input('aid');
        $fid  = $request->input('fid');
        $type = $request->input('type'); // type would be application
        $name = 'SEC FORM 2A';


        $secform2A = new secform2A();
        $secform2A->applicant_company = $request->input('applicant_company');
        $secform2A->applicant_company_address = $request->input('applicant_company_address');
        $secform2A->officer_replaced_name = $request->input('officer_replaced_name');
        $secform2A->current_reg = $request->input('current_reg');
        $secform2A->officer_replaced_period_service = $request->input('officer_replaced_period_service');
        $secform2A->reasons_for_leaving = $request->input('reasons_for_leaving');
        $secform2A->comment_on_integrity = $request->input('comment_on_integrity');
        $secform2A->sub_officer_name = $request->input('sub_officer_name');
        $secform2A->reg_sought = $request->input('reg_sought');
        $secform2A->sub_officer_period_service = $request->input('sub_officer_period_service');



        // checkbox fields

        $form_sec2_2b_submitted = $request->input('form_sec2_2b_submitted');
        $secform2A->form_sec2_2b_submitted = $this->checkboxValue($form_sec2_2b_submitted);

        $secform2A->sig = $request->input('sig');
        $secform2A->designation = $request->input('designation');
        $secform2A->director_name = $request->input('director_name');
        $secform2A->director_sig = $request->input('director_sig');
        $secform2A->secretary_name = $request->input('secretary_name');
        $secform2A->sec_sig = $request->input('sec_sig');
        $secform2A->company_seal = $request->input('company_seal');
        $secform2A->affidavit_name = $request->input('affidavit_name');
        $secform2A->dir_sec = $request->input('dir_sec');
        $secform2A->affidavit_of = $request->input('affidavit_of', false);
        $secform2A->first_input = $request->input('first_input');
        $secform2A->second_input = $request->input('second_input');
        $secform2A->third_input = $request->input('third_input');
        $secform2A->fourth_input = $request->input('fourth_input');
        $secform2A->fifth_input = $request->input('fifth_input');
        $secform2A->despondent_sig = $request->input('despondent_sig');


        $secform2A->save();

        $request->session()->flash('success','Form Submitted');

        // associate this submission with an application if its for
        // an application

        if($type == 'application'){
            $appliationSubmission = new applicationSubmission();
            $appliationSubmission->aid = $aid;
            $appliationSubmission->fid = $fid;
            $appliationSubmission->uid = auth()->user()->uid;
            $appliationSubmission->id = $secform2A->sf2aid;
            $appliationSubmission->name = $name;
            $appliationSubmission->save();
        }

        $request->session()->flash('success','Form Submitted');

        return redirect()->route('cmo.application.details',['application' => $aid]);


    }

    public function secForm2B()
    {
        /**
         * Type is to take the type of submission which is application or transfer
         */
        $type = Input::get('type');
        $function = Input::get('function');
        $application = Input::get('application');

        $application =  application::find($application);

        $function = funct::find($function);

        return view('cmo.sec-form-2B',[
            'type' => $type,
            'function' => $function,
            'application' => $application
        ]);

    }

    public function postSecForm2B(Request $request){



        $aid  = $request->input('aid');
        $fid  = $request->input('fid');
        $type = $request->input('type'); // type would be application
        $name = 'SEC FORM 2B';


        $secform2B = new secform2B();
        $secform2B->applicant_name = $request->input('applicant_name');
        $secform2B->applicant_reg_as = $request->input('applicant_reg_as');
        $secform2B->res_address = $request->input('res_address');
        $secform2B->employer_name = $request->input('employer_name');
        $secform2B->bus_address = $request->input('bus_address');



        $applying_for = $request->input('applying_for');
        $secform2B->applying_for = $this->checkboxValue2($applying_for);

        $secform2B->trans_previous_employer = $request->input('trans_previous_employer');
        $secform2B->date_of_resignation = $request->input('date_of_resignation');
        $secform2B->resig_letter = $request->input('resig_letter');
        $secform2B->reg_sought = $request->input('reg_sought');
        $secform2B->sub_officer_period_service = $request->input('sub_officer_period_service');
        $secform2B->applicable_exam = $request->input('applicable_exam');
        $secform2B->date_passed = $request->input('date_passed');
        $secform2B->proof_passing = $request->input('proof_passing', false);
        $secform2B->date_signed = $request->input('date_signed');
        $secform2B->applicant_sig = $request->input('applicant_sig');


        $secform2B->director_name = $request->input('director_name');
        $secform2B->dir_sig = $request->input('dir_sig');
        $secform2B->affidavit_name = $request->input('affidavit_name');
        $secform2B->affidavit_of = $request->input('affidavit_of', false);
        $secform2B->first_input = $request->input('first_input');
        $secform2B->second_input = $request->input('second_input');
        $secform2B->third_input = $request->input('third_input');
        $secform2B->fourth_input = $request->input('fourth_input');
        $secform2B->fifth_input = $request->input('fifth_input');
        $secform2B->despondent_sig = $request->input('despondent_sig');


        // checkbox fields

        $refusal = $request->input('refusal');
        $secform2B->refusal = $this->checkboxValue($refusal);


        $change_of_name = $request->input('change_of_name');
        $secform2B->change_of_name = $this->checkboxValue($change_of_name);


        $offences = $request->input('offences');
        $secform2B->offences = $this->checkboxValue($offences);


        $civil_procedures = $request->input('civil_procedures');
        $secform2B->civil_procedures = $this->checkboxValue($civil_procedures);


        $bankruptcy = $request->input('bankruptcy');
        $secform2B->bankruptcy = $this->checkboxValue($bankruptcy);


        $judgement = $request->input('judgement');
        $secform2B->judgement = $this->checkboxValue($judgement);


        $surety = $request->input('surety');
        $secform2B->surety = $this->checkboxValue($surety);



        $business_activities = $request->input('business_activities');
        $secform2B->business_activities = $this->checkboxValue($business_activities);



        $secform2B->save();

        $request->session()->flash('success','Form Submitted');

        // associate this submission with an application if its for
        // an application

        if($type == 'application'){
            $appliationSubmission = new applicationSubmission();
            $appliationSubmission->aid = $aid;
            $appliationSubmission->fid = $fid;
            $appliationSubmission->uid = auth()->user()->uid;
            $appliationSubmission->id = $secform2B->sf2bid;
            $appliationSubmission->name = $name;
            $appliationSubmission->save();
        }

        $request->session()->flash('success','Form Submitted');

        return redirect()->route('cmo.application.details',['application' => $aid]);

    }

    public function secForm2C()
    {
        /**
         * Type is to take the type of submission which is application or transfer
         */
        $type = Input::get('type');
        $function = Input::get('function');
        $application = Input::get('application');

        $application =  application::find($application);

        $function = funct::find($function);

        return view('cmo.sec-form-2C',[
            'type' => $type,
            'function' => $function,
            'application' => $application
        ]);

    }

    public function secForm2D()
    {
        /**
         * Type is to take the type of submission which is application or transfer
         */
        $type = Input::get('type');
        $function = Input::get('function');
        $application = Input::get('application');

        $application =  application::find($application);

        $function = funct::find($function);

        return view('cmo.sec-form-2D',[
            'type' => $type,
            'function' => $function,
            'application' => $application
        ]);

    }

    public function postSecForm2D(Request $request){


        $aid  = $request->input('aid');
        $fid  = $request->input('fid');
        $type = $request->input('type'); // type would be application
        $name = 'SEC FORM 2D';



        $secform2D = new secform2D();
        $secform2D->name = $request->input('name');
        $secform2D->sponsoring_company = $request->input('sponsoring_company');
        $secform2D->res_address = $request->input('res_address');
        $secform2D->perm_address = $request->input('perm_address');
        $secform2D->mail_address = $request->input('mail_address');
        $secform2D->official_email = $request->input('official_email');
        $secform2D->personal_email = $request->input('personal_email');
        $secform2D->gsm_number = $request->input('gsm_number');
        $secform2D->landline_number = $request->input('landline_number');
        $secform2D->dob = $request->input('dob');
        $secform2D->pob = $request->input('pob');
        $secform2D->sob = $request->input('sob',false);
        $secform2D->nationality = $request->input('nationality');
        $secform2D->sex = $request->input('sex');
        $secform2D->marital_status = $request->input('marital_status');
        $secform2D->next_kin_name = $request->input('next_kin_name');
        $secform2D->next_kin_number = $request->input('next_kin_number');
        $secform2D->next_kin_address = $request->input('next_kin_address');
        $secform2D->next_kin_relationship = $request->input('next_kin_relationship');
        $secform2D->next_kin_other = $request->input('next_kin_other');



        // checkbox fields

        $q1 = $request->input('q1');
        $q2 = $request->input('q2');
        $q3 = $request->input('q3');
        $q4 = $request->input('q4');
        $q5 = $request->input('q5');
        $q6 = $request->input('q6');
        $q7 = $request->input('q7');
        $q8 = $request->input('q8');
        $q9 = $request->input('q9');
        $q10 = $request->input('q10');
        $q11 = $request->input('q11');
        $q12 = $request->input('q12');
        $q13 = $request->input('q13');
        $q14 = $request->input('q14');
        $q15 = $request->input('q15');
        $q16 = $request->input('q16');
        $q17 = $request->input('q17');
        $q18 = $request->input('q18');
        $q19 = $request->input('q19');
        $q20 = $request->input('q20');

        $secform2D->q1 = $this->checkboxValue($q1);
        $secform2D->q2 = $this->checkboxValue($q2);
        $secform2D->q3 = $this->checkboxValue($q3);
        $secform2D->q4 = $this->checkboxValue($q4);
        $secform2D->q5 = $this->checkboxValue($q5);
        $secform2D->q6 = $this->checkboxValue($q6);
        $secform2D->q7 = $this->checkboxValue($q7);
        $secform2D->q8 = $this->checkboxValue($q8);
        $secform2D->q9 = $this->checkboxValue($q9);
        $secform2D->q10 = $this->checkboxValue($q10);
        $secform2D->q11 = $this->checkboxValue($q11);
        $secform2D->q12 = $this->checkboxValue($q12);
        $secform2D->q13 = $this->checkboxValue($q13);
        $secform2D->q14 = $this->checkboxValue($q14);
        $secform2D->q15 = $this->checkboxValue($q15);
        $secform2D->q16 = $this->checkboxValue($q16);
        $secform2D->q17 = $this->checkboxValue($q17);
        $secform2D->q18 = $this->checkboxValue($q18);
        $secform2D->q19 = $this->checkboxValue($q19);
        $secform2D->q20 = $this->checkboxValue($q20);

        $secform2D->q1_details = $request->input('q1_details');
        $secform2D->q2_details = $request->input('q2_details');
        $secform2D->q3_details = $request->input('q3_details');
        $secform2D->q4_details = $request->input('q4_details');
        $secform2D->q5_details = $request->input('q5_details');
        $secform2D->q6_details = $request->input('q6_details');
        $secform2D->q7_details = $request->input('q7_details');
        $secform2D->q8_details = $request->input('q8_details');
        $secform2D->q9_details = $request->input('q9_details');
        $secform2D->q10_details = $request->input('q10_details');
        $secform2D->q11_details = $request->input('q11_details');
        $secform2D->q12_details = $request->input('q12_details');
        $secform2D->q13_details = $request->input('q13_details');
        $secform2D->q14_details = $request->input('q14_details');
        $secform2D->q15_details = $request->input('q15_details');
        $secform2D->q16_details = $request->input('q16_details');
        $secform2D->q17_details = $request->input('q17_details');
        $secform2D->q18_details = $request->input('q18_details');
        $secform2D->q19_details = $request->input('q19_details');
        $secform2D->q20_details = $request->input('q20_details');
        $secform2D->affidavit_name = $request->input('affidavit_name');
        $secform2D->affidavit_address = $request->input('affidavit_address');
        $secform2D->affidavit_full_name = $request->input('affidavit_full_name');
        $secform2D->first_input = $request->input('first_input');
        $secform2D->second_input = $request->input('second_input');
        $secform2D->third_input = $request->input('third_input');
        $secform2D->fourth_input = $request->input('fourth_input');
        $secform2D->fifth_input = $request->input('fifth_input', false);
        $secform2D->despondent_sig = $request->input('despondent_sig');



        $secform2D->save();

        $request->session()->flash('success','Form Submitted');


        // associate this submission with an application if its for
        // an application

        if($type == 'application'){
            $appliationSubmission = new applicationSubmission();
            $appliationSubmission->aid = $aid;
            $appliationSubmission->fid = $fid;
            $appliationSubmission->uid = auth()->user()->uid;
            $appliationSubmission->id = $secform2D->sf2did;
            $appliationSubmission->name = $name;
            $appliationSubmission->save();
        }

        $request->session()->flash('success','Form Submitted');

        return redirect()->route('cmo.application.details',['application' => $aid]);

    }


    public function secForm3()
    {
        return view('cmo.sec-form-3');
    }

    public function postSecForm3(Request $request){



        $secform3 = new secform3();
        $secform3->reg_name = $request->input('reg_name');
        $secform3->state_company = $request->input('state_company');
        $secform3->reg_address = $request->input('reg_address');
        $secform3->pre_address = $request->input('pre_address');
        $secform3->date_incorporation = $request->input('date_incorporation');
        $secform3->type_of_reg = $request->input('type_of_reg');


        $secform3->authorized = $request->input('authorized');
        $secform3->share_of_n1 = $request->input('share_of_n1');
        $secform3->each_am1 = $request->input('each_am1');
        $secform3->issued = $request->input('issued');
        $secform3->share_of_n2 = $request->input('share_of_n2');
        $secform3->each_am2 = $request->input('each_am2',false);
        $secform3->paid_up = $request->input('paid_up');
        $secform3->share_of_n3 = $request->input('share_of_n3');
        $secform3->each_am3 = $request->input('each_am3');

        $secform3->nig_name = $request->input('nig_name');
        $secform3->nig_address = $request->input('nig_address');
        $secform3->nig_am = $request->input('nig_am');
        $secform3->nig_held = $request->input('nig_held');

        $secform3->fore_name = $request->input('fore_name');
        $secform3->fore_address = $request->input('fore_address');
        $secform3->fore_am = $request->input('fore_am');
        $secform3->fore_held = $request->input('fore_held');

        $secform3->part_name = $request->input('part_name');
        $secform3->part_address = $request->input('part_address');
        $secform3->part_qua = $request->input('part_qua');
        $secform3->part_exp = $request->input('part_exp');
        $secform3->pre_employment = $request->input('pre_employment');
        $secform3->pre_date = $request->input('pre_date');
        $secform3->pre_reasons_leaving = $request->input('pre_reasons_leaving');

        $secform3->exec_name = $request->input('exec_name');
        $secform3->exec_qua = $request->input('exec_qua');
        $secform3->exec_qua_date = $request->input('exec_qua_date');

        $secform3->ord_1 = $request->input('ord_1');
        $secform3->ord_2 = $request->input('ord_2');

        $secform3->pref_1 = $request->input('pref_1');
        $secform3->pref_2 = $request->input('pref_2');

        $secform3->deb_1 = $request->input('deb_1');
        $secform3->deb_2 = $request->input('deb_2');

        $secform3->gov_1 = $request->input('gov_1');
        $secform3->gov_2 = $request->input('gov_2');

        $secform3->oth_1 = $request->input('oth_1');
        $secform3->oth_2 = $request->input('oth_2');

        $secform3->mut_1 = $request->input('mut_1');
        $secform3->mut_2 = $request->input('mut_2');

        $secform3->open_1 = $request->input('open_1');
        $secform3->open_2 = $request->input('open_2');

        $secform3->agent_name = $request->input('agent_name');
        $secform3->agent_address = $request->input('agent_address');
        $secform3->agent_basis = $request->input('agent_basis');

        $secform3->sho_1 = $request->input('sho_1');
        $secform3->sho_2 = $request->input('sho_2');
        $secform3->sho_3 = $request->input('sho_3');
        $secform3->sho_4 = $request->input('sho_4');
        $secform3->sho_5 = $request->input('sho_5');

        $secform3->inv_1 = $request->input('inv_1');
        $secform3->inv_2 = $request->input('inv_2');
        $secform3->inv_3 = $request->input('inv_3');
        $secform3->inv_4 = $request->input('inv_4');
        $secform3->inv_5 = $request->input('inv_5');

        $secform3->quo_1 = $request->input('quo_1');
        $secform3->quo_2 = $request->input('quo_2');
        $secform3->quo_3 = $request->input('quo_3');
        $secform3->quo_4 = $request->input('quo_4');
        $secform3->quo_5 = $request->input('quo_5');

        $secform3->unquo_1 = $request->input('unquo_1');
        $secform3->unquo_2 = $request->input('unquo_2');
        $secform3->unquo_3 = $request->input('unquo_3');
        $secform3->unquo_4 = $request->input('unquo_4');
        $secform3->unquo_5 = $request->input('unquo_5');

        $secform3->oth_sho_1 = $request->input('oth_sho_1');
        $secform3->oth_sho_2 = $request->input('oth_sho_2');
        $secform3->oth_sho_3 = $request->input('oth_sho_3');
        $secform3->oth_sho_4 = $request->input('oth_sho_4');
        $secform3->oth_sho_5 = $request->input('oth_sho_5');

        $secform3->cred_1 = $request->input('cred_1');
        $secform3->cred_2 = $request->input('cred_2');
        $secform3->cred_3 = $request->input('cred_3');
        $secform3->cred_4 = $request->input('cred_4');
        $secform3->cred_5 = $request->input('cred_5');

        $secform3->lia_1 = $request->input('lia_1');
        $secform3->lia_2 = $request->input('lia_2');
        $secform3->lia_3 = $request->input('lia_3');
        $secform3->lia_4 = $request->input('lia_4');
        $secform3->lia_5 = $request->input('lia_5');

        $secform3->oth_lia_1 = $request->input('oth_lia_1');
        $secform3->oth_lia_2 = $request->input('oth_lia_2');
        $secform3->oth_lia_3 = $request->input('oth_lia_3');
        $secform3->oth_lia_4 = $request->input('oth_lia_4');
        $secform3->oth_lia_5 = $request->input('oth_lia_5');

        $secform3->dep_1 = $request->input('dep_1');
        $secform3->dep_2 = $request->input('dep_2');
        $secform3->dep_3 = $request->input('dep_3');
        $secform3->dep_4 = $request->input('dep_4');
        $secform3->dep_5 = $request->input('dep_5');



        // checkbox fields


        $q7 = $request->input('q7');
        $q8 = $request->input('q8');
        $q9 = $request->input('q9');
        $number16_i = $request->input('number16_i');
        $number16_ii = $request->input('number16_ii');
        $number16_iii = $request->input('number16_iii');
        $q17 = $request->input('q17');
        $q18 = $request->input('q18');
        $q21 = $request->input('q21');
        $q22 = $request->input('q22');
        $q23 = $request->input('q23');
        $q24_i = $request->input('q24_i');
        $q24_ii = $request->input('q24_ii');
        $q24_iii = $request->input('q24_iii');
        $q24_iv = $request->input('q24_iv');
        $q24_v = $request->input('q24_v');
        $q25 = $request->input('q25');
        $q27_ii = $request->input('q27_ii');
        $q27_v = $request->input('q27_v');


        $secform3->q7 = $this->checkboxValue($q7);
        $secform3->q8 = $this->checkboxValue($q8);
        $secform3->q9 = $this->checkboxValue($q9);
        $secform3->number16_i = $this->checkboxValue($number16_i);
        $secform3->number16_ii = $this->checkboxValue($number16_ii);
        $secform3->number16_iii = $this->checkboxValue($number16_iii);
        $secform3->q17 = $this->checkboxValue($q17);
        $secform3->q18 = $this->checkboxValue($q18);
        $secform3->q21 = $this->checkboxValue($q21);
        $secform3->q22 = $this->checkboxValue($q22);
        $secform3->q23 = $this->checkboxValue($q23);
        $secform3->q24_i = $this->checkboxValue($q24_i);
        $secform3->q24_ii = $this->checkboxValue($q24_ii);
        $secform3->q24_iii = $this->checkboxValue($q24_iii);
        $secform3->q24_iv = $this->checkboxValue($q24_iv);
        $secform3->q24_v = $this->checkboxValue($q24_v);
        $secform3->q25 = $this->checkboxValue($q25);
        $secform3->q27_ii = $this->checkboxValue($q27_ii);
        $secform3->q27_v = $this->checkboxValue($q27_v);


        $secform3->q7_details = $request->input('q7_details');
        $secform3->q8_details = $request->input('q8_details');
        $secform3->q9_details = $request->input('q9_details');
        $secform3->number16_i_details = $request->input('number16_i_details');
        $secform3->number16_ii_details = $request->input('number16_ii_details');
        $secform3->number16_iii_details = $request->input('number16_iii_details');
        $secform3->q17_details = $request->input('q17_details');
        $secform3->q18_details = $request->input('q18_details');
        $secform3->q19 = $request->input('q19');
        $secform3->q20 = $request->input('q20');
        $secform3->q21_details = $request->input('q21_details');
        $secform3->q22_details = $request->input('q22_details');
        $secform3->q23_details = $request->input('q23_details');
        $secform3->q24_vi = $request->input('q24_vi');
        $secform3->q26_a_i = $request->input('q26_a_i');
        $secform3->q26_a_ii = $request->input('q26_a_ii');
        $secform3->q26_a_iii = $request->input('q26_a_iii');
        $secform3->q26_b = $request->input('q26_b');
        $secform3->q26_c_name = $request->input('q26_c_name');
        $secform3->q26_c_service = $request->input('q26_c_service');
        $secform3->q26_c_rendered = $request->input('q26_c_rendered');
        $secform3->q26_d_dispute = $request->input('q26_d_dispute');
        $secform3->q26_d_party = $request->input('q26_d_party');
        $secform3->q26_d_set = $request->input('q26_d_set');
        $secform3->q26_e = $request->input('q26_e');
        $secform3->q26_f = $request->input('q26_f');
        $secform3->q27_i_name = $request->input('q27_i_name');
        $secform3->q27_i_address = $request->input('q27_i_address');
        $secform3->q27_ii_details = $request->input('q27_ii_details');
        $secform3->q27_iii = $request->input('q27_iii');
        $secform3->q27_iv = $request->input('q27_iv');
        $secform3->q27_v_details = $request->input('q27_v_details');
        $secform3->q27_vi = $request->input('q27_vi');


        $secform3->affidavit_name = $request->input('affidavit_name');
        $secform3->affidavit_address = $request->input('affidavit_address');
        $secform3->affidavit_full_name = $request->input('affidavit_full_name');
        $secform3->first_input = $request->input('first_input');
        $secform3->second_input = $request->input('second_input');
        $secform3->third_input = $request->input('third_input');
        $secform3->fourth_input = $request->input('fourth_input');
        $secform3->fifth_input = $request->input('fifth_input', false);
        $secform3->commish = $request->input('commish', false);
        $secform3->despondent_sig = $request->input('despondent_sig');
        $secform3->audit = $request->input('audit');
        $secform3->profit = $request->input('profit');
        $secform3->schedule = $request->input('schedule');
        $secform3->cac = $request->input('cac');
        $secform3->sworn = $request->input('sworn');
        $secform3->memo = $request->input('memo');



        $secform3->save();

        $request->session()->flash('success','Form Submitted');

        return redirect()->route('cmo.applications.sec-form-3');

        //   }catch (\Exception $exception){

        //        $request->session()->flash('error','Sorry an error occurred. Please try again');
        //        return redirect( 'cmo.sec-form-3');

        //   }


    }

    public function secForm3A()
    {
        return view('cmo.sec-form-3A');
    }

    public function postSecForm3A(Request $request){



        $secform3A = new secform3A();
        $secform3A->reg_name = $request->input('reg_name');
        $secform3A->state_company = $request->input('state_company');
        $secform3A->reg_address = $request->input('reg_address');
        $secform3A->pre_address = $request->input('pre_address');
        $secform3A->date_incorporation = $request->input('date_incorporation');
        $secform3A->type_of_reg = $request->input('type_of_reg');


        $secform3A->authorized = $request->input('authorized');
        $secform3A->share_of_n1 = $request->input('share_of_n1');
        $secform3A->each_am1 = $request->input('each_am1');
        $secform3A->issued = $request->input('issued');
        $secform3A->share_of_n2 = $request->input('share_of_n2');
        $secform3A->each_am2 = $request->input('each_am2',false);
        $secform3A->paid_up = $request->input('paid_up');
        $secform3A->share_of_n3 = $request->input('share_of_n3');
        $secform3A->each_am3 = $request->input('each_am3');

        $secform3A->nig_name = $request->input('nig_name');
        $secform3A->nig_address = $request->input('nig_address');
        $secform3A->nig_am = $request->input('nig_am');
        $secform3A->nig_held = $request->input('nig_held');

        $secform3A->fore_name = $request->input('fore_name');
        $secform3A->fore_address = $request->input('fore_address');
        $secform3A->fore_am = $request->input('fore_am');
        $secform3A->fore_held = $request->input('fore_held');

        $secform3A->part_name = $request->input('part_name');
        $secform3A->part_address = $request->input('part_address');
        $secform3A->part_qua = $request->input('part_qua');
        $secform3A->part_exp = $request->input('part_exp');
        $secform3A->pre_employment = $request->input('pre_employment');
        $secform3A->pre_date = $request->input('pre_date');
        $secform3A->pre_reasons_leaving = $request->input('pre_reasons_leaving');

        $secform3A->exec_name = $request->input('exec_name');
        $secform3A->exec_qua = $request->input('exec_qua');
        $secform3A->exec_qua_date = $request->input('exec_qua_date');

        $secform3A->ord_1 = $request->input('ord_1');
        $secform3A->ord_2 = $request->input('ord_2');

        $secform3A->pref_1 = $request->input('pref_1');
        $secform3A->pref_2 = $request->input('pref_2');

        $secform3A->deb_1 = $request->input('deb_1');
        $secform3A->deb_2 = $request->input('deb_2');

        $secform3A->gov_1 = $request->input('gov_1');
        $secform3A->gov_2 = $request->input('gov_2');

        $secform3A->oth_1 = $request->input('oth_1');
        $secform3A->oth_2 = $request->input('oth_2');

        $secform3A->mut_1 = $request->input('mut_1');
        $secform3A->mut_2 = $request->input('mut_2');

        $secform3A->open_1 = $request->input('open_1');
        $secform3A->open_2 = $request->input('open_2');

        $secform3A->agent_name = $request->input('agent_name');
        $secform3A->agent_address = $request->input('agent_address');
        $secform3A->agent_basis = $request->input('agent_basis');

        $secform3A->sho_1 = $request->input('sho_1');
        $secform3A->sho_2 = $request->input('sho_2');
        $secform3A->sho_3 = $request->input('sho_3');
        $secform3A->sho_4 = $request->input('sho_4');
        $secform3A->sho_5 = $request->input('sho_5');

        $secform3A->inv_1 = $request->input('inv_1');
        $secform3A->inv_2 = $request->input('inv_2');
        $secform3A->inv_3 = $request->input('inv_3');
        $secform3A->inv_4 = $request->input('inv_4');
        $secform3A->inv_5 = $request->input('inv_5');

        $secform3A->quo_1 = $request->input('quo_1');
        $secform3A->quo_2 = $request->input('quo_2');
        $secform3A->quo_3 = $request->input('quo_3');
        $secform3A->quo_4 = $request->input('quo_4');
        $secform3A->quo_5 = $request->input('quo_5');

        $secform3A->unquo_1 = $request->input('unquo_1');
        $secform3A->unquo_2 = $request->input('unquo_2');
        $secform3A->unquo_3 = $request->input('unquo_3');
        $secform3A->unquo_4 = $request->input('unquo_4');
        $secform3A->unquo_5 = $request->input('unquo_5');

        $secform3A->oth_sho_1 = $request->input('oth_sho_1');
        $secform3A->oth_sho_2 = $request->input('oth_sho_2');
        $secform3A->oth_sho_3 = $request->input('oth_sho_3');
        $secform3A->oth_sho_4 = $request->input('oth_sho_4');
        $secform3A->oth_sho_5 = $request->input('oth_sho_5');

        $secform3A->cred_1 = $request->input('cred_1');
        $secform3A->cred_2 = $request->input('cred_2');
        $secform3A->cred_3 = $request->input('cred_3');
        $secform3A->cred_4 = $request->input('cred_4');
        $secform3A->cred_5 = $request->input('cred_5');

        $secform3A->lia_1 = $request->input('lia_1');
        $secform3A->lia_2 = $request->input('lia_2');
        $secform3A->lia_3 = $request->input('lia_3');
        $secform3A->lia_4 = $request->input('lia_4');
        $secform3A->lia_5 = $request->input('lia_5');

        $secform3A->oth_lia_1 = $request->input('oth_lia_1');
        $secform3A->oth_lia_2 = $request->input('oth_lia_2');
        $secform3A->oth_lia_3 = $request->input('oth_lia_3');
        $secform3A->oth_lia_4 = $request->input('oth_lia_4');
        $secform3A->oth_lia_5 = $request->input('oth_lia_5');

        $secform3A->dep_1 = $request->input('dep_1');
        $secform3A->dep_2 = $request->input('dep_2');
        $secform3A->dep_3 = $request->input('dep_3');
        $secform3A->dep_4 = $request->input('dep_4');
        $secform3A->dep_5 = $request->input('dep_5');



        // checkbox fields


        $q7 = $request->input('q7');
        $q8 = $request->input('q8');
        $q9 = $request->input('q9');
        $number16_i = $request->input('number16_i');
        $number16_ii = $request->input('number16_ii');
        $number16_iii = $request->input('number16_iii');
        $q17 = $request->input('q17');
        $q18 = $request->input('q18');
        $q21 = $request->input('q21');
        $q22 = $request->input('q22');
        $q23 = $request->input('q23');
        $q24_i = $request->input('q24_i');
        $q24_ii = $request->input('q24_ii');
        $q24_iii = $request->input('q24_iii');
        $q24_iv = $request->input('q24_iv');
        $q24_v = $request->input('q24_v');
        $q25 = $request->input('q25');
        $q27_ii = $request->input('q27_ii');
        $q27_v = $request->input('q27_v');


        $secform3A->q7 = $this->checkboxValue($q7);
        $secform3A->q8 = $this->checkboxValue($q8);
        $secform3A->q9 = $this->checkboxValue($q9);
        $secform3A->number16_i = $this->checkboxValue($number16_i);
        $secform3A->number16_ii = $this->checkboxValue($number16_ii);
        $secform3A->number16_iii = $this->checkboxValue($number16_iii);
        $secform3A->q17 = $this->checkboxValue($q17);
        $secform3A->q18 = $this->checkboxValue($q18);
        $secform3A->q21 = $this->checkboxValue($q21);
        $secform3A->q22 = $this->checkboxValue($q22);
        $secform3A->q23 = $this->checkboxValue($q23);
        $secform3A->q24_i = $this->checkboxValue($q24_i);
        $secform3A->q24_ii = $this->checkboxValue($q24_ii);
        $secform3A->q24_iii = $this->checkboxValue($q24_iii);
        $secform3A->q24_iv = $this->checkboxValue($q24_iv);
        $secform3A->q24_v = $this->checkboxValue($q24_v);
        $secform3A->q25 = $this->checkboxValue($q25);
        $secform3A->q27_ii = $this->checkboxValue($q27_ii);
        $secform3A->q27_v = $this->checkboxValue($q27_v);


        $secform3A->q7_details = $request->input('q7_details');
        $secform3A->q8_details = $request->input('q8_details');
        $secform3A->q9_details = $request->input('q9_details');
        $secform3A->number16_i_details = $request->input('number16_i_details');
        $secform3A->number16_ii_details = $request->input('number16_ii_details');
        $secform3A->number16_iii_details = $request->input('number16_iii_details');
        $secform3A->q17_details = $request->input('q17_details');
        $secform3A->q18_details = $request->input('q18_details');
        $secform3A->q19 = $request->input('q19');
        $secform3A->q20 = $request->input('q20');
        $secform3A->q21_details = $request->input('q21_details');
        $secform3A->q22_details = $request->input('q22_details');
        $secform3A->q23_details = $request->input('q23_details');
        $secform3A->q24_vi = $request->input('q24_vi');
        $secform3A->q26_a_i = $request->input('q26_a_i');
        $secform3A->q26_a_ii = $request->input('q26_a_ii');
        $secform3A->q26_a_iii = $request->input('q26_a_iii');
        $secform3A->q26_b = $request->input('q26_b');
        $secform3A->q26_c_name = $request->input('q26_c_name');
        $secform3A->q26_c_service = $request->input('q26_c_service');
        $secform3A->q26_c_rendered = $request->input('q26_c_rendered');
        $secform3A->q26_d_dispute = $request->input('q26_d_dispute');
        $secform3A->q26_d_party = $request->input('q26_d_party');
        $secform3A->q26_d_set = $request->input('q26_d_set');
        $secform3A->q26_e = $request->input('q26_e');
        $secform3A->q26_f = $request->input('q26_f');
        $secform3A->q27_i_name = $request->input('q27_i_name');
        $secform3A->q27_i_address = $request->input('q27_i_address');
        $secform3A->q27_ii_details = $request->input('q27_ii_details');
        $secform3A->q27_iii = $request->input('q27_iii');
        $secform3A->q27_iv = $request->input('q27_iv');
        $secform3A->q27_v_details = $request->input('q27_v_details');
        $secform3A->q27_vi = $request->input('q27_vi');


        $secform3A->affidavit_name = $request->input('affidavit_name');
        $secform3A->affidavit_address = $request->input('affidavit_address');
        $secform3A->affidavit_full_name = $request->input('affidavit_full_name');
        $secform3A->first_input = $request->input('first_input');
        $secform3A->second_input = $request->input('second_input');
        $secform3A->third_input = $request->input('third_input');
        $secform3A->fourth_input = $request->input('fourth_input');
        $secform3A->fifth_input = $request->input('fifth_input', false);
        $secform3A->commish = $request->input('commish', false);
        $secform3A->despondent_sig = $request->input('despondent_sig');
        $secform3A->audit = $request->input('audit');
        $secform3A->profit = $request->input('profit');
        $secform3A->schedule = $request->input('schedule');
        $secform3A->cac = $request->input('cac');
        $secform3A->sworn = $request->input('sworn');
        $secform3A->memo = $request->input('memo');



        $secform3A->save();

        $request->session()->flash('success','Form Submitted');

        return redirect()->route('cmo.applications.sec-form-3A');

        //   }catch (\Exception $exception){

        //        $request->session()->flash('error','Sorry an error occurred. Please try again');
        //        return redirect( 'cmo.sec-form-3');

        //   }


    }

    public function secForm3B()
    {
        return view('cmo.sec-form-3B');
    }

}
