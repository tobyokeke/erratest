<?php

namespace App\Http\Controllers;

use App\application;
use App\applicationDocument;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class LegalController extends Controller
{
    public function __construct() {
        $this->middleware('auth');
    }

    public function dashboard() {

        $applications = application::orderBy('created_at','desc')->where('status','submitted')->get()->take(5);

        $documents = applicationDocument::orderBy('created_at','desc')->where('requires_legal',1)->where('legal_status','pending')->get()->take(5);

        $allApplications = application::where('status','submitted')->get();
        $applicationsToday = application::all()->where('status','submitted')->where('created_at','>',Carbon::today())->count();
        $applicationsThisWeek = application::all()->where('status','submitted')->where('created_at','>',Carbon::now()->startOfWeek())->count();
        $applicationsThisMonth = application::all()->where('status','submitted')->where('created_at','>',Carbon::now()->startOfMonth())->count();

        $applicationsToReview = [];

        foreach($allApplications as $application){
            foreach($application->Documents as $document){
                if($document->requires_legal == 1 && $document->legal_status == 'pending' ){
                    // identify applications that have documents that are ment to be reviewed
                    array_push($applicationsToReview,$application);
                }
            }
        }


        return view('legal.dashboard',[
            'applications' => $applications,
            'allApplications' => $allApplications,
            'applicationsToday' => $applicationsToday,
            'applicationsThisWeek' => $applicationsThisWeek,
            'applicationsThisMonth' => $applicationsThisMonth,

            'applicationsToReview' => $applicationsToReview,
            'documents' => $documents

        ]);


    }

    public function applications() {
        $aids = [];

        $allApplications = application::where('status','submitted')->get();

        foreach($allApplications as $application){
            foreach($application->Documents as $document){
                if($document->requires_legal == 1 && $document->legal_status == 'pending' ){
                    // identify applications that have documents that are ment to be reviewed
                    array_push($applicationsToReview,$application->aid);
                }
            }
        }

        $applications = application::whereIn('aid',$aids)->paginate(30);

        return view('legal.applications.manage',[
            'applications' => $applications
        ]);
    }
}
