<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\secform2;
use App\secform2A;
use App\secform2B;
use App\secform2D;
use App\secform3;
use App\secform3A;
use App\secform3B;

class ViewController extends Controller
{
    public function secForm2($sf2id)
    {

        $secform2 = secform2::find($sf2id);

        return view('cmo.views.sec-form-2',
            [
                'form2' => $secform2
            ]
        );
    }

    public function secForm2D($id)
    {

        $secform2 = secform2::find($id);

        return view('cmo.views.sec-form-2D',
            [
                'form2D' => $secform2
            ]
        );
    }

}
