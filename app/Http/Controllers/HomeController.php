<?php

namespace App\Http\Controllers;

use App\profile;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }



    public function dashboard() {
        if(auth()->user()->role == 'cmo'){
           return redirect()->route('cmo.dashboard');
        }

        if(auth()->user()->role == 'administrator'){
            return redirect()->route('admin.dashboard');
        }

        if(auth()->user()->role == 'dh'){
            return redirect()->route('dhDashboard');
        }

        if(auth()->user()->role == 'hod'){
            return redirect()->route('hodDashboard');
        }

        if(auth()->user()->role == 'deskofficer'){
            return redirect()->route('deskOfficer.dashboard');
        }

    }

    public function changePassword() {
        return view('changePassword');
    }

    public function editProfile() {
        $user = User::find(auth()->user()->uid);
        return view('editProfile',[
            'user' => $user->Profile
        ]);
    }

    public function postEditProfile(Request $request) {
        $profile = profile::find(auth()->user()->Profile->upid);
        $profile->lastName = $request->input('lastName');
        $profile->firstName = $request->input('firstName');
        $profile->middleName = $request->input('middleName');
        $profile->function = $request->input('function');
        $profile->jobResponsibility = $request->input('jobResponsibility');
        $profile->addressLine1 = $request->input('addressLine1');
        $profile->addressLine2 = $request->input('addressLine2');
        $profile->state = $request->input('state');
        $profile->country = $request->input('country');
        $profile->phone = $request->input('phone');
        $profile->save();

        session()->flash('success','Profile Updated.');
        return redirect()->back();
    }

    public function logout() {
        auth()->logout();
        return redirect('/');
    }
}
