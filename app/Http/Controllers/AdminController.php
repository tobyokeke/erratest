<?php

namespace App\Http\Controllers;

use App\application;
use App\funct;
use App\functionForm;
use App\payment;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;

class AdminController extends Controller
{

    public function dashboard() {

        $users = User::all()->sortByDesc('created_at')->take(5);

        //get user info
        $allUsers = User::all()->count();
        $usersToday = User::where('created_at','>',Carbon::today())->count();
        $usersThisWeek = User::where('created_at','>',Carbon::now()->startOfWeek())->count();
        $usersThisMonth = User::where('created_at','>',Carbon::now()->startOfMonth())->count();

        //payment info
        $allPayments = payment::all()->where('status','paid')->sum('amount');
        $paymentsToday = payment::where('created_at','>',Carbon::today())->where('status','paid')->sum('amount');
        $paymentsThisWeek = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfWeek())->sum('amount');
        $paymentsThisMonth = payment::where('status','paid')->where('created_at','>',Carbon::now()->startOfMonth())->sum('amount');

        //functions
        $allFunctions = funct::all()->count();
        $functionsToday = funct::where('created_at','>',Carbon::today())->count();
        $functionsThisWeek = funct::where('created_at','>',Carbon::now()->startOfWeek())->count();
        $functionsThisMonth = funct::where('created_at','>',Carbon::now()->startOfMonth())->count();

        //applications
        $allApplications = application::all()->count();
        $applicationsToday = application::where('created_at','>',Carbon::today())->count();
        $applicationsThisWeek = application::where('created_at','>',Carbon::now()->startOfWeek())->count();
        $applicationsThisMonth = application::where('created_at','>',Carbon::now()->startOfMonth())->count();


        return view('admin.dashboard',[
            // user info
            'users' => $users,

            'allUsers' => $allUsers,
            'usersToday' => $usersToday,
            'usersThisWeek' => $usersThisWeek,
            'usersThisMonth' => $usersThisMonth,

            //payment info

            'allPayments' => $allPayments,
            'paymentsToday' => $paymentsToday,
            'paymentsThisWeek' => $paymentsThisWeek,
            'paymentsThisMonth' => $paymentsThisMonth,

             //functions info

            'allFunctions' => $allFunctions,
            'functionsToday' => $functionsToday,
            'functionsThisWeek' => $functionsThisWeek,
            'functionsThisMonth' => $functionsThisMonth,

             //applications info

            'allApplications' => $allApplications,
            'applicationsToday' => $applicationsToday,
            'applicationsThisWeek' => $applicationsThisWeek,
            'applicationsThisMonth' => $applicationsThisMonth,

        ]);
    }

    public function users() {
        $users = User::orderBy('created_at','desc')->paginate(20);
        return view('admin.users.manage',[
            'users' => $users
        ]);
    }

    public function userDetails(User $user) {
        return view('admin.users.details',[
            'user' => $user
        ]);
    }

    public function postEditUser(Request $request, User $user) {

        $this->validate($request,[
            'email' => 'email|required',
            'phone' => 'required|min:11|max:11'
        ]);


        $user->role = $request->input('role');
        $user->email = $request->input('email');
        $user->phone = $request->input('phone');
        $user->save();

        session()->flash('success','User Edited.');
        return redirect()->back();
    }

    public function functions() {
        $functions = funct::orderBy('created_at','desc')->paginate(10);
        return view('admin.functions.manage',[
            'functions' => $functions
        ]);
    }

    public function functionDetails(funct $function) {
        return view('admin.functions.details',[
            'function' => $function
        ]);
    }

    public function addFunction() {
        return view('admin.functions.add');
    }

    public function postAddFunction(Request $request) {
        $forms = $request->input('forms');
        $isStandalone = $request->input('isStandalone');

        if($isStandalone == 'on') $isStandalone = 1;
        else $isStandalone = 0;

        $function = new funct();
        $function->isStandalone = $isStandalone;
        $function->uid = auth()->user()->uid;
        $function->name = $request->input('name');
        $function->applicationFee = $request->input('applicationFee');
        $function->processingFee = $request->input('processingFee');
        $function->registrationFee = $request->input('registrationFee');
        $function->description = $request->input('description');
        $function->save();

        foreach($forms as $form){
            $functionForm = new functionForm();
            $functionForm->fid = $function->fid;
            $functionForm->name = $form;
            $functionForm->save();
        }

        session()->flash('success','Function Added.');
        return redirect()->back();

    }


}
