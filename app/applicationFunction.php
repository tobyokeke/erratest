<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicationFunction extends Model
{
    protected $primaryKey = 'afid';
    protected $table = 'application_functions';

    public function Application() {
        return $this->belongsTo(application::class,'aid','aid');
    }

    public function Function () {
        return $this->belongsTo(funct::class,'fid','fid');
    }
}
