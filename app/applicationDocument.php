<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class applicationDocument extends Model
{
    protected $primaryKey = 'adid';
    protected $table = 'application_documents';

    public function Application() {
        return $this->belongsTo(application::class,'aid','aid');
    }

    public function Staff() { // gives legal staff who vetted the document
        return $this->belongsTo(User::class,'vetted_by','uid');
    }
}
