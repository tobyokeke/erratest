<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class application extends Model
{
    protected $primaryKey = 'aid';
    protected $table = 'applications';

    public function User() {
        return $this->belongsTo(User::class,'uid','uid');
    }

    public function AssignedTo() {
        return $this->belongsTo(User::class,'assignedTo','uid');
    }

    public function Functions() {
        return $this->belongsToMany(funct::class,'application_functions','aid','fid');
    }

    public function Submissions() {
        return $this->hasMany(applicationSubmission::class,'aid','aid');
    }

    public function Payment() {
        return $this->hasOne(payment::class,'aid','aid');
    }

    public function Documents() {
        return $this->hasMany(applicationDocument::class,'aid','aid');
    }

    public function FunctionDocuments($fid){
        return applicationDocument::where('aid',$this->aid)->where('fid',$fid)->get();
    }

}
