<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class profile extends Model
{
    protected $primaryKey = 'upid';
    protected $table = 'user_profile';

    public function User() {
        return $this->belongsTo(User::class,'uid','uid');
    }
}
