<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class secform2 extends Model
{
    protected $primaryKey = 'sf2id';
    protected $table = 'sec-form-2';
}
