<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class companyFunction extends Model
{
    protected $primaryKey = 'cfid';
    protected $table = 'company_functions';

    public function Company() {
        return $this->belongsTo(company::class,'cid','cid');
    }

    public function Funct() {
        return $this->belongsTo(funct::class,'fid','fid');
    }
}
