<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class secform2A extends Model
{
    protected $primaryKey = 'sf2aid';
    protected $table = 'sec-form-2A';
}
