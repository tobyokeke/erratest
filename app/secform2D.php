<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class secform2D extends Model
{
    protected $primaryKey = 'sf2did';
    protected $table = 'sec-form-2D';
}
