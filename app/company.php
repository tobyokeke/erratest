<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class company extends Model
{
    protected $primaryKey = 'cid';
    protected $table = 'companies';
    protected $dates = ['startDate','endDate'];


    public function Functions() {
        return $this->belongsToMany(funct::class,'company_functions','cid','fid');
    }

    public function CompanyFunctions() {
        return $this->hasMany(companyFunction::class,'cid','cid');
    }

    public function Users() {
        return $this->belongsToMany(User::class,'user_companies','cid','uid');
    }
}
