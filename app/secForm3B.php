<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class secForm3B extends Model
{
    protected $primaryKey = 'sf3bid';
    protected $table = 'sec-form-3B';
}
