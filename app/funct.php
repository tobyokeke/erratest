<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class funct extends Model
{
    // used funct because i couldn't used function. its reserved. Still works right? ;)
    protected $primaryKey = 'fid';
    protected $table = 'functions';

    public function Forms() {
        return $this->hasMany(functionForm::class,'fid','fid');
    }

    public function User() {
        return $this->belongsTo(User::class,'uid','uid');
    }

}
