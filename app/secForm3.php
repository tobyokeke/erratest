<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class secForm3 extends Model
{
    protected $primaryKey = 'sf3id';
    protected $table = 'sec-form-3';
}
