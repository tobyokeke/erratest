<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InitialDb extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('application_updates', function(Blueprint $table){
            $table->increments('auid');
            $table->integer('aid');
            $table->integer('uid');
            $table->string('update',2000);
            $table->timestamps();
        });

        Schema::create('users', function(Blueprint $table){
            $table->increments('uid');
            $table->string('name');
            $table->string('phone',191)->unique();
            $table->string('email',191)->unique();
            $table->boolean('isEmailVerified')->default(0);
            $table->timestamp('emailVerifiedAt')->nullable();
            $table->timestamp('lastLogin')->nullable();
            $table->enum('role',['administrator','deskofficer','hod','dh','cmo','legal'])->default('cmo');
            $table->string('password');
            $table->timestamps();
        });

        Schema::create('confirmations', function(Blueprint $table){
            $table->increments('confid');
            $table->integer('uid');
            $table->string('email');
            $table->string('token',191)->unique();
            $table->timestamps();
        });

        /**
         * Every user(Sponsored Individual) belongs to a company and users can switch company,
         * but still keep their registration status.
         * A company has to be registered as well and the company information has to be verified by SEC.
         * An already registered user(Sponsored Individual) can start the registration for a new company,
         * which would nullify their connection to another company.
        */
        Schema::create('companies', function(Blueprint $table){
            $table->increments('cid');
            $table->integer('fileNumber')->nullable();
            $table->string('operatorName');
            $table->integer('rcNumber')->unique();
            $table->integer('cacNumber')->unique();
            $table->date('registrationDate');
            $table->decimal('fidelityBond',12,2);
            $table->decimal('paidUpCapital',12,2);
            $table->enum('cmoStatus',['expiredBond',
                        'illegalOperator',
                        'invalidRegistration',
                        'pendingRegistration',
                        'suspendedOperator',
                        'validRegistration'
                        ])->default('pendingRegistration');
            $table->date('registrationExpiryDate')->nullable();
            $table->date('fidelityBondExpiryDate')->nullable();
            $table->timestamps();
        });

        /**
         * Store functions for each company
         */

        Schema::create('company_functions', function(Blueprint $table){
            $table->increments('cfid');
            $table->integer('fid');
            $table->integer('cid');
            $table->timestamps();
        });

        /**
         * Users(Sponsored Individuals) can be independently registered and can transfer their registration
         * to another company.
         * First User(Sponsored Individual) for a company should be the company's compliance officer,
         * company must have at least one compliance officer.
         */
        Schema::create('user_profile', function(Blueprint $table){
            $table->increments('upid');
            $table->integer('uid')->unique();
            $table->string('bvn');
            $table->string('lastName');
            $table->string('firstName');
            $table->string('middleName')->nullable();
            $table->integer('function')->nullable();// null if user is a sec staff.
            $table->enum('jobResponsibility',[
                                        'branchOfficeHead',
                                        'complianceOfficer',
                                        'executiveDirector',
                                        'independentDirector',
                                        'managingDirector',
                                        'nonExecutiveDirector',
                                        'secStaff',
                                        'sponsoredIndividual'
                                        ])->default('complianceOfficer');
            $table->enum('status',['invalid',
                                'pending',
                                'suspended',
                                'valid'
                                ])->default('valid');//if job responsibility is secStaff then this becomes validRegistration
            $table->string('addressLine1')->nullable();
            $table->string('addressLine2')->nullable();
            $table->string('state')->nullable();
            $table->string('country')->nullable();
            $table->string('phone',191)->unique()->nullable();
            $table->timestamps();

//            $table->foreign('uid')->references('uid')->on('users');
//            $table->foreign('function')->references('fid')->on('functions');
        });

        /**
         * users belong to many companies but only one can be active at any time,
         * the others would be for reference and history purposes.
         * Requests to join a company has to be approved by the company's compliance officer
         */
        Schema::create('user_companies', function(Blueprint $table){
            $table->increments('ucid');
            $table->integer('uid');
            $table->integer('cid');
            $table->enum('status', ['active','inactive','pending'])->default('pending');
            $table->timestamp('startDate')->nullable();
            $table->timestamp('endDate')->nullable();
            $table->timestamps();

//            $table->foreign('uid')->references('uid')->on('users');
//            $table->foreign('cid')->references('cid')->on('companies');
        });


        Schema::create('functions', function(Blueprint $table){
            $table->increments('fid');// why not auto-increments
            $table->integer('uid');// why is user id here? user(sponsored individual) can only have one function which is catered for in the user_profile table, company can have up to 7 functions which is catered for in the companies table.
                                            // uid is here for us to know the sec staff that created the function
            $table->string('name');
            $table->enum('status',['active','disabled'])->default('active'); // indicates that submissions can now be accepted for this function
            //$table->integer('price');// Need to track each fee seperately upon payment.
            $table->decimal('processingFee',12,2);
            $table->decimal('applicationFee',12,2);
            $table->decimal('registrationFee',12,2);
            $table->text('description',20000);
            $table->boolean('isStandalone')->default(0);
            $table->timestamps();
        });

        Schema::create('function_forms', function(Blueprint $table){
            $table->increments('ffid');
            $table->integer('fid');
            $table->enum('name',[
                'SEC FORM 1',
                'SEC FORM 2',
                'SEC FORM 2A',
                'SEC FORM 2B',
                'SEC FORM 2C',
                'SEC FORM 2D',
                'SEC FORM 3',
                'SEC FORM 3A',
                'SEC FORM 3B',
                'SEC FORM 4',
                'SEC FORM 4A',
                'SEC FORM 5',
                'SEC FORM 5A',
                'SEC FORM 5B',
                'SEC FORM 5C',
                'SEC FORM 6',
                'SEC FORM 6A',
                'SEC FORM 6A 1',
                'SEC FORM 6A 2',
                'SEC FORM 6A 3',
                'SEC FORM 6A 4',
                'SEC FORM 6A 5',
                'SEC FORM 7',
                'SEC FORM 8',
                'SEC FORM 9A',
                'SEC FORM 9B',
                'SEC FORM MT1',

                ]);
            $table->timestamps();
        });


        Schema::create('applications', function(Blueprint $table){ // holds users applications
            $table->increments('aid');
            $table->integer('uid');
            $table->integer('assignedTo')->nullable(); // to track the staff the application is assigned to
            $table->enum('status',['pending','submitted','approved','rejected']);
            $table->timestamps();
        });

        Schema::create('application_functions', function(Blueprint $table){
            $table->increments('afid');
            $table->integer('aid');
            $table->integer('fid');
            $table->timestamps();
        });

        Schema::create('application_submissions', function(Blueprint $table){
            //handles linking of multiple form submissions to one application
            $table->increments('asid');
            $table->integer('aid');
            $table->integer('fid');
            $table->integer('uid');
            $table->integer('id'); // stores the id of the submission for the selected form
            $table->enum('name',[
                'SEC FORM 1',
                'SEC FORM 2',
                'SEC FORM 2A',
                'SEC FORM 2B',
                'SEC FORM 2C',
                'SEC FORM 2D',
                'SEC FORM 3',
                'SEC FORM 3A',
                'SEC FORM 3B',
                'SEC FORM 4',
                'SEC FORM 4A',
                'SEC FORM 5',
                'SEC FORM 5A',
                'SEC FORM 5B',
                'SEC FORM 5C',
                'SEC FORM 6',
                'SEC FORM 6A',
                'SEC FORM 6A 1',
                'SEC FORM 6A 2',
                'SEC FORM 6A 3',
                'SEC FORM 6A 4',
                'SEC FORM 6A 5',
                'SEC FORM 7',
                'SEC FORM 8',
                'SEC FORM 9A',
                'SEC FORM 9B',
                'SEC FORM MT1',

            ]); // stores the selected form
            $table->timestamps();
        });


        Schema::create('application_documents', function(Blueprint $table){
            $table->increments('adid');
            $table->integer('aid');
            $table->integer('fid');
            $table->integer('uid');
            $table->integer('vetted_by')->nullable();
            $table->string('name');
            $table->boolean('requires_legal')->default(0);
            $table->enum('legal_status',['pending','approved','rejected'])->default('pending');
            $table->timestamp('date_legal_vetted')->nullable();
            $table->timestamp('date_do_vetted')->nullable();
            $table->enum('status',['pending','approved','rejected'])->default('pending');
            $table->string('review_message')->nullable();
            $table->string('legal_review_message')->nullable();
            $table->string('url',2000);
            $table->timestamps();
        });


        Schema::create('payments', function(Blueprint $table){
            $table->increments('pid');
            $table->integer('uid');
            $table->integer('aid')->nullable(); //nullable so that we can put a record and then charge, when payment is successful
                                                        // we would continue on to make an application and assign this payment to it

            $table->string('functions');
            $table->decimal('amount');
            $table->string('channel')->default('remita');
            $table->enum('status',['initiated','pending','failed','paid']);
            $table->string('reference');
            $table->string('flwReference')->nullable();
            $table->decimal('charge',12,2)->nullable();
            $table->timestamps();

        });

        Schema::create('sec-form-2', function(Blueprint $table){
            $table->increments('sf2id');
            $table->text('capital_market');
            $table->text('surname');
            $table->text('othernames');
            $table->text('alias');
            $table->text('residential_address');
            $table->text('office_address');
            $table->text('official_position');
            $table->text('employment');
            $table->text('email');
            $table->text('dob');
            $table->text('pob');
            $table->text('sob');
            $table->text('nationality');
            $table->text('soo');
            $table->text('number');
            $table->text('sex');
            $table->text('marital_status');
            $table->text('visible_mark');
            $table->text('license_no');
            $table->text('fresh');
            $table->text('poi');
            $table->text('doi');
            $table->text('doe');
            $table->text('father_name');
            $table->text('mother_name');
            $table->text('maiden_name');
            $table->text('spouse_name');
            $table->text('employment_spouse');
            $table->text('name_employer');
            $table->text('employer_address');
            $table->text('banker_name');
            $table->text('banker_address');
            $table->text('branch');
            $table->text('account_type');
            $table->text('account_number');
            $table->text('date_op');
            $table->text('name_of_employer');
            $table->text('emp_address');
            $table->timestamp('date_from')->nullable();
            $table->timestamp('date_to')->nullable();
            $table->text('part_full');
            $table->text('position_held');
            $table->text('pre_name_employer');
            $table->text('pre_address');
            $table->timestamp('pre_date_from')->nullable();
            $table->timestamp('pre_date_to')->nullable();
            $table->text('pre_part_full');
            $table->text('pre_position_held');
            $table->text('pre_reasons_leaving');
            $table->text('edu_institution');
            $table->text('edu_address');
            $table->timestamp('edu_date_from')->nullable();
            $table->timestamp('edu_date_to')->nullable();
            $table->text('edu_course_study');
            $table->text('edu_qualification_obtained');
            $table->text('awarding_institute');
            $table->text('certificate');
            $table->timestamp('year')->nullable();
            $table->timestamp('bro_date_exam')->nullable();
            $table->text('subject');
            $table->text('test_scores');
            $table->text('ref_name');
            $table->text('ref_address');
            $table->text('ref_occupation');
            $table->enum('registration',array('yes','no'));
            $table->text('registration_details');
            $table->enum('registration2',array('yes','no'));
            $table->text('registration_details2');
            $table->enum('registration3',array('yes','no'));
            $table->text('registration_details3');
            $table->enum('registration4',array('yes','no'));
            $table->text('registration_details4');
            $table->enum('change_of_name',array('yes','no'));
            $table->text('change_of_name_details');
            $table->enum('offences1',array('yes','no'));
            $table->text('offences1_details');
            $table->enum('offences2',array('yes','no'));
            $table->text('offences2_details');
            $table->enum('judgement',array('yes','no'));
            $table->integer('judgement_copy')->nullable(); // attachment
            $table->text('judgement_details');
            $table->enum('surety',array('yes','no'));
            $table->text('surety_details');
            $table->text('business_activities1');
            $table->text('business_activities2');
            $table->text('business_activities3');
            $table->enum('investment_adviser',array('yes','no'));
            $table->text('investment_adviser_details');
            $table->enum('number14a',array('yes','no'));
            $table->text('number14a_details');
            $table->enum('number14b',array('yes','no'));
            $table->text('number14b_details');
            $table->enum('number15',array('yes','no'));
            $table->text('number15_details');
            $table->enum('number16',array('yes','no'));
            $table->text('number16_details');
            $table->text('affidavit_name');
            $table->text('affidavit_of');
            $table->text('first_input');
            $table->text('second_input');
            $table->text('third_input');
            $table->text('fourth_input');
            $table->text('fifth_input');
            $table->integer('despondent_sig')->nullable(); // attachment
            $table->string('sponsoring_firm');
            $table->integer('applicant_sig')->nullable(); // attachment
            $table->integer('director_sig')->nullable(); // attachment
            $table->timestamps();
        });

        Schema::create('sec-form-2A', function(Blueprint $table){
           $table->increments('sf2aid');
           $table->text('applicant_company');
           $table->text('applicant_company_address');
           $table->text('officer_replaced_name');
           $table->text('current_reg');
           $table->text('officer_replaced_period_service');
           $table->text('reasons_for_leaving');
           $table->text('comment_on_integrity');
           $table->text('sub_officer_name');
           $table->text('reg_sought');
           $table->text('sub_officer_period_service');
           $table->enum('form_sec2_2b_submitted',array('yes','no'));
           $table->integer('sig')->nullable(); // attachment
           $table->text('designation');
           $table->text('director_name');
           $table->integer('director_sig')->nullable(); // attachment
           $table->text('secretary_name');
           $table->integer('sec_sig')->nullable(); // attachment
           $table->integer('company_seal')->nullable(); // attachment
           $table->text('affidavit_name');
           $table->text('dir_sec');
           $table->text('affidavit_of');
           $table->string('first_input');
           $table->string('second_input');
           $table->string('third_input');
           $table->string('fourth_input');
           $table->string('fifth_input');
           $table->integer('despondent_sig')->nullable(); // attachment
           $table->timestamps();
        });

        Schema::create('sec-form-2B', function(Blueprint $table){
            $table->increments('sf2bid');
            $table->text('applicant_name');
            $table->text('applicant_reg_as');
            $table->text('res_address');
            $table->text('employer_name');
            $table->text('bus_address');
            $table->enum('applying_for',array('transfer','change_of_status'));
            $table->text('trans_previous_employer')->nullable();
            $table->timestamp('date_of_resignation')->nullable();
            $table->integer('resig_letter')->nullable(); //attachment
            $table->text('reg_sought');
            $table->text('sub_officer_period_service');
            $table->text('applicable_exam');
            $table->timestamp('date_passed')->nullable();
            $table->text('proof_passing');
            $table->timestamp('date_signed')->nullable();
            $table->integer('applicant_sig')->nullable(); //attachment
            $table->text('director_name');
            $table->integer('dir_sig')->nullable(); //attachment
            $table->text('affidavit_name');
            $table->text('affidavit_of');
            $table->text('first_input');
            $table->text('second_input');
            $table->text('third_input');
            $table->text('fourth_input');
            $table->text('fifth_input');
            $table->enum('refusal',array('yes','no'));
            $table->enum('change_of_name',array('yes','no'));
            $table->enum('offences',array('yes','no'));
            $table->enum('civil_procedures',array('yes','no'));
            $table->enum('bankruptcy',array('yes','no'));
            $table->enum('judgement',array('yes','no'));
            $table->enum('surety',array('yes','no'));
            $table->enum('business_activities',array('yes','no'));
            $table->integer('despondent_sig')->nullable(); // attachment
            $table->timestamps();

        });

        Schema::create('sec-form-2D', function(Blueprint $table){
            $table->increments('sf2did');
            $table->string('name');
            $table->text('sponsoring_company');
            $table->text('res_address');
            $table->text('perm_address');
            $table->text('mail_address');
            $table->text('official_email');
            $table->text('personal_email');
            $table->text('gsm_number')->nullable();
            $table->text('landline_number')->nullable();
            $table->text('dob');
            $table->text('pob');
            $table->text('sob');
            $table->text('nationality');
            $table->text('sex');
            $table->text('marital_status');
            $table->text('next_kin_name');
            $table->text('next_kin_number');
            $table->text('next_kin_address');
            $table->text('next_kin_relationship');
            $table->text('next_kin_other')->nullable();
            $table->enum('q1',array('yes','no'));
            $table->text('q1_details');
            $table->enum('q2',array('yes','no'));
            $table->text('q2_details');
            $table->enum('q3',array('yes','no'));
            $table->text('q3_details');
            $table->enum('q4',array('yes','no'));
            $table->text('q4_details');
            $table->enum('q5',array('yes','no'));
            $table->text('q5_details');
            $table->enum('q6',array('yes','no'));
            $table->text('q6_details');
            $table->enum('q7',array('yes','no'));
            $table->text('q7_details');
            $table->enum('q8',array('yes','no'));
            $table->text('q8_details');
            $table->enum('q9',array('yes','no'));
            $table->text('q9_details');
            $table->enum('q10',array('yes','no'));
            $table->text('q10_details');
            $table->enum('q11',array('yes','no'));
            $table->text('q11_details');
            $table->enum('q12',array('yes','no'));
            $table->text('q12_details');
            $table->enum('q13',array('yes','no'));
            $table->text('q13_details');
            $table->enum('q14',array('yes','no'));
            $table->text('q14_details');
            $table->enum('q15',array('yes','no'));
            $table->text('q15_details');
            $table->enum('q16',array('yes','no'));
            $table->text('q16_details');
            $table->enum('q17',array('yes','no'));
            $table->text('q17_details');
            $table->enum('q18',array('yes','no'));
            $table->text('q18_details');
            $table->enum('q19',array('yes','no'));
            $table->text('q19_details');
            $table->enum('q20',array('yes','no'));
            $table->text('q20_details');
            $table->text('affidavit_name');
            $table->text('affidavit_address');
            $table->text('affidavit_full_name');
            $table->text('first_input');
            $table->text('second_input');
            $table->text('third_input');
            $table->text('fourth_input');
            $table->text('fifth_input');
            $table->integer('despondent_sig')->nullable(); // attachment
            $table->timestamps();

        });

        Schema::create('sec-form-3', function(Blueprint $table){
            $table->increments('sf3id');
            $table->string('reg_name');
            $table->text('state_company');
            $table->text('reg_address');
            $table->text('pre_address');
            $table->timestamp('date_incorporation')->nullable();
            $table->text('type_of_reg');
            $table->enum('q7',array('yes','no'));
            $table->text('q7_details');
            $table->enum('q8',array('yes','no'));
            $table->text('q8_details');
            $table->enum('q9',array('yes','no'));
            $table->text('q9_details');
            $table->text('authorized');
            $table->text('share_of_n1');
            $table->text('each_am1');
            $table->text('issued');
            $table->text('share_of_n2');
            $table->text('each_am2');
            $table->text('paid_up');
            $table->text('share_of_n3');
            $table->text('each_am3');

            $table->text('nig_name');
            $table->text('nig_address');
            $table->text('nig_am');
            $table->text('nig_held');

            $table->text('fore_name');
            $table->text('fore_address');
            $table->text('fore_am');
            $table->text('fore_held');


            $table->text('part_name');
            $table->text('part_address');
            $table->text('part_qua');
            $table->text('part_exp');
            $table->text('pre_employment')->nullable();
            $table->timestamp('pre_date')->nullable();
            $table->text('pre_reasons_leaving');

            $table->text('exec_name');
            $table->text('exec_qua');
            $table->timestamp('exec_qua_date')->nullable();
            $table->text('designation')->nullable();



            $table->text('ord_1');
            $table->text('ord_2');
            $table->text('pref_1');
            $table->text('pref_2');
            $table->text('deb_1');
            $table->text('deb_2');
            $table->text('gov_1');
            $table->text('gov_2');
            $table->text('oth_1');
            $table->text('oth_2');
            $table->text('mut_1');
            $table->text('mut_2');
            $table->text('open_1');
            $table->text('open_2');

            $table->text('agent_name');
            $table->text('agent_address');
            $table->text('agent_basis');

            $table->text('sho_1');
            $table->text('sho_2');
            $table->text('sho_3');
            $table->text('sho_4');
            $table->text('sho_5');

            $table->text('inv_1');
            $table->text('inv_2');
            $table->text('inv_3');
            $table->text('inv_4');
            $table->text('inv_5');

            $table->text('quo_1');
            $table->text('quo_2');
            $table->text('quo_3');
            $table->text('quo_4');
            $table->text('quo_5');

            $table->text('unquo_1');
            $table->text('unquo_2');
            $table->text('unquo_3');
            $table->text('unquo_4');
            $table->text('unquo_5');

            $table->text('oth_sho_1');
            $table->text('oth_sho_2');
            $table->text('oth_sho_3');
            $table->text('oth_sho_4');
            $table->text('oth_sho_5');

            $table->text('cred_1');
            $table->text('cred_2');
            $table->text('cred_3');
            $table->text('cred_4');
            $table->text('cred_5');

            $table->text('lia_1');
            $table->text('lia_2');
            $table->text('lia_3');
            $table->text('lia_4');
            $table->text('lia_5');

            $table->text('oth_lia_1');
            $table->text('oth_lia_2');
            $table->text('oth_lia_3');
            $table->text('oth_lia_4');
            $table->text('oth_lia_5');

            $table->text('dep_1');
            $table->text('dep_2');
            $table->text('dep_3');
            $table->text('dep_4');
            $table->text('dep_5');


            $table->enum('number16_i',array('yes','no'));
            $table->text('number16_i_details');
            $table->enum('number16_ii',array('yes','no'));
            $table->text('number16_ii_details');
            $table->text('number16_iii');

            $table->enum('q17',array('yes','no'));
            $table->text('q17_details');
            $table->enum('q18',array('yes','no'));
            $table->text('q18_details');
            $table->text('q19');
            $table->text('q20');
            $table->enum('q21',array('yes','no'));
            $table->text('q21_details');
            $table->enum('q22',array('yes','no'));
            $table->text('q22_details');
            $table->enum('q23',array('yes','no'));
            $table->text('q23_details');
            $table->enum('q24_i',array('yes','no'));
            $table->enum('q24_ii',array('yes','no'));
            $table->enum('q24_iii',array('yes','no'));
            $table->enum('q24_iv',array('yes','no'));
            $table->enum('q24_v',array('yes','no'));
            $table->text('q24_vi');
            $table->enum('q25',array('yes','no'));
            $table->text('q26_a_i');
            $table->text('q26_a_ii');
            $table->text('q26_a_iii');
            $table->text('q26_b');
            $table->text('q26_c_name');
            $table->text('q26_c_service');
            $table->text('q26_c_rendered');
            $table->text('q26_d_dispute');
            $table->text('q26_d_party');
            $table->text('q26_d_set');
            $table->text('q26_e');
            $table->text('q26_f');
            $table->text('q27_i_name');
            $table->text('q27_i_address');
            $table->enum('q27_ii',array('yes','no'));
            $table->text('q27_ii_details');
            $table->text('q27_iii');
            $table->text('q27_iv');
            $table->enum('q27_v',array('yes','no'));
            $table->text('q27_v_details');
            $table->text('q27_vi');


            $table->text('affidavit_name');
            $table->text('affidavit_address');
            $table->text('affidavit_full_name');
            $table->text('first_input');
            $table->text('second_input');
            $table->text('third_input');
            $table->text('fourth_input');
            $table->text('fifth_input');
            $table->text('commish');
            $table->integer('despondent_sig')->nullable(); // attachment
            $table->integer('audit')->nullable(); // attachment
            $table->integer('profit')->nullable(); // attachment
            $table->integer('schedule')->nullable(); // attachment
            $table->integer('cac')->nullable(); // attachment
            $table->integer('sworn')->nullable(); // attachment
            $table->integer('memo')->nullable(); // attachment
            $table->timestamps();


        });

        Schema::create('sec-form-3A', function(Blueprint $table){
            $table->increments('sf3aid');
            $table->string('reg_name');
            $table->text('reg_address');
            $table->text('mail_address');
            $table->text('fax_no');
            $table->text('email');
            $table->text('website');
            $table->text('telephone');
            $table->text('state_if');
            $table->timestamp('date_incorporation')->nullable();
            $table->text('object');
            $table->enum('b_i',array('yes','no'));
            $table->text('b_ii');
            $table->text('q6');
            $table->enum('q_6a',array('yes','no'));
            $table->enum('q_6b',array('yes','no'));
            $table->text('promoter_name');
            $table->text('promoter_address');

            $table->text('nig_name');
            $table->text('nig_address');
            $table->text('nig_am');
            $table->text('nig_held');

            $table->text('fore_name');
            $table->text('fore_address');
            $table->text('fore_am');
            $table->text('fore_held');

            $table->text('pro_name');
            $table->text('pro_date_from');
            $table->text('pro_date_to');
            $table->text('pro_course');
            $table->text('pro_qua');

            $table->enum('q8_b_i',array('yes','no'));
            $table->text('q8_b_i_details');

            $table->enum('q8_b_ii',array('yes','no'));
            $table->text('q8_b_ii_details');


            $table->text('authorized');
            $table->text('share_of_n1');
            $table->text('each_am1');
            $table->text('issued');
            $table->text('share_of_n2');
            $table->text('each_am2');
//            $table->text('paid_up');
            $table->text('share_of_n3');
            $table->text('each_am3');

            $table->text('nig_name2');
            $table->text('nig_address2');
            $table->text('nig_am2');
            $table->text('nig_held2');

            $table->text('fore_name2');
            $table->text('fore_address2');
            $table->text('fore_am2');
            $table->text('fore_held2');

            $table->text('part_name');
            $table->text('part_address');
            $table->text('part_qua');
            $table->text('part_exp');
            $table->text('pre_employment')->nullable();
            $table->timestamp('pre_date')->nullable();
            $table->text('pre_share_comp');
            $table->text('pre_directorship');


            $table->text('exec_name');
            $table->text('exec_designation');
            $table->text('exec_qua');
            $table->text('exec_position_held');
            $table->text('exec_exp');
            $table->timestamp('exec_date_ap')->nullable();
            $table->text('exec_function_area');

            $table->text('rating_name');
            $table->text('rating_focus');
            $table->text('rating_exp');
            $table->text('rating_related_exp');

            $table->text('q14a');
            $table->text('q14b');
            $table->text('q14c');
            $table->text('q14d');


            $table->text('q15_name');
            $table->text('q15_address');
            $table->text('q15_nature_of_business');


            $table->text('q15_i');
            $table->text('q15_ii');
            $table->text('q15_iii');
            $table->text('q15_iv');


            $table->text('q16a');
            $table->text('q16b');
            $table->text('q16c');
            $table->text('q16d');

            $table->text('q17a');
            $table->text('q17b');
            $table->text('q17c');
            $table->text('q17d');

            $table->enum('q18',array('yes','no'));
            $table->text('q18_details');
            $table->enum('q19',array('yes','no'));
            $table->text('q19_details');
            $table->enum('q20',array('yes','no'));
            $table->text('q20_details');
            $table->enum('q21',array('yes','no'));
            $table->text('q21_details');
            $table->enum('q22',array('yes','no'));
            $table->text('q22_details');
            $table->enum('q23',array('yes','no'));
            $table->text('q23_details');
            $table->enum('q24',array('yes','no'));
            $table->text('q24_details');
            $table->enum('q25',array('yes','no'));
            $table->text('q25_details');
            $table->text('q26_i');
            $table->text('q26_ii');
            $table->text('q26_iii');
            $table->text('q26_iv');
            $table->text('q26_v');
            $table->enum('q26_vi',array('yes','no'));
            $table->text('q26_vi_details');
            $table->enum('q27',array('yes','no'));
            $table->text('q27_details');

            $table->text('ord_1');
            $table->text('ord_2');
            $table->text('pref_1');
            $table->text('pref_2');
            $table->text('deb_1');
            $table->text('deb_2');
            $table->text('gov_1');
            $table->text('gov_2');
            $table->text('oth_1');
            $table->text('oth_2');

            $table->text('mut_1');
            $table->text('mut_2');
            $table->text('open_1');
            $table->text('open_2');

            $table->text('first_company');
            $table->text('second_company');
            $table->text('third_company');

            $table->text('debt_1');
            $table->text('debt_2');
            $table->text('debt_3');
            $table->text('debt_4');
            $table->text('debt_5');

            $table->text('sho_1');
            $table->text('sho_2');
            $table->text('sho_3');
            $table->text('sho_4');
            $table->text('sho_5');

            $table->text('inv_1');
            $table->text('inv_2');
            $table->text('inv_3');
            $table->text('inv_4');
            $table->text('inv_5');

            $table->text('quo_1');
            $table->text('quo_2');
            $table->text('quo_3');
            $table->text('quo_4');
            $table->text('quo_5');

            $table->text('unquo_1');
            $table->text('unquo_2');
            $table->text('unquo_3');
            $table->text('unquo_4');
            $table->text('unquo_5');

            $table->text('oth_sho_1');
            $table->text('oth_sho_2');
            $table->text('oth_sho_3');
            $table->text('oth_sho_4');
            $table->text('oth_sho_5');

            $table->text('cred_1');
            $table->text('cred_2');
            $table->text('cred_3');
            $table->text('cred_4');
            $table->text('cred_5');

            $table->text('lia_1');
            $table->text('lia_2');
            $table->text('lia_3');
            $table->text('lia_4');
            $table->text('lia_5');

            $table->text('oth_lia_1');
            $table->text('oth_lia_2');
            $table->text('oth_lia_3');
            $table->text('oth_lia_4');
            $table->text('oth_lia_5');

            $table->text('dep_1');
            $table->text('dep_2');
            $table->text('dep_3');
            $table->text('dep_4');
            $table->text('dep_5');

            $table->text('q29_i_name');
            $table->text('q29_i_address');


            $table->enum('q29_ii',array('yes','no'));
            $table->text('q29_ii_details');
            $table->enum('q29_iii',array('yes','no'));
            $table->text('q29_iii_details');
            $table->text('q29_a');
            $table->text('q29_b');
            $table->text('q29_c');
            $table->text('q29_iv');

            $table->enum('q29_v',array('yes','no'));
            $table->text('q29_v_details');

            $table->enum('q29_vi',array('yes','no'));
            $table->text('q29_vi_details');

            $table->text('q30_name');
            $table->text('q30_address');


            $table->text('res_1');
            $table->text('res_2');
            $table->text('total_1');
            $table->text('total_2');
            $table->text('acc_1');
            $table->text('acc_2');
            $table->text('dif_1');
            $table->text('dif_2');
            $table->text('net_1');
            $table->text('net_2');
            $table->text('reason_1');

            $table->text('affidavit_name');
            $table->text('affidavit_address');
            $table->text('first_input');
            $table->text('second_input');
            $table->text('third_input');
            $table->text('fourth_input');
            $table->text('fifth_input');
            $table->text('commish');
            $table->integer('despondent_sig')->nullable(); // attachment
            $table->integer('cac')->nullable(); // attachment
            $table->integer('memo')->nullable(); // attachment
            $table->integer('pro')->nullable(); // attachment
            $table->integer('under')->nullable(); // attachment
            $table->integer('bye_laws')->nullable(); // attachment
            $table->integer('sworn')->nullable(); // attachment
            $table->integer('chart')->nullable(); // attachment

            $table->timestamps();


        });

        Schema::create('attachments', function(Blueprint $table){
            $table->increments('atid');
            $table->string('name')->nullable();
            $table->string('url');
            $table->timestamps();

        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('application_updates');
        Schema::dropIfExists('users');
        Schema::dropIfExists('confirmations');
        Schema::dropIfExists('companies');
        Schema::dropIfExists('company_functions');
        Schema::dropIfExists('user_profile');
        Schema::dropIfExists('user_companies');
        Schema::dropIfExists('functions');
        Schema::dropIfExists('function_forms');
        Schema::dropIfExists('applications');
        Schema::dropIfExists('application_functions');
        Schema::dropIfExists('application_submissions');
        Schema::dropIfExists('application_documents');
        Schema::dropIfExists('payments');
        Schema::dropIfExists('sec-form-2');
        Schema::dropIfExists('sec-form-2A');
        Schema::dropIfExists('sec-form-2B');
        Schema::dropIfExists('sec-form-2D');
        Schema::dropIfExists('sec-form-3');
        Schema::dropIfExists('sec-form-3A');
        Schema::dropIfExists('attachments');


    }
}
