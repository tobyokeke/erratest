@extends('cmo.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard.css')}}">
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard_theme_dots.css')}}">
    <style>



    </style>
@endsection

@section('scripts')
    <script src="{{url('admin/assets/js/formvalidation.js')}}"></script>

@endsection

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Users</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Users</a></li>
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">
        <div class="col-lg-12 col-xl-6 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Add A Sponsored Individual</h4>
                </div>
                <div class="card-body users">

                    All users you invite would be sent a link to complete their registration. Once complete,
                    they would be added to your company
                    <div class="form-group row">
                        <label for="inputName" class="col-md-6 col-form-label bold-label">Number of Staff in your company</label>
                        <div class="col-md-6 user-details">
                            0
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-xl-6 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Invite a User</h4>
                </div>
                <div class="card-body">
                    <form method="post" id="vertical-validation"
                          action="{{route('cmo.users.add.post')}}"
                          novalidate>

                        @csrf

                        <div class="form-group">
                            <div class="form-group overflow-hidden">
                                <label>Role</label>
                                <select name="role" class="form-control select2 w-100" >
                                    <option >Branch Office Head</option>
                                    <option >Executive Director</option>
                                    <option >Independent Director</option>
                                    <option >Managing Director</option>
                                    <option >Non-Executive Director</option>
                                    <option >Sponsored Individual</option>
                                </select>
                            </div>
                        </div>

                        <div class="">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Firstname</label>
                                <input type="text" name="fname" class="form-control"  placeholder="Enter firstname" required>
                                <div class="invalid-feedback">
                                    Please Enter Your Firstname
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Surname</label>
                                <input type="text" name="sname" class="form-control"  placeholder="Enter surname" required>
                                <div class="invalid-feedback">
                                    Please Enter Your Surname
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" name="email" class="form-control"  placeholder="Enter email" required>
                                <div class="invalid-feedback">
                                    Please Enter Valid Email Address
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" name="phone" minlength="11" maxlength="11" class="form-control"  placeholder="Enter phone" required>
                                <div class="invalid-feedback">
                                    Please Enter Valid Phone Number
                                </div>
                            </div>



                        </div>
                        <button type="submit" class="btn btn-primary mt-1 mb-0">Invite</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--row closed-->

@endsection
