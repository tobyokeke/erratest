@extends('cmo.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Users</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Users</a></li>
        </ol>
    </div>
    <!--page-header closed-->



    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>SPONSORED INDIVIDUALS</h4>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless text-nowrap mb-0">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Registered</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->role}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->created_at->diffForHumans()}}</td>
                                    <td>
                                        <a href="{{route('admin.users.details',['user' => $user->uid])}}">
                                            <span class="badge badge-pill badge-primary">View</span>
                                        </a>
                                    </td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach

                            </tbody>
                        </table>

                        {{$users->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row closed-->


@endsection
