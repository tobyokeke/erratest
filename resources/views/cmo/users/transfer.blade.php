@extends('cmo.layouts.app')

@section('styles')
    <style >
        .amount{
            font-size: 60px;
            color:green;
        }

        .functions{
            font-size:14px;
        }
        .functions .row{
            margin-bottom: 10px !important;
        }
        .hidden{
            display:none !important;
        }
    </style>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios@0.18.0/dist/axios.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

@endsection

@section('scripts')
    <script src="{{url('assets/js/accordiation.js')}}"></script>
@endsection

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Users</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Users</a></li>
        </ol>
    </div>
    <!--page-header closed-->



    <!--row open-->
    <div class="row" id="app" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Transfer of Sponsored Individuals</h4>

                </div>
                <div class="card-body">

                    <p><b>THE PROCESS</b></p>
                    <p>

                        You would be required to fill out two forms. SEC 2a and SEC 2b and attach the required documents as specified
                        below. After you fill these out, a letter would be sent your previous employer for verification.

                    </p>


                    <!--start second row (requirements)-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>REQUIREMENTS</h4>
                                </div>
                                <div class="card-body">


                                    <div class="acc-1 js-accordion  ">

                                            <div class="accordion__item js-accordion-item active " id="sec2a">
                                                <div class="accordion-header js-accordion-header">1. SEC FORM 2a</div>
                                                <div class="accordion-body js-accordion-body">
                                                    <div class="accordion-body__contents">
                                                        You need to fill this form
                                                        <a class="btn btn-secondary mt-2 mb-2" disabled="" href="{{route('cmo.applications.sec-form-2A')}}">FILLED</a>

                                                    </div>
                                                </div>
                                            </div>


                                            <div class="accordion__item js-accordion-item active " id="sec2b">
                                                <div class="accordion-header js-accordion-header">1. SEC FORM 2b</div>
                                                <div class="accordion-body js-accordion-body">
                                                    <div class="accordion-body__contents">
                                                        You need to fill this form
                                                        <a class="btn btn-warning mt-2 mb-2" href="{{route('cmo.applications.sec-form-2B')}}">FILL NOW</a>

                                                    </div>
                                                </div>
                                            </div>

                                    </div>
                                </div>
                                <div class="card-footer">
                                    <h4>ATTACH DOCUMENTS</h4>
                                    <input type="file">
                                    <a class="btn btn-success" style="color:white;" >SUBMIT</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end second row-->


                </div>
            </div>
        </div>
    </div>
    <!--row closed-->



@endsection
