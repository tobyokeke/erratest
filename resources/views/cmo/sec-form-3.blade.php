@extends('cmo.layouts.app')

@section('content')
    <style>
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Form Sec 3</h4>
            <p style="color:white;">Application form for the registration of broker/dealer, Corporate Investment Adviser,Fund/Portfolio Managers and other Corporate Entities under the investments and securities Act 2007.</p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->

    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                <form method="post" enctype="multipart/form-data" action="{{route('cmo.applications.post-sec-form-3')}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col">
                            <label>1. Registered Name </label>
                            <input class="form-control" name="reg_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>2. State if Private/Public Company/Partnership</label>
                            <input class="form-control" name="state_company" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>3. Registered Address </label>
                            <input class="form-control" name="reg_address" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>4. Present Address (if different from 3.)</label>
                            <input class="form-control" name="pre_address" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>5. Date of Incorporation/Registration </label>
                            <input class="form-control" name="date_incorporation" type="date" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>6. Type of Registration (Tick which ever is applicable)</label>
                            <input name="type_of_reg">
{{--                                     <p>Issuing House <input type="checkbox"></p>--}}
{{--                                     <p>Broker/Dealer <input type="checkbox"></p>--}}
{{--                                     <p>Investment Adviser (Corporate entity) <input type="checkbox"></p>--}}
{{--                                     <p>Fund/ Portfolio Managers <input type="checkbox"></p>--}}
{{--                                     <p>Underwriters <input type="checkbox"></p>--}}
{{--                                     <p>Banker to an issue <input type="checkbox"></p>--}}
                             <input class="form-control" type="text" placeholder="Any Other Category (Specify)">


                        </div>
                    </div><br>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>7. Is the Company/firm engaged in other activities? </p><br>
                            <textarea type="text" name="q7_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q7" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>8. Have you any branch Office? </p><br>
                            <textarea type="text" name="q8_details" class="form-control" placeholder="If yes, state particulars"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q8" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>9. Are you applying for registration of any branch? </p><br>
                            <textarea type="text" name="q9_details" class="form-control" placeholder="If yes, give particulars"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q9" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <h6>10. Capital Structure</h6>

                    <div class="row">
                        <div class="form-group col">
                            <label>i. Authorized </label>
                            <input class="form-control" name="authorized" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Shares of N </label>
                            <input class="form-control" name="share_of_n1" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Each amounting to N</label>
                            <input class="form-control" name="each_am1" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>ii. Issued</label>
                            <input class="form-control" name="issued" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Shares of N </label>
                            <input class="form-control" name="share_of_n2" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Each amounting to N  </label>
                            <input class="form-control" name="each_am2" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>iii. Paid up</label>
                            <input class="form-control" name="paid_up" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Shares of N </label>
                            <input class="form-control" name="share_of_n3" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Each amounting to N  </label>
                            <input class="form-control" name="each_am3" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>11. Names and Addresses of Members/Partners holding 5% or<br> more of the capital of the Firm/Company</h6>

                    <p>(a) Nigerian</p><br>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="nig_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="nig_address" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Amount (N)</label>
                            <input class="form-control" name="nig_am" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>% Held</label>
                            <input class="form-control" name="nig_held" type="text" placeholder="">
                        </div>

                    </div><br>

                    <p>(b) Foreign</p><br>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="fore_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="fore_address" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Amount (N)</label>
                            <input class="form-control" name="fore_am" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>% Held</label>
                            <input class="form-control" name="fore_held" type="text" placeholder="">
                        </div>

                    </div><br>


                    <h6>12. Particulars of Directories/Partners</h6>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="part_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="part_address" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Qualification</label>
                            <input class="form-control" name="part_qua" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Experience</label>
                            <input class="form-control" name="part_exp" type="text" placeholder="">
                        </div>

                    </div><br>


                    <div class="row">
                        <div class="form-group col">
                            <label>Previous Employment </label>
                            <input class="form-control" name="pre_employment" type="text" placeholder="If any">
                        </div>
                        <div class="form-group col">
                            <label>Date</label>
                            <input class="form-control" name="pre_date" type="date" placeholder="If any">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Reasons for Leaving</label>
                            <textarea class="form-control" name="pre_reasons_leaving" type="text" placeholder=""></textarea>
                        </div>

                    </div><br>

                    <h6>13. Particulars of Executive and Senior Staff</h6>

                    <div class="row">
                        <div class="form-group col-md-5">
                            <label>Name </label>
                            <input class="form-control" type="text" name="exec_name" placeholder="">
                        </div>
                        <div class="form-group col-md-5">
                            <label>Qualifications </label>
                            <input class="form-control" name="exec_qua" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label>Qualification date </label>
                            <input class="form-control" name="exec_qua_date" type="date" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Designation</label>
                            <input class="form-control" name="designation" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>14. (i) State the Nominal and Market values of your investments<br>
                        securities as at the date of this application</h6>
                    <table border="1" style="table-layout: fixed; width: 100%;">

                        <tr>
                            <th scope="col" class="text-center">Security</th>
                            <th scope="col" class="text-center">Nominal Value (N)</th>
                            <th scope="col" class="text-center">Market Value (N)</th>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Ordinary Shares</th>
                            <td><input name="ord_1" type="text"></td>
                            <td><input name="ord_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Preferences Shares</th>
                            <td><input name="pref_1" type="text"></td>
                            <td><input name="pref_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Debentures</th>
                            <td><input name="deb_1" type="text"></td>
                            <td><input name="deb_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Government Bonds</th>
                            <td><input name="gov_1" type="text"></td>
                            <td><input name="gov_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Other Fixed Interest Bearing Securities</th>
                            <td><input name="oth_1" type="text"></td>
                            <td><input name="oth_2" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Mutual Funds</th>
                            <td><input name="mut_1" type="text"></td>
                            <td><input name="mut_2" type="text"></td>

                        </tr>

                        <tr>
                            <th scope="row" class="text-center">(Open-ended)</th>
                            <td><input name="open_1" type="text"></td>
                            <td><input name="open_2" type="text"></td>

                        </tr>



                    </table><br>

                    <p>(ii) Please furnish details of commission sharing agreements(s) (if any) you have entered into:</p>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name of Agent</label>
                            <input class="form-control" name="agent_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="agent_address" type="text" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>Basis of Renumeration</label>
                            <input class="form-control" name="agent_basis" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>15.State the build-up of the following items over the last five years or since incorporation if less than five years</h6>
                    <table border="1" style="table-layout: fixed; width: 100%;">

                        <tr>
                            <th scope="col" class="text-center">Debtors</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>



                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Short Term Loans (Assets)</th>
                            <td><input name="sho_1" type="text"></td>
                            <td><input name="sho_2" type="text"></td>
                            <td><input name="sho_3" type="text"></td>
                            <td><input name="sho_4" type="text"></td>
                            <td><input name="sho_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Investments (At year and market)</th>
                            <td><input name="inv_1" type="text"></td>
                            <td><input name="inv_2" type="text"></td>
                            <td><input name="inv_3" type="text"></td>
                            <td><input name="inv_4" type="text"></td>
                            <td><input name="inv_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Quoted</th>
                            <td><input name="quo_1" type="text"></td>
                            <td><input name="quo_2" type="text"></td>
                            <td><input name="quo_3" type="text"></td>
                            <td><input name="quo_4" type="text"></td>
                            <td><input name="quo_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Unquoted</th>
                            <td><input name="unquo_1" type="text"></td>
                            <td><input name="unquo_2" type="text"></td>
                            <td><input name="unquo_3" type="text"></td>
                            <td><input name="unquo_4" type="text"></td>
                            <td><input name="unquo_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Other Short Term Assets</th>
                            <td><input name="oth_sho_1" type="text"></td>
                            <td><input name="oth_sho_2" type="text"></td>
                            <td><input name="oth_sho_3" type="text"></td>
                            <td><input name="oth_sho_4" type="text"></td>
                            <td><input name="oth_sho_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Creditors</th>
                            <td><input name="cred_1" type="text"></td>
                            <td><input name="cred_2" type="text"></td>
                            <td><input name="cred_3" type="text"></td>
                            <td><input name="cred_4" type="text"></td>
                            <td><input name="cred_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Short Term Loans (Liabilities)</th>
                            <td><input name="lia_1" type="text"></td>
                            <td><input name="lia_2" type="text"></td>
                            <td><input name="lia_3" type="text"></td>
                            <td><input name="lia_4" type="text"></td>
                            <td><input name="lia_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Other Short term Liabilities</th>
                            <td><input name="oth_lia_1" type="text"></td>
                            <td><input name="oth_lia_2" type="text"></td>
                            <td><input name="oth_lia_3" type="text"></td>
                            <td><input name="oth_lia_4" type="text"></td>
                            <td><input name="oth_lia_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Client Deposit</th>
                            <td><input name="dep_1" type="text"></td>
                            <td><input name="dep_2" type="text"></td>
                            <td><input name="dep_3" type="text"></td>
                            <td><input name="dep_4" type="text"></td>
                            <td><input name="dep_5" type="text"></td>


                        </tr>


                    </table><br>


                    <h6>16. ANSWER THE FOLLOWING QUESTION</h6><br>


                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>i.  Has the applicant to the best of its information and believe any affiliate? </p><br>
                            <textarea type="text" name="number16_i_details" class="form-control" placeholder="If yes, give details (List of names, addresses, nature of affiliate and other details)"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="number16_i" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>ii. Is there subsisting between you and any of the affiliate any technical/management agreement??</p><br>
                            <textarea type="text" name="number16_ii_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="number16_ii" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>iii. State of basis of fees charged</p><br>
                            <textarea name="number16_iii" type="text" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>17. Has the applicant or any its affiliates been denied registration or expelled from From membership of any government agency, Securities Exchange or Association
                                of Securities Dealers or any other SRO?</p><br>
                            <textarea type="text" name="q17_details" class="form-control" placeholder="If yes, give reasons"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q17" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>18. Has your membership or that of any affiliates in any of the institutions mentioned In (17) at any time been revoked, suspended or withdrawn?</p><br>
                            <textarea type="text" name="q18_details" class="form-control" placeholder="If yes, give reasons"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q18" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>19. Has the applicant or any of its affiliates, to the best of the applicant’s knowledge And believe operated under and carried on business under any name other than The name shown in this application?</p><br>
                            <textarea name="q19" type="text" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>20. Is any depository holding any of the assets of the Company/Firm? </p><br>
                            <textarea name="q20" type="text" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>21. Has any person or company undertaken to act as a guarantor in relation to the financial or other undertakings of the applicant?</p><br>
                            <textarea type="text" name="q21_details" class="form-control" placeholder="If yes,give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q21" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>22. Has a subordinated agreement been executed by the creditor(s) in relation to loans owing by the applicant?</p><br>
                            <textarea type="text" name="q22_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q22" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>23. Has your company/firm or any of its affiliates been subject to any winding up order/receivership arrangement?</p><br>
                            <textarea type="text" name="q23_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q23" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <h6>24. Has your company/firm or any of its affiliates been involved in any litigation within Ten preceding this application over.</h6>

                    <div class="row bottom-space">


                        <div class="form-group col-md-6">
                            <p>i. filing of any application for registration?</p><br>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q24_i" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>ii. purchase or sale of securities?</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q24_ii" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>iii. any offence relating to the code of the business of Broker/Dealer, Investment Adviser, Bank or Insurance?	</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q24_iii" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>iv.  any criminal offence?</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q24_iv" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>v. any civil offence?</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q24_v" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>Where the answer of any of the items 24(i)-(v) is yes give details</p><br>
                            <textarea type="text" name="q24_vi" class="form-control" placeholder=""></textarea>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>25.  Has your firm or any of its affiliates been refused any fidelity bond?</p><br>
                            <textarea type="text" name="q25_details" class="form-control" placeholder="If yes give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q25" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <h6>26. SPECIFIC INFORMATION AS TO BANKER TO AN ISSUE</h6>

                    <p>(a) Business information</p>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>(i) Indicate type of activities carried on/proposed to be carried on ( collection bankers for application money and/or allotment money, reformed banker paying bankers for payment of dividend/interest)?</p><br>
                            <textarea type="text" name="q26_a_i" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>(ii) Describe modalities for handling application, processing them and for co-ordination between branches, remittance of money, collection and accounting for funds</p><br>
                            <textarea type="text" name="q26_a_ii" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>(iii) Enclose a copy of typical contract with the client for the service rendered</p><br>
                            <textarea type="text" name="q26_a_iii" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <p>(b) Experience</p>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>Experience as bankers to the issue (period to be indicated) services provided during previous years</p><br>
                            <textarea type="text" name="q25_b" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <p>(c). List of clients (corporate clients only) for specific activity</p>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="q25_c_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Service</label>
                            <input class="form-control" name="q25_c_service" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Rendered</label>
                            <input class="form-control" name="q25_c_rendered" type="text" placeholder="">
                        </div>

                    </div><br>

                    <p>(d). Details of all and pending dispute for specific activity of banker to an issue</p>

                    <div class="row">
                        <div class="form-group col">
                            <label>Nature of Dispute</label>
                            <input class="form-control" name="q26_d_dispute" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Name of party</label>
                            <input class="form-control" name="q26_d_party" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Pending/Settled</label>
                            <input class="form-control" name="q26_d_set" type="text" placeholder="">
                        </div>

                    </div><br>

                    <p>(e). Income from specific activity of banker to an issue</p>

                    <div class="row">
                        <div class="form-group col">
                            <label></label>
                            <input class="form-control" name="q26_e" type="text" placeholder="">
                        </div>

                    </div><br>

                    <p>(f). Any other information considered relevant to the nature of services rendered by the company</p>

                    <div class="row">
                        <div class="form-group col">
                            <label></label>
                            <input class="form-control" name="q26_f" type="text" placeholder="">
                        </div>

                    </div><br>

                    <p>27. ANNUAL AUDIT OF THE FIRM/COMPANY/PARTNERSHIP</p>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>i. State the name(s) and address(es) of the auditor(s) of the applicant</p><br>
                            <input type="text" name="q27_i_name" class="form-control" placeholder="Name">
                            <input type="text" name="q27_i_address" class="form-control" placeholder="Address">
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>ii. Has there been any change over the past five years?</p><br>
                            <textarea type="text" name="q27_ii_details" class="form-control" placeholder=".If so ,why?"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q27_ii" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>iii. Is the auditing firm, to the best of your knowledge and believe, also responsible for the auditing of any of your affiliates?</p><br>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>iv. State the basis of remuneration for services rendere</p><br>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>v. Has there been any adverse comment by the auditors on the value/mix of the securities in your investment portfolio over the past five years?</p><br>
                            <textarea type="text" name="q27_v_details" class="form-control" placeholder=".If so ,why?"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q27_v" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>


                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>vi. Is the auditing firm or any of the partners or principal officers, to the best of your knowledge and believe, holding any substantial securities in the companies within your investment portfolio, or is any of them holding a directorate seat in the company?</p><br>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>


                    <h5 class="text-center">AFFIDAVIT</h5>
                    <p class="text-center">
                        I………………………………………………………………………………………………<br>	Name	in	Full

                        <br>Residing at……………………………………………………………<br>



                    </p>
                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_name" type="text" placeholder="Full Name">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_address" type="text" placeholder="Address">
                        </div>

                    </div><br>


                    <p>
                        Hereby make an Oath and state as follows: <br>
                        (1) That I am Director/Partner/officer of the applicant company/firm<br>
                        (2) That I am duly authorized by the company/firm to sign this application;<br>
                        (3) That the statements of facts  made in this application and exhibit attached thereto are to the best of my knowledge and believe true.<br>
                        <span class="ml-auto">Despondent<br>………………………………………………</span><br>
                        <span class="text-center">
                              SWORN at the………………………………………………………court this……………………………day
                              Of………………………………………………year………………………………………………………………<br>
                              BEFORE ME<br>
                              ………………………………………………………………<br>
                              COMMISSIONER OF OATHS<br>
                            </span>

                    </p>

                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" name="first_input" type="text" placeholder="Sworn at the">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="second_input" type="text" placeholder="court this">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="third_input" type="text" placeholder="day of">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="fourth_input" type="text" placeholder="year">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="fifth_input" type="text" placeholder="Before me">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="commish" type="text" placeholder="Commissioner of oaths">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload Despondent Signature</label><br>
                            <input type="file" name="despondent_sig" class="" id="customFile">
                        </div>
                    </div><br>


                    <h6>i. Each	application	filed must be accompanied by</h6>

                    <div class="row">

                        <div class="form-group col">
                            <label class="" for="customFile">(a) Upload a audited financial statements for the latest one year</label><br>
                            <input type="file" name="audit" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">(b) Upload a detailed profit and loss for same period under (a);</label><br>
                            <input type="file" name="profit" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload separate schedule of securities owned by the applicant (if any)</label><br>
                            <input type="file" name="schedule" class="" id="customFile">
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label class="" for="customFile">Upload a copy of the certificate of Incorporation </label><br>
                            <input type="file" name="cac" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload Sworn	undertaking	to	keep	such	records	and	render	such	returns	as	may	be	specified	by	the	Commission	from
                                time	to	time</label><br>
                            <input type="file" name="sworn" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload a copy of the Memorandum and Article of <br>Association/Partnership Deed certified by the Corporate
                                Affair Commission </label><br>
                            <input type="file" name="memo" class="" id="customFile">
                        </div>
                    </div><br>

                    <h6>ii. An affiliate for the purpose of the Form is a company or Partnership in which the applicant has at
                        least 20% ownership interest however so called.</h6>


                    <div class="row text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>

                </form>

            </div>
        </div>
        <!--row closed-->

    </div>

   
@endsection


