@extends('cmo.layouts.app')

@section('content')
    <style>

        .bottom-space{
            padding-bottom: 50px;

        }

        .stepper-item li.active{
            /* CSS for creating steper block before the li item*/
            height:25px;
            width:25px;
            line-height: 30px;
            border: 2px solid #1A75BC;
            display:block;
            border-radius: 50%;
            background-color: #1A75BC;
        }


        .stepper-item li{
            /* CSS for creating steper block before the li item*/
            height:25px;
            width:25px;
            line-height: 30px;
            border: 2px solid grey;
            display:block;
            border-radius: 50%;
            background-color: grey;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Applications</h4>
            <p style="color:white;">Form SEC 2D for Fit and Proper Persons (Sponsored Individuals,
                Directors/Partners) for Registration in the Capital Market<br> Under the Investment and Securities Act 2007.</p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->




    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                <div class="row" style="padding-bottom: 80px;" >
                    <div class="col-md-3 stepper-item" >
                        <h4>Section A</h4>
                        <p>company registration</p>
                        <ul> <li></li></ul>
                    </div>
                    <div class="col-md-3 stepper-item">
                        <h4>Section B</h4>
                        <p>company registration</p>
                        <ul><li class="active"></li></ul>
                    </div>
                    <div class="col-md-3 stepper-item">
                        <h4>Affidavit</h4>
                        <p>sworn affidavit</p>
                        <ul><li></li></ul>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-light">CANCEL</button>
                        <button type="button" class="btn btn-primary">SAVE</button>

                    </div>
                </div>

                <div class="row" style="padding-bottom:10px;">
                    <form>
                        <h6>Please tick the appropriate boxes</h6>
                        <div class="row bottom-space">

                            <div class="form-group col-md-10">
                                <p>Have you been refused the right to or restricted from carrying on any trade, business or<br>
                                    profession for which a specific license, registration or other authorization is required by law<br>
                                    in any jurisdiction? If Yes provide details</p><br>
                                <textarea type="text" class="form-control" placeholder="If Yes Enter Details"></textarea>
                            </div>
                            <div class="form-group col-md-1">
                              <h5>YES</h5>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                </div>
                            </div>
                            <div class="form-group col-md-1">
                              <h5>NO</h5>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" disabled>
                                </div>
                            </div>

                        </div>

                        <div class="row bottom-space">

                            <div class="form-group col-md-10">
                                <p>Have you been disciplined, suspended, or sanctioned under the ISA or Rules and<br> Regulations
                                    of the Commission, or any other regulatory authority or Professional body or<br> government
                                    agency, whether in Nigeria or elsewhere? If yes provide details</p><br>
                                <textarea type="text" class="form-control" placeholder="If Yes Enter Details"></textarea>
                            </div>
                            <div class="form-group col-md-1">
                                <h5>YES</h5>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                </div>
                            </div>
                            <div class="form-group col-md-1">
                                <h5>NO</h5>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" disabled>
                                </div>
                            </div>

                        </div>

                        <div class="row bottom-space">

                            <div class="form-group col-md-10">
                                <p>Have you been subject of any complaint relating to activities that are regulated by the<br>
                                    Commission, or under any law in any jurisdiction? If yes provide details</p><br>
                                <textarea type="text" class="form-control" placeholder="If Yes Enter Details"></textarea>
                            </div>
                            <div class="form-group col-md-1">
                                <h5>YES</h5>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                                </div>
                            </div>
                            <div class="form-group col-md-1">
                                <h5>NO</h5>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="" id="defaultCheck2" disabled>
                                </div>
                            </div>

                        </div>

                    </form>
                </div>
            </div>
        </div>
        <!--row closed-->


@endsection

