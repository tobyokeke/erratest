@extends('cmo.layouts.app')

@section('content')
    <style>

        .bottom-space{
         padding-bottom: 50px;

        }

        .stepper-item li.active{
            /* CSS for creating steper block before the li item*/
            height:25px;
            width:25px;
            line-height: 30px;
            border: 2px solid #1A75BC;
            display:block;
            border-radius: 50%;
            background-color: #1A75BC;
        }


        .stepper-item li{
            /* CSS for creating steper block before the li item*/
            height:25px;
            width:25px;
            line-height: 30px;
            border: 2px solid grey;
            display:block;
            border-radius: 50%;
            background-color: grey;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

       <div class="col-md-10">

           <h4 class="page-title">Applications</h4>
           <p style="color:white;">Form SEC 2D for Fit and Proper Persons (Sponsored Individuals,
               Directors/Partners) for Registration in the Capital Market<br> Under the Investment and Securities Act 2007.</p>
       </div>

       <div class="col-md-2">
         <p  style="color:white;">Home <span>/ Dashboard</span></p>


       </div>
    </div>
    <!--page-header closed-->




    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                <div class="row" style="padding-bottom: 80px;" >
                    <div class="col-md-3 stepper-item" >
                      <h4>Section A</h4>
                        <p>company registration</p>
                        <ul> <li class="active"></li></ul>
                    </div>
                    <div class="col-md-3 stepper-item">
                       <h4>Section B</h4>
                        <p>company registration</p>
                        <ul><li></li></ul>
                    </div>
                    <div class="col-md-3 stepper-item">
                        <h4>Affidavit</h4>
                        <p>sworn affidavit</p>
                        <ul><li></li></ul>
                    </div>
                    <div class="col-md-3">
                        <button type="button" class="btn btn-light">CANCEL</button>
                        <button type="button" class="btn btn-primary">SAVE</button>

                    </div>
                </div>

                <div class="row" style="padding-bottom:10px;">
                    <form>
                        <h6>Name in Full</h6>
                        <div class="row bottom-space">

                            <div class="form-group col">

                                <input type="text" class="form-control" placeholder="Surname">
                            </div>
                            <div class="form-group col">

                                <input type="text" class="form-control" placeholder="First name">
                            </div>
                            <div class="form-group col">

                                <input type="text" class="form-control" placeholder="Middle name">
                            </div>
                        </div>

                        <h6>Sponsoring Company</h6>
                        <div class="form-row bottom-space">

                            <div class="form-group col">

                                <input type="text" class="form-control" placeholder="Company Name">
                            </div>

                        </div>

                        <h6>Current Residential Address</h6>
                        <div class="form-row bottom-space">

                            <div class="form-group col">

                                <input type="text" class="form-control" placeholder="Residential Address">
                            </div>

                        </div>

                        <h6>Permanent Home Address (not P.O box)</h6>
                        <div class="form-row bottom-space">

                            <div class="form-group col">

                                <input type="text" class="form-control" placeholder="Home Address">
                            </div>

                        </div>

                        <h6>Mailing Address</h6>
                        <div class="form-row bottom-space">

                            <div class="form-group col">

                                <input type="text" class="form-control" placeholder="Mailing Address">
                            </div>

                        </div>

                        <div class="row bottom-space">

                            <div class="form-group col">
                                <label for="inputEmail">Official E-mail</label>
                                <input type="text" class="form-control" placeholder="@example.com">
                            </div>
                            <div class="form-group col">
                                <label for="inputEmail2">Personal E-mail</label>
                                <input type="text" class="form-control" placeholder="@example.com">
                            </div>
                            <div class="form-group col">
                                <label for="inputEmail">GSM Number</label>
                                <input type="text" class="form-control" placeholder="GSM Number">
                            </div>
                        </div>

                        <div class="row">

                            <div class="form-group col">
                                <label for="inputDob">Date of Birth</label>
                                <input type="date" class="form-control" placeholder="dd/mm/yyyy">
                            </div>
                            <div class="form-group col">
                                <label for="inputPlace">Place of Birth</label>
                                <input type="text" class="form-control" placeholder="place of Birth">
                            </div>
                            <div class="form-group col">
                                <label for="inputEmail">Nationality</label>
                                <input type="text" class="form-control" placeholder="e.g nigerian">
                            </div>
                        </div>
                    </form>
                </div>
        </div>
    </div>
    <!--row closed-->


@endsection

