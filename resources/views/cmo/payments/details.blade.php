
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <!-- style-sheet-->
{{--<link rel="stylesheet" href="{{url('results/css/result.css')}}" />--}}
<!-- font awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" integrity="sha384-hWVjflwFxL6sNzntih27bfxkr27PmbbK/iSvJ+a4+0owXq79v+lsFkW54bOGbiDQ" crossorigin="anonymous">
    <!-- 525icons -->
    {{--<link rel="stylesheet" type='text/css' href="https://cdn.jsdelivr.net/gh/runestro/525icons@5.5/fonts/525icons.min.css">--}}
    <title>PAYMENT BREAKDOWN | SECURITIES AND EXCHANGE COMMISSION</title>
</head>
<body>
<style>
    /*@import url('https://fonts.googleapis.com/css?family=Montserrat:300,500,700');*/
    /* font families need to be inserted  */
    .s-head {
        display: flex;
        justify-content: center;
        align-items: center;
        flex-wrap: wrap;
    }
    body{
        font-family: 'Times New Roman' !important;
    }

    .logoImage{

        float:left;
        margin:30px;
    }
    .heading {
        font-family: "Montserrat","sans-serif";
        text-align: center;
        margin-top:40px;
    }
    h2 {
        font-size: 40px;
        color: #1976bb;
        margin-top: 30px;
        margin-bottom: 15px
    }
    h3 {
        font-size: 30px;
        color: #6c757d;
        margin-top: 15px;
        margin-bottom: 15px
    }
    h4 {
        font-size: 28px;
        color: #333334;
        font-weight: 500;
        margin-top: 15px;
        margin-bottom: 15px;
    }
    table.result-info {
        empty-cells: show;
        background-color: #6c757d;
        border: 1px solid black;
        color: white;
        font-size: 18px;
        font-family: 'Times New Roman';
    }
    table, tr,td {
        border: 1px solid #CCCCCC;
        empty-cells: show;
        border-collapse: collapse;
    }
    td{
        padding-left: 20px;
        text-align: center;
    }
    td.whitestroke{
        border: 1px solid white !important;
        empty-cells: show !important;
        border-collapse: collapse !important;
        text-align: left;
    }
    table.result td {
        border: 1px solid #CCCCCC;
        empty-cells: show;
        font-family: Source Sans Pro;
        font-size: 18px;
    }
    .result tr:nth-child(even){
        background-color: #EFEFEF;
    }
    td.brown{
        background-color: #6c757d;
    }
    td.wine{
        background-color: #1976bb;
    }
    td.center{
        text-align: center;
    }
    th.center{
        text-align: center;
    }
    td.bold{
        font-weight: 800;
        font-size:14px;
    }
    td.red{
        color: #1976bb;
    }
    td.comments{
        font-family: Sue Ellen Francisco;
        color: #4879B3;
        font-size: 14px;
    }
    table.bottom{
        font-size: 20px;
    }
    td.vertical {
        height: 100px;
        padding-left: 0px;
        width: 135px;
        padding-top: 0px;
        padding-right: 0px;
        transform: rotate(270deg);
        text-align: center;

    }
    td.nwidth{
        width: 25px;
    }

</style>
<div align="center" class="container">
    <img src="{{url('admin/assets/img/logolight.png')}}" class="logoImage" alt="school-logo">
    <div class="s-head">
        <div class="heading">
            {{--<h2>SECURITIES AND EXCHANGE COMMISSION</h2>--}}
            <h3>ERRA</h3>
            <h4>PAYMENT BREAKDOWN</h4>
        </div>
    </div>

    <table class="result-info" style="width:100%">
        <tr>
            <td class="whitestroke ">PAID BY: {{strtoupper($payment->User->name)}}</td>
            <td class="whitestroke">DATE: {{$payment->created_at->toDayDateTimeString()}}</td>
            {{--<td class="whitestroke">CLASS: {{$class->name}}</td>--}}
{{--            <td class="whitestroke ">SUBJECT: {{strtoupper($subject->name)}}</td>--}}
            {{--<td class="whitestroke ">TOTAL STUDENTS: {{$totalStudents}}</td>--}}
        </tr>
    </table>

    {{--<div style="width:400px">--}}
        {{--<canvas id="myChart" width="200" height="200"></canvas>--}}
    {{--</div>--}}

    <table class="result" style="width:100%">
        {{--<tr>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--<td class="center" colspan="4"><b>COGNITIVE REPORT</b></td>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--<td></td>--}}
        {{--</tr>--}}
        <tr style="background-color:white">


            <td class="nwidth"></td>
            <td class="vertical"></td>
            <td class="nwidth">APPLICATION FEE(&#x20A6;)</td>
            <td class="nwidth">REGISTRATION FEE(&#x20A6;)</td>
            <td class="nwidth">PROCESSING FEE(&#x20A6;)</td>
            <td class="nwidth">TOTAL (&#x20A6;)</td>
        </tr>
        <tr style="background-color:#1976bb; color:white;">
            <td>S/N</td>
            <td style="min-width:200px;">GRADES</td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
        </tr>

        @foreach($payment->Application->Functions as $function)
        <tr>
            <td>
                {{$loop->iteration}}
            </td>
            <td>
                {{$function->name}}
            </td>

            <td>
                {{number_format($function->applicationFee,2)}}
            </td>
            <td>
                {{number_format($function->registrationFee,2)}}
            </td>
            <td>
                {{number_format($function->processingFee,2)}}
            </td>
            <td>
                {{number_format($function->registrationFee + $function->processingFee,2)}}
            </td>
        </tr>
        @endforeach

        <tr>
            <th colspan="4" class="center">GRAND TOTAL</th>
            <th class="right">&#x20A6;{{number_format($payment->amount,2)}}</th>
        </tr>


    </table>
    <table class="bottom" style="width:100%">
        <tr>
            <td colspan="5" class="center red">ABUJA, NIGERIA</td>
        </tr>
    </table>



</div>
</body>
</html>
