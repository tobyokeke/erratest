@extends('cmo.layouts.app')

@section('content')
    <style>
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Form Sec 2A</h4>
            <p style="color:white;">Application form for Resignation of Sponsored Individual by the Sponsoring Company under the Investment and Securities act 2007</p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->

    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                <form method="post" enctype="multipart/form-data" action="{{route('cmo.applications.post-sec-form-2A')}}">
                    {{ csrf_field() }}

                    <input type="hidden" name="type" value="{{$type}}">
                    <input type="hidden" name="fid" value="{{$function->fid}}">
                    <input type="hidden" name="aid" value="{{$application->aid}}">



                    <h5>Applicant Company</h5>
                    <div class="row">
                        <div class="form-group col">
                            <label>Name of Applicant Company </label>
                            <input class="form-control" name="applicant_company" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address of Applicant Company</label>
                            <input class="form-control" name="applicant_company_address" type="text" placeholder="">
                        </div>

                    </div><br>
                    <h6>OFFICER BEING REPLACED</h6>
                    <div class="row">
                        <div class="form-group col">
                            <label>Full Name</label>
                            <input class="form-control" name="officer_replaced_name" type="text" placeholder="Surname Other names">
                        </div>
                        <div class="form-group col">
                            <label>Current Registration</label>
                            <input class="form-control" name="current_reg" type="text" placeholder="">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="form-group col">
                            <label>Period of Service in your Employment</label>
                            <input class="form-control" name="officer_replaced_period_service" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Reasons for leaving</label>
                            <textarea class="form-control" name="reasons_for_leaving" type="text" placeholder=""></textarea>
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="form-group col">
                            <label>Comment on Integrity</label>
                            <textarea class="form-control" name="comment_on_integrity" type="text" placeholder=""></textarea>
                        </div>

                    </div><br>



                    <h6>SUBSTITUTE OFFICER</h6>
                    <div class="row">
                        <div class="form-group col">
                            <label>Full Name</label>
                            <input class="form-control" name="sub_officer_name" type="text" placeholder="Surname Other names">
                        </div>
                        <div class="form-group col">
                            <label>Registration Sought</label>
                            <input class="form-control" name="reg_sought" type="text" placeholder="">
                        </div>
                    </div><br>
                    <div class="row">
                        <div class="form-group col">
                            <label>Period of Service in your Employment</label>
                            <input class="form-control" name="sub_officer_period_service" type="text" placeholder="">
                        </div>

                    </div><br>
                    <div class="row bottom-space">

                        <div class="form-group col-md-4">
                            <p>Form SEC2/2b Submitted</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="form_sec2_2b_submitted" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="col-md-6"></div>

                    </div>
                    <div class="row">
                        <div class="form-group col">
                            <label class="" for="customFile">Signature</label><br>
                            <input type="file" name="sig" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label>Designation</label>
                            <input class="form-control" name="designation" type="text" placeholder="">
                        </div>

                    </div><br>


                    <div class="row">
                        <div class="form-group col">
                            <label>Name of Director</label>
                            <input class="form-control" name="director_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Signature</label><br>
                            <input type="file" name="director_sig" class="" id="customFile">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name of Secretary</label>
                            <input class="form-control" name="secretary_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Signature</label><br>
                            <input type="file" name="sec_sig" class="" id="customFile">
                        </div>

                    </div><br>


                    <div class="row">
                        <div class="form-group col">
                            <label class="" for="customFile">Upload Company Seal</label><br>
                            <input type="file" name="company_seal" class="" id="customFile">
                        </div>
                    </div><br>




                    <h5 class="text-center">AFFIDAVIT</h5>
                    <p class="text-center">
                        I………………………………………………………………………………………………Director/Secretary<br>
                        (Full Name)<br>
                        ………………………………………………………………………………………Of…………………………………………………………………<br>
                        (Name of Applicant Company)
                    </p>
                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_name" type="text" placeholder="Full Name">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="dir_sec" type="text" placeholder="Director/Secretary">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_of" type="text" placeholder="Of">
                        </div>
                    </div><br>


                    <p>
                        Hereby make an Oath and state as follows: <br>
                        (1) That I have been duly authorized to sign this form on behalf of the Company<br>
                        (2) that the statement of facts made and the information supplied on this from are to the best of my knowledge and belief true and correct<br>

                        <span class="ml-auto">Signature of Despondent………………………………………………</span><br>
                        <span class="text-center">
                              SWORN to at………………………………………………………court this……………………………..day
                              Of………………………………………………year………………………………………………………………
                              BEFORE ME
                              ………………………………………………………………
                              COMMISSIONER OF OATHS
                            </span>

                    </p>

                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" name="first_input" type="text" placeholder="first input">
                        </div>
                        <div class="form-group col">
                            <input class="form-control"name="second_input" type="text" placeholder="second input">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="third_input" type="text" placeholder="third input">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="fourth_input" type="text" placeholder="fourth input">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="fifth_input" type="text" placeholder="fifth input">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload Despondent Signature</label><br>
                            <input type="file" name="despondent_sig" class="" id="customFile">
                        </div>
                    </div><br>

                    <div class="row text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

            </div>
        </div>
        <!--row closed-->

    </div>
@endsection
