@extends('cmo.layouts.app')

@section('content')
    <style>
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Form Sec 1</h4>
            <p style="color:white;">	Registration of bonus/scrip issue by Public Quoted/Uniquoted Companies.</p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->




    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                    <form>
                        <h5>1.</h5>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <label>(a) Name of company</label>
                                <input type="text" class="form-control" placeholder="Name of company">
                            </div>
                            <div class="form-group col-md-6">
                                <label>Address of company</label>
                                <input class="form-control" type="text" value="" placeholder="Address of company">
                            </div>


                        </div>

                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <label>(b) Date of incorporation</label>
                                <input type="date" class="form-control" placeholder="">
                            </div>
                            <div class="form-group col-md-6">
                                <label>(c) Nature of business</label>
                                <textarea class="form-control" type="text" placeholder=""></textarea>
                            </div>


                        </div>
                        <h5>2. Build up of Share Capital</h5>
                        <table border="1" style="table-layout: fixed; width: 100%;">
                            <tr>

                                <th colspan="6" class="text-center"></th>
                                <th colspan="6" class="text-center">ISSUED AND PAID UP</th>
                            </tr>
                            <tr>

                                <th colspan="6" class="text-center">AUTHORIZED NORMAL VALUE</th>
                                <th colspan="2" class="text-center">Other than by Bonus</th>
                                <th colspan="2" class="text-center">By Bonus</th>
                                <th colspan="2" class="text-center">Total</th>
                            </tr>
                            <tr>

                                <th colspan="2" class="text-center">Year</th>
                                <th colspan="2" class="text-center">No of Shares</th>
                                <th colspan="2" class="text-center">Amount N'000</th>

                                <th class="text-center">Amount N'000</th>
                                <th class="text-center">No of Shares</th>
                                <th class="text-center">Amount N'000</th>
                                <th class="text-center">No of Shares</th>
                                <th  colspan="2" class="text-center">Amount N'000</th>
                            </tr>
                            <tbody>
                              <tr>

                                  <td colspan="2" class="text-center"><input type="text"></td>
                                  <td colspan="2" class="text-center"><input type="text"></td>
                                  <td colspan="2" class="text-center"><input type="text"></td>

                                  <td class="text-center"><input type="text"></td>
                                  <td class="text-center"><input type="text"></td>
                                  <td class="text-center"><input type="text"></td>
                                  <td class="text-center"><input type="text"></td>
                                  <td colspan="2" class="text-center"><input type="text"></td>
                              </tr>
                            </tbody>
                        </table>
                        <br>
                        <h5>3. Shareholding Position</h5>
                        <p>(a) Shareholders holding 5% and above (Before Bonus)</p>
                        <table border="1" style="table-layout: fixed; width: 100%;">

                            <tr>
                                <td></td>
                                <th scope="col" class="text-center">Name </th>
                                <th scope="col" class="text-center">No. of Shares</th>
                                <th scope="col" class="text-center">Amount (#)</th>
                                <th scope="col" class="text-center">Percentage %</th>

                            </tr>
                            <tr>
                                <th scope="row" class="text-center">FOREIGN</th>
                                <td><input type="text"></td>
                                <td><input type="text"></td>
                                <td><input type="text"></td>
                                <td><input type="text"></td>

                            </tr>
                            <tr>
                                <th scope="row" class="text-center">NIGERIAN</th>
                                <td><input type="text"></td>
                                <td><input type="text"></td>
                                <td><input type="text"></td>
                                <td><input type="text"></td>

                            </tr>
                        </table><br>

                        <p>(b) Shareholders holding 5% and above (After Bonus)</p>
                        <table border="1" style="table-layout: fixed; width: 100%;">

                            <tr>
                                <td></td>
                                <th scope="col" class="text-center">Name </th>
                                <th scope="col" class="text-center">No. of Shares</th>
                                <th scope="col" class="text-center">Amount (#)</th>
                                <th scope="col" class="text-center">Percentage %</th>

                            </tr>
                            <tr>
                                <th scope="row" class="text-center">FOREIGN</th>
                                <td><input type="text"></td>
                                <td><input type="text"></td>
                                <td><input type="text"></td>
                                <td><input type="text"></td>

                            </tr>
                            <tr>
                                <th scope="row" class="text-center">NIGERIAN</th>
                                <td><input type="text"></td>
                                <td><input type="text"></td>
                                <td><input type="text"></td>
                                <td><input type="text"></td>

                            </tr>
                        </table><br>

                        <h5>4. Appropriation of Profits</h5>
                        <table border="1" style="table-layout: fixed; width: 100%;">

                            <tr>
                                <td></td>
                                <th scope="col" class="text-center">Current year </th>
                                <th scope="col" class="text-center">Previous year</th>


                            </tr>
                            <tr>
                                <td></td>
                                <th scope="col" class="text-center">&#x20A6; </th>
                                <th scope="col" class="text-center">&#x20A6;</th>


                            </tr>
                            <tr>
                                <th scope="row" class="text-center">Profit after taxation</th>
                                <td><input type="text"></td>
                                <td><input type="text"></td>


                            </tr>
                            <tr>
                                <th scope="row" class="text-center">Dividend</th>
                                <td><input type="text"></td>
                                <td><input type="text"></td>


                            </tr>
                            <tr>
                                <th scope="row" class="text-center">Reserves for bonus issues</th>
                                <td><input type="text"></td>
                                <td><input type="text"></td>


                            </tr>
                            <tr>
                                <th scope="row" class="text-center">Other reserves</th>
                                <td><input type="text"></td>
                                <td><input type="text"></td>


                            </tr>
                            <tr>
                                <th scope="row" class="text-center">Retained profit</th>
                                <td><input type="text"></td>
                                <td><input type="text"></td>


                            </tr>
                        </table><br>

                        <h5>5. Basic scrip of issue/ Date of issue</h5>
                        <div class="row boot-space">
                            <div class="form-group col-md-8">
                                <label>Basic scrip of issue</label>
                                <textarea class="form-control" type="text"></textarea>
                            </div>
                            <div class="form-group col-md-4">
                                <label>Scrip Date of issue</label>
                                <input class="form-control" type="date" >
                            </div>
                        </div><br>

                        <p class="text-center">
                            We declare that: <br>(a) The information supplied above is, to the best of our knowledge and belief,<br>
                            true, correct and conforms with the company’s resolution of<br>
                            <span>………………………………………………………………..</span>approving the scrip issue.

                        </p><br>
                        <input class="form-control" type="text" placeholder="Declaration">

                        <p class="text-center">
                            (b) That distributable reserves of the company are sufficient to meet the<br> additional capital introduced by the proposed scrip/bonus
                        </p><br>

                        <h5 class="text-center">Affidavit</h5>
                        <p class="text-center">
                            I……………………………….................................................................of………………………………………..... <br>
                            (Full name)         (Address)<br>
                            ……………………………………………………………………………………………………………………………...  <br> Director/Secretary <br>
                            To……………………………………………………………………………………………………………………….....   <br> (Applicant Company)
                        </p>
                        <div class="row">
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="Full Name">
                            </div>
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="Address">
                            </div>
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="Director/Secretary">
                            </div>
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="Applicant Company">
                            </div>
                        </div><br>


                        <p>
                            Make oath and state as follows: <br>
                            1.  That I am duly authorized to supply the information and in the exhibits thereto;<br>
                            2.  That the information supplied and the comments made in this form and in the exhibits thereto are to the best of my knowledge and belief true and correct.<br>


                            <span class="text-center">
                              SWORN to at………………………………………………………court this……………………………..day
                              Of…………………………………………………………………………………………………………………………
                              BEFORE ME
                              ………………………………………………………………
                              COMMISSIONER OF OATHS
                            </span>

                        </p>
                        <div class="row">
                            <div class="form-group col">
                               <input class="form-control" type="text" placeholder="first input">
                            </div>
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="second input">
                            </div>
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="third input">
                            </div>
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="fourth input">
                            </div>
                        </div><br>


                        <p class="text-center">
                            Dated this……………………………………………day of………………………………………………………..<br>
                            Signed:(1)………………………………………………………………………………...Managing Director<br>
                            (2)………………………………………………………………………………………….Company Secretary<br>
                            (3)……………………………………………………………………………………………….Chief Accountant<br>
                        </p>
                        <div class="row">
                            <div class="form-group col">
                                <input class="form-control" type="date" placeholder="Dated">
                            </div>
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="Sign (1)">
                            </div>
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="Sign (2)">
                            </div>
                            <div class="form-group col">
                                <input class="form-control" type="text" placeholder="Sign (3)">
                            </div>
                        </div>

                      <p>
                          <b>Note</b><br>
                          <b>Completed form should be accompanied with the following:<b>
                      </p>

                       <ul>
                           <ol type="I">
                               <li>A copy each of the Board and Shareholders Resolution authorizing the issue and certified by the company secretary.</li>
                               <li>A signed copy of the Audited Account of the Company showing the provision for the bonus.</li>
                               <li>A copy of the Certificate of increase in Share Capital certified by CAC (where applicable). </li>
                               <li>Evidence of payment of registration and filing fees. </li>
                               <li>A copy each of the Certificate of Incorporation and Memorandum and Articles of Association certified by the CAC (where the Company is filing for the first time or if there had been amendments to the Mermart). </li>
                               <li>A Copy of Original CAC Certified True Copy of Form CAC 2. </li>
                           </ol>
                       </ul>
                    </form>

            </div>
        </div>
        <!--row closed-->

    </div>
@endsection


