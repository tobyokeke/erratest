@extends('cmo.layouts.app')

@section('content')
    <style>
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Form Sec 2D</h4>
            <p style="color:white;">Application form for fit and persons (Sponsored Individuals, Directors/Partners) for registration in the capital market under the investments and Securities ACT 2007 </p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->

    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                <form method="post" enctype="multipart/form-data" action="{{route('cmo.applications.post-sec-form-2D')}}">
                    {{ csrf_field() }}

                    <input type="hidden" name="type" value="{{$type}}">
                    <input type="hidden" name="fid" value="{{$function->fid}}">
                    <input type="hidden" name="aid" value="{{$application->aid}}">


                    <h5>SECTION A</h5>
                    <div class="row">
                        <div class="form-group col">
                            <label>Name in Full </label>
                            <input class="form-control" name="name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Sponsoring Company</label>
                            <input class="form-control" name="sponsoring_company" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Current Residential Address </label>
                            <input class="form-control" name="res_address" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Permanent Home Address</label>
                            <input class="form-control" name="perm_address" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Mailing Address</label>
                            <input class="form-control" name="mail_address" type="text" placeholder="">
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Official E-mail </label>
                            <input class="form-control" name="official_email" type="email" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Personal E-mail</label>
                            <input class="form-control" name="personal_email" type="email" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>GSM Number </label>
                            <input class="form-control" name="gsm_number" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Landline Number</label>
                            <input class="form-control" name="landline_number" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Date of Birth </label>
                            <input class="form-control" name="dob" type="date" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Place of Birth</label>
                            <input class="form-control" name="pob" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>State of origin </label>
                            <input class="form-control" name="soo" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Nationality</label>
                            <input class="form-control" name="nationality" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Sex(M/F)</label>
                            <input class="form-control" name="sex" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Marital Status</label>
                            <input class="form-control" name="marital_status" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name of Next of Kin </label>
                            <input class="form-control" name="next_kin_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Next of Kin Telephone Number</label>
                            <input class="form-control" name="next_kin_number" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Next	of	Kin(s) Permanent Residential Address</label>
                            <input class="form-control" name="next_kin_address" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Relationship	– Son/Daughter, Wife, Father/Mother</label>
                            <input class="form-control" name="next_kin_relationship" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Others – Specify</label>
                            <input class="form-control" name="next_kin_other" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h5>SECTION B</h5><br>

                    <p>TICK	THE	APPROPRIATE	BOX	IN THE FOLLOWING QUESTIONS </p>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>i. Have	you	been	refused	the	right	to	or	restricted	from	carrying	on	any	trade,	business or profession for which a specific license, registration or other authorization is required by law	in any jurisdiction?</p><br>
                            <textarea type="text" name="q1_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q1" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>2. Have	you	been	disciplined,	suspended,	or	sanctioned	under	the	ISA	or	Rules	and	Regulations	of	the	Commission,	or	any	other	regulatory	authority	or	Professional	body	or	government	agency,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q2_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                        <label class="text-center">Yes/No</label>
                        <div class="form-check text-center">
                            <input
                                    name="q2" class="form-check-input"
                                    type="checkbox" value="" id="defaultCheck1">
                        </div>
                    </div>


            </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>3.Have	you	been	subject	of	any	complaint	relating	to	activities	that	are	regulated	by	the	Commission,	or	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q3_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q3" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>4.Have	you	been	subjected	to	any	proceedings	of	a	disciplinary	or	civil/criminal	nature	or	been	notified	of	any	potential	proceedings	or	of	any	investigation	which	might	lead	to	proceedings,	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q4_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q4" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>5.Have	you	been	convicted	of	any	offence,	or	currently	being	subjected	to	any	pending	criminal	proceedings	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q5_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q5" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>6.Have	you	ever	been	convicted	of	any	offence	relating	to	fraud,	misrepresentation	or	dishonesty	in	any	civil	or	criminal	proceeding	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q6_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q6" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>7.State	clearly	if	there	is	any	pending	proceeding	relating	to	fraud,	dishonesty	and	misrepresentation	in	any	court	of	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q7_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q7" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>8.Have	you	had	any	civil/criminal	penalty,	enforcement	action	taken	against	you	by	the	Commission	or	any	other	regulatory	authority	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q8_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q8" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>9.Have	you	had	any	civil/criminal	penalty,	enforcement	action	taken	against	you	by	the	Commission	or	any	other	regulatory	authority	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q9_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q9" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>10. Have	you	been	subject	of	any	investigation	or	disciplinary	proceedings	or	been	issued	a	warning	or	reprimand	by	the	Commission	or	any	other	regulatory	authority,	an	operator	of	a	market	or	clearing	facility,	any	professional	body	or	government	agency,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q10_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q10" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>11. Have	you	been	refused	a	fidelity	or	surety	bond,	whether	in	Nigeria	or	elsewhere?	</p><br>
                            <textarea type="text" name="q11_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q11" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>12. Have	you	demonstrated	unwillingness	to	comply	with	any	regulatory	requirement	or	to	uphold	any	professional	and	ethical	standards,	whether	in	Nigeria	or	elsewhere?	</p><br>
                            <textarea type="text" name="q12_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q12" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>13.Have	you	been	untruthful	or	provided	false	or	misleading	information	to	the	Commission,	investors,	and	general	public	or	been	uncooperative	in	any	dealings	with	the	Commission,	investors,	general	public	or	any	other	regulatory	authority	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q13_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q13" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>14.Have	you	been	a	director,	partner,	shareholder,	staff	or	concerned	in	the	management	of business	that	has	been	censured,	disciplined,	prosecuted	or	convicted	of	a	civil/criminal	offence,	or	been	the	subject	of	any	disciplinary	or	civil/criminal	investigation	or	proceeding,	in	Nigeria	or	elsewhere,	in	relation	to	any	matter	that	took	place	while	you	were	a	director,	partner,	shareholder,	staff	or	concerned	in	the	management	of	the	Business?	</p><br>
                            <textarea type="text" name="q14_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q14" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>15. Have	you	been	a	director,	partner,	shareholder,	staff	or	concerned	in	the	management	of	business	that	has	been	suspended	or	refused	membership	or	registration	by	the	Commission	or	any	other	regulatory	authority,	an	operator	of	a	market	or	clearing	facility,	any	professional	body	or	government	agency?</p><br>
                            <textarea type="text" name="q15_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q15" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>16. Have	you	been	a	director,	partner,	shareholder,	staff	or	concerned	in	the	management	of	a	business	that	has	gone	into	insolvency,	liquidation,	administration	or	receivership,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q16_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q16" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>17.Have	you	been	dismissed	or	asked	to	resign	from	—<br>	(i)	an	office;<br>	(ii)	employment;<br>	(iii)	a	position	of	trust;	or;	<br>(iv)	A	fiduciary	appointment	or	similar	position,	whether	in	Nigeria	or	elsewhere	in	the	last	10	years?</p><br>
                            <textarea type="text" name="q17_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q17" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>18. Have	you	been	subjected	to	disciplinary	proceedings	by	current	or	former	employer(s),	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q18_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q18" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>19. Have	you	been	disqualified	from	acting	as	a	director	or	in	any	managerial	capacity,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q19_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q19" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>20.Have	you	been	an	officer	found	liable	for	an	offence	committed	by	a	body	corporate	as	a	result	of	the	offence	having	proved	to	have	been	committed	with	the	consent	or	connivance	of,	or	neglect	attributable	to,	you,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q20_details" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q20" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <h5 class="text-center">Sworn Declaration<br>(AFFIDAVIT)</h5>
                    <p class="text-center">
                        I………………………………………………………………………………………………<br>	(Name	in	Full)

                        <br>Of……………………………………………………………<br>	(Address)



                    </p>
                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_name" type="text" placeholder="Full Name">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_address" type="text" placeholder="Address">
                        </div>

                    </div><br>


                    <p>
                        Hereby make an Oath and state as follows: <br>
                        (1) That I………………………………………………………………………………………the	applicant<br> (Name	in	Full)<br>
                        herein	seek	registration	with	the	Securities	and	Exchange	Commission,	Nigeria.<br>
                        (2)  That I	have read and understood the questions in this form	as well	as	the	answers	I	have	given	and	that	to	the	best	of	my	knowledge	and	belief	the	statements	made	herein	and	the	attachments	are	true <br>
                        (3) That	I	swear	to	this	affidavit	in	good	faith.<br>
                        <span class="ml-auto">Despondent………………………………………………</span><br>
                        <span class="text-center">
                              SWORN to at………………………………………………………court this……………………………..day
                              Of………………………………………………year………………………………………………………………
                              BEFORE ME
                              ………………………………………………………………
                              COMMISSIONER OF OATHS
                            </span>

                    </p>

                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_full_name" type="text" placeholder="Full Name (Affidavit)">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="first_input" type="text" placeholder="first input">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="second_input" type="text" placeholder="second input">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="third_input" type="text" placeholder="third input">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="fourth_input" type="text" placeholder="fourth input">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="fourth_input" type="text" placeholder="fifth input">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload Despondent Signature</label><br>
                            <input type="file" class="" name="despondent_sig" id="customFile">
                        </div>
                    </div><br>

                    <div class="row text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

            </div>
        </div>
        <!--row closed-->

    </div>
@endsection

