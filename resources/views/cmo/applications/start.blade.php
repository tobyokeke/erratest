@extends('cmo.layouts.app')

@section('styles')
    <style >
     .amount{
         font-size: 60px;
         color:green;
     }

        .functions{
            font-size:14px;
        }
        .functions .row{
            margin-bottom: 10px !important;
        }
        .hidden{
            display:none !important;
        }
 </style>
    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script src="https://unpkg.com/axios@0.18.0/dist/axios.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="{{env('INLINE_FORM_URL')}}"></script>

@endsection

@section('scripts')
    <script src="{{url('assets/js/accordiation.js')}}"></script>
@endsection

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Applications</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Applications</a></li>
        </ol>
    </div>
    <!--page-header closed-->



    <!--row open-->
    <div class="row" id="app" xmlns:v-on="http://www.w3.org/1999/xhtml">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Start A New Application</h4>

                </div>
                <div class="card-body">

                    <p><b>SELECT FUNCTIONS</b></p>
                    <p>
                        Please select the functions you would like to apply for from the list below.
                        As you add more functions to your selection, you would see the price to pay,
                        you are required to make payment before you would be able to complete the forms and upload the
                        required documents that would enable you submit your application for processing.

                    </p>


                    <div class="row">

                        <div class="col-md-8">
                            <div class="card">
                                <div class="card-header">Functions</div>
                                <div class="card-body functions">
                                    @foreach($functions as $function)
                                    <!-- start function -->
                                    <div class="row">
                                        <div class="col-md-8">{{$function->name}}

                                            @if($function->isStandalone == 1)
                                            <span class="badge badge-warning">Standalone</span>
                                            @endif

                                        </div>
                                        <div class="col-md-2">&#x20A6;{{$function->processingFee + $function->registrationFee + $function->applicationFee}}</div>
                                        <div class="col-md-2">
                                            <input id="{{$function->fid}}" class="checkbox" v-on:click="addFunction($event,{{$function->processingFee + $function->registrationFee + $function->applicationFee}}, {{$function->isStandalone}})" value="{{$function->fid}}" type="checkbox">
                                        </div>
                                    </div>
                                    <!-- end function -->
                                    @endforeach

                                    @if(count($functions) == 0)
                                        <div class="row" align="center">No functions</div>
                                    @endif

                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">
                            <div class="card">
                                <div class="card-body">
                                    <p><b>Total amount to be paid</b></p>
                                    <p>Price is inclusive of tax</p>
                                    <p class="amount">&#x20A6;@{{ totalPrice }}</p>

                                    {{--<hr>--}}

                                    {{--<p align="center">--}}
                                        {{--<span>Click to proceed to payment gateway &nbsp;</span><br><br>--}}


                                        {{--<a class="btn btn-primary" style="color:white;" v-on:click="initiatePayment()">PAY NOW</a>--}}

                                    {{--</p>--}}
                                </div>
                            </div>

                        </div>

                    </div> <!-- end first row -->


                    <!--start second row (requirements)-->
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>REQUIREMENTS</h4>
                                </div>
                                <div class="card-body">


                                    <div class="acc-1 js-accordion  ">

                                        @foreach($functions as $function)
                                            <div class="accordion__item js-accordion-item active hidden" id="r-{{$function->fid}}">
                                                <div class="accordion-header js-accordion-header">{{$loop->iteration}}. {{$function->name}}</div>
                                                <div class="accordion-body js-accordion-body">
                                                    <div class="accordion-body__contents">
                                                        {{$function->description}}
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach


                                    </div>
                                </div>
                                <div class="card-footer">
                                    <span>                                    Have all you require?</span>
                                    <a class="btn btn-primary" style="color:white;" v-on:click="initiatePayment()">CONTINUE APPLICATION</a>
                                    {{--<a class="btn btn-primary mt-2 mb-2" href="#">Send me your Questions?</a>--}}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--end second row-->


                </div>
            </div>
        </div>
    </div>
    <!--row closed-->


     {{--<script src="http://www.remitademo.net/payment/v1/remita-pay-inline.bundle.js"></script>--}}



    <script>
        Vue.use(axios);
        var app = new Vue({
            el: "#app",
            data: {
                uid: "{{auth()->user()->uid}}",
                email: "{{auth()->user()->email}}",
                csrftoken: '{{csrf_token()}}',
                baseUrl : "{{url('/')}}",
                publicKey : "{{env('PUBLIC_KEY')}}",
                loading: false,
                totalPrice : 0,
                functions : []
            },
            mounted() {

            },
            methods: {
                addFunction(event,price,isStandalone){
                    console.log(event.target.checked);
                    console.log(event.target.value);


                    if(event.target.checked){

                        // if function is checked show requirements
                        let requirement = document.getElementById('r-' + event.target.id);
                        requirement.classList.remove('hidden');

                        if(isStandalone){
                            this.totalPrice = 0;

                            // disable all other checkboxes when a standalone is selected
                           let checkboxes =  document.getElementsByClassName('checkbox');

                           for(let i = 0; i < checkboxes.length; i++){
                               let item = checkboxes[i];

                               if(item.id != event.target.id){ // disable other checkboxes
                                   item.disabled = true;
                                   item.checked = false;
                                   let requirement = document.getElementById('r-' + item.id);
                                   requirement.classList.add('hidden');

                               }
                           }


                        }

                        this.totalPrice += price;
                        this.functions.push(event.target.value);
                    }
                    else{

                        if(isStandalone){

                            // enable all other checkboxes when a standalone is un-selected
                            let checkboxes =  document.getElementsByClassName('checkbox');

                            for(let i = 0; i < checkboxes.length; i++){
                                let item = checkboxes[i];
                                item.checked = false;
                                item.disabled = null;
                                let requirement = document.getElementById('r-' + item.id);
                                requirement.classList.add('hidden');

                            }

                        }

                        this.totalPrice -= price;
                        this.functions.splice(this.functions.indexOf(event.target.value),1);
                    }
                },

                makePayment() {
                    var self = this;
                    var paymentEngine = RmPaymentEngine.init({
                        key: 'dG9ieS5va2VrZUBnbWFpbC5jb218NDE5MTcyNDl8OGI1ZjllZmU1OGI1NzQ2YTNkNTY1MzgzOGI3ZGE4ZDYzZjQ1ODgxNDk0OTI3Y2RmZGMwY2UxYzNhM2EyY2ZiNGZkNDIyYzU0OWZhZDhkMWFjOTllMTdkNTQzYTY3YjYzMjgxYTRmNmE2YjAwMmQyZTY0NjA2OTcyZmQ5YTAzNTc=',
                        customerId: "4343434",
                        firstName: "James",
                        lastName: "Spark",
                        email: "toby.okeke@gmail.com",
                        narration: "Payment for SEC application",
                        amount: 10000,
                        onSuccess: function (response) {
                            console.log('callback Successful Response', response);
                        },
                        onError: function (response) {
                            console.log('callback Error Response', response);
                        },
                        onClose: function () {
                            console.log("closed");
                        }
                    });
                    paymentEngine.showPaymentWidget();
                },
                initiatePayment(){
                    let data = {
                        'uid': this.uid,
                        'amount' : this.totalPrice,
                        'functions' : this.functions
                    };

                    this.loading = true;
                    axios.post(this.baseUrl + "/cmo/pay", data)
                        .then(response => {
                            this.loading = false;

                            this.amount = response.data.data.amount;
                            this.reference = response.data.data.reference;
                            console.log(response.data.data);

                            this.payWithQuidpay();

                            // JSON responses are automatically parsed.
                        })
                        .catch(e => {
                            this.loading = false;
                            swal({
                                text: e.response.data.message,
                                icon: "error",
                                buttons: false,
                                timer: 3000,
                            });
                            console.log(e.response.data.message);
                        });

                },

                payWithQuidpay(){
                    let self = this;
                    var x = getpaidSetup({
                        PBFPubKey: self.publicKey,
                        customer_email: self.email,
                        amount: self.amount,
                        // customer_phone: self.phone,
                        currency: "NGN",
                        txref: self.reference,
                        meta: [{
                            metaname: "uid",
                            metavalue: self.uid
                        }],
                        onclose: function() {},
                        callback: function(response) {
                            var txref = response.tx.txRef; // collect txRef returned and pass to a 					server page to complete status check.

                            console.log("This is the response returned after a charge", response);
                            if (
                                response.tx.chargeResponseCode == "00" ||
                                response.tx.chargeResponseCode == "0"
                            ) {

                                let data = {
                                    "reference" : txref
                                };

                                self.loading = true;
                                axios.post(self.baseUrl + "/cmo/create-application", data)
                                    .then(response => {
                                        self.loading = false;

                                        window.location.href = self.baseUrl + '/cmo/applications';
                                        // this.reference = response.data.data.reference;
                                        console.log(response.data.data);

                                        this.payWithQuidpay();

                                        // JSON responses are automatically parsed.
                                    })
                                    .catch(e => {
                                        this.loading = false;
                                        swal({
                                            text: e.response.data.message,
                                            icon: "error",
                                            buttons: false,
                                            timer: 3000,
                                        });
                                        console.log(e.response.data.message);
                                    });

                                // redirect to a success page
                            } else {
                                // redirect to a failure page.
                            }

                            x.close(); // use this to close the modal immediately after payment.
                        }
                    });

                },
            }
        });
    </script>



@endsection
