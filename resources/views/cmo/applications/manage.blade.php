@extends('cmo.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Applications</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Applications</a></li>
        </ol>
    </div>
    <!--page-header closed-->



    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Applications</h4>

                </div>
                <div class="card-body">
                    <table class="table table-borderless text-nowrap mb-0">
                        <thead>
                        <tr>
                            <th>S/N</th>
                            <th>ID</th>
                            <th>Function(s)</th>
                            <th>Status</th>
                            <th>Started</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count = 1; ?>
                        @foreach($applications as $application)
                            <tr>
                                <td>{{$count}}</td>
                                <td>{{$application->aid}}</td>
                                <td>
                                    @foreach($application->Functions as $function)
                                    {{$function->name}},
                                    @endforeach
                                </td>
                                <td>{{$application->status}}</td>
                                <td>{{$application->created_at->diffForHumans()}}</td>
                                <td>
                                    <a href="{{route('cmo.application.details',['$application' => $application->aid])}}">
                                        <span class="badge badge-pill badge-primary">Continue</span>
                                    </a>

                                </td>
                            </tr>
                            <?php $count++; ?>
                        @endforeach

                        </tbody>
                    </table>

                    {{$applications->links()}}
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--row closed-->


@endsection
