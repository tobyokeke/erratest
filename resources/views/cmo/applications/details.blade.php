@extends('cmo.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard.css')}}">
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard_theme_dots.css')}}">
    <style>


        .form-div{
            margin-bottom:10px;
        }

        .upload, .btn-success{
            background-color: darkgreen;
            border-color:darkgreen;
        }


    </style>
@endsection

@section('scripts')
    <!--Formvalidation js-->
    <script src="{{url('admin/assets/js/formvalidation.js')}}"></script>

@endsection

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Applications</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Applications</a></li>
        </ol>
    </div>
    <!--page-header closed-->

    <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">

        <div class="card">
            <div class="card-header">
                APPLICATION ID - #{{$application->aid}}

                @if($application->status == 'pending')
                <span class="badge badge-warning pull-right">PENDING </span>
                @endif

                @if($application->status == 'submitted')
                <span class="badge badge-secondary pull-right">SUBMITTED </span>
                @endif

                @if($application->status == 'approved')
                <span class="badge badge-success pull-right">APPROVED </span>
                @endif

                @if($application->status == 'rejected')
                <span class="badge badge-danger pull-right">REJECTED </span>
                @endif

            </div>
            <div class="card-body users">
                CREATED ON - {{$application->created_at->toDayDateTimeString()}} <br>
                AMOUNT PAID - &#x20A6;{{number_format($application->Payment->amount,2)}} <a href="{{route('cmo.payment.details',['payment' => $application->Payment->pid])}}"  class="badge badge-primary">VIEW BREAKDOWN</a>


                @if($showSubmit && $application->status == 'pending')
                <div   align="center">
                    <a href="{{route('cmo.application.submit',['application' => $application->aid])}}" class="btn btn-success">SUBMIT TO SEC</a><br>
                    <small>This would enable your application be vetted</small>
                </div>
                @endif

            </div>
        </div>

    </div>



    <!--row open-->
    <div class="row">
        @foreach($application->Functions as $function)
        <div class="col-lg-6 col-xl-6 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                   FUNCTION - {{$function->name}}
                </div>
                <div class="card-body users">
                    @foreach($function->Forms as $form)

                     <div class="row form-div">
                        <div class="col-md-8">{{$form->name}}</div>
                        <div class="col-md-4">
                            @if(\App\applicationSubmission::where('aid',$application->aid)->where('fid',$function->fid)->where('uid',auth()->user()->uid)->where('name',$form->name)->count() <=  0 )
                                <a href="{{url('cmo/'. str_slug($form->name) . '?type=application&function='. $function->fid . '&application=' . $application->aid )}}" class="btn btn-primary" >FILL NOW</a>
                            @else
                            <a href="{{url('cmo/application/view/' . str_slug($form->name) . '/' . \App\applicationSubmission::where('aid',$application->aid)->where('fid',$function->fid)->where('uid',auth()->user()->uid)->where('name',$form->name)->first()->asid)}}" class="btn btn-secondary" >FILLED</a>
                            @endif
                        </div>
                    </div>

                    @endforeach

                    <b>Attached Documents</b>
                        <hr>

                        @if(count($application->FunctionDocuments($function->fid)) <= 0 ) No Documents @endif
                        @foreach($application->FunctionDocuments($function->fid) as $document)

                            <div class="row mb-2">
                                <div class="col-md-8">
                                    <a href="{{$document->url}}">
                                        {{$document->name}}
                                    </a>
                                    @if($document->status == 'pending')
                                        <span class="badge badge-warning pull-right">PENDING </span>
                                    @endif

                                    @if($document->status == 'approved')
                                        <span class="badge badge-success pull-right">APPROVED </span>
                                    @endif

                                    @if($document->status == 'rejected')
                                        <span class="badge badge-danger pull-right">REJECTED </span>
                                    @endif

                                </div>
                                <div class="col-md-2"></div>
                                <div class="col-md-2">
                                    @if($application->status == 'pending')
                                    <a href="{{route('cmo.applications.document.delete',['adid' => $document->adid])}}"><i style="color: darkred;" class="mdi mdi-delete"></i> </a>
                                    @endif
                                </div>
                            </div>
                        @endforeach

                        @if($application->status == 'pending')

                        <br><br>
                        <b>Attach a document</b>
                        <hr>
                            <form method="post" action="{{route('cmo.applications.document.post')}}" enctype="multipart/form-data">

                                <input type="hidden" name="fid" value="{{$function->fid}}">
                                <input type="hidden" name='aid' value="{{$application->aid}}">
                                @csrf
                                <div class="row">
                                <div class="col-md-12">
                                    <label >Document Name</label>
                                    <select class="form-control" name="name">
                                        <option>CAC Document</option>
                                        <option>CV</option>
                                    </select>
                                </div>

                                <div style="margin-top:10px;" class="row col-12">
                                    <div class="col-md-8">
                                        <input type="file" name="document">
                                    </div>
                                    <div class="col-md-4">
                                        <button class="btn upload btn-success"  type="submit"><i style="color: white;" class="mdi mdi-upload"></i> </button>
                                    </div>
                                </div>

                                </div>
                            </form>

                            @endif

                </div>


            </div>

        </div>
        @endforeach

    </div>
    <!--row closed-->


@endsection
