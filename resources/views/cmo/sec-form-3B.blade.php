@extends('cmo.layouts.app')

@section('content')
    <style>
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Form Sec 3</h4>
            <p style="color:white;">Application form for the registration of broker/dealer, Corporate Investment Adviser,Fund/Portfolio Managers and other Corporate Entities under the investments and securities Act 2007.</p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->

    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                <form>

                    <div class="row">
                        <div class="form-group col">
                            <label>1. Registered Name </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>2. State if Private/Public Company/Partnership</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>3. Registered Address </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>4. Present Address (if different from 3.)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>5. Date of Incorporation/Registration </label>
                            <input class="form-control" type="date" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>6. Type of Registration (Tick which ever is applicable)</label>

                            <p>Venture	Capital	Fund	Manager	i.e. (Bank	Subsidiary) <input type="checkbox"></p>
                            <p>Venture	Capital	Company	(Fund	Manager)<input type="checkbox"></p>

                            <input class="form-control" type="text" placeholder="Any Other Category (Specify)">


                        </div>
                    </div><br>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>7. Is the Company/firm engaged in other activities? </p><br>
                            <textarea type="text" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>8. Have you any branch Office? </p><br>
                            <textarea type="text" class="form-control" placeholder="If yes, state particulars"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <lable>Yes</lable>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>9. Are you applying for registration of any branch? </p><br>
                            <textarea type="text" class="form-control" placeholder="If yes, give particulars"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <h6>10. Capital Structure</h6>

                    <div class="row">
                        <div class="form-group col">
                            <label>i. Authorized </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Shares of N </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Each amounting to N</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>ii. Issued</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Shares of N </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Each amounting to N  </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>iii. Paid up</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Shares of N </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Each amounting to N  </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>11. Names and Addresses of Members/Partners holding 5% or<br> more of the capital of the Firm/Company</h6>

                    <p>(a) Nigerian</p><br>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Amount (N)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>% Held</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <p>(b) Foreign</p><br>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Amount (N)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>% Held</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>


                    <h6>12. Particulars of Directories/Partners</h6>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Qualification</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Experience</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>


                    <div class="row">
                        <div class="form-group col">
                            <label>Previous Employment </label>
                            <input class="form-control" type="text" placeholder="If any">
                        </div>
                        <div class="form-group col">
                            <label>Date</label>
                            <input class="form-control" type="date" placeholder="If any">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Reasons for Leaving</label>
                            <textarea class="form-control" type="text" placeholder=""></textarea>
                        </div>

                    </div><br>

                    <h6>13. Particulars of Executive and Senior Staff</h6>

                    <div class="row">
                        <div class="form-group col-md-5">
                            <label>Name </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-5">
                            <label>Qualifications </label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label>Qualification date </label>
                            <input class="form-control" type="date" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Designation</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>14. State the Nominal and Market values of your investments<br>
                        securities as at the date of this application</h6>
                    <table border="1" style="table-layout: fixed; width: 100%;">

                        <tr>
                            <th scope="col" class="text-center">Security</th>
                            <th scope="col" class="text-center">Nominal Value (N)</th>
                            <th scope="col" class="text-center">Market Value (N)</th>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Ordinary Shares</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Preferences Shares</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Debentures</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Government Bonds</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Other Fixed Interest Bearing Securities</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Mutual Funds</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>

                        </tr>

                        <tr>
                            <th scope="row" class="text-center">(Open-ended)</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>

                        </tr>



                    </table><br>

                    <p>(ii) Please furnish details of commission sharing agreements(s) (if any) you have entered into
                    <br>your incubates/start-ups
                    </p>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name of incubates/start-ups</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>Basis of Renumeration</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>Year(s)</label>
                            <input class="form-control" type="text" placeholder="">
                        </div>


                    </div><br>

                    <h6>15.State the build-up of the following items over the last five years or since incorporation if less than five years</h6>
                    <table border="1" style="table-layout: fixed; width: 100%;">

                        <tr>
                            <th scope="col" class="text-center">Debtors</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>



                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Short Term Loans (Assets)</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Investments (At year and market)</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Quoted</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Unquoted</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Other Short Term Assets</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Creditors</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Short Term Loans (Liabilities)</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Other Short term Liabilities</th>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>
                            <td><input type="text"></td>


                        </tr>



                    </table><br>


                    <h6>16. ANSWER THE FOLLOWING QUESTION</h6><br>


                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>i.  How	many staff are under your employment? </p><br>
                            <textarea type="text" class="form-control" placeholder="If yes, give details "></textarea><br>
                            <label class="" for="customFile">attach(list of	names, address,	 nature	 of
                                employment and	other details)…</label><br>
                            <input type="file" class="" id="customFile">

                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>ii. Is there	 subsisting	 between you and any of	the incubatee/start-up technical/management
                                agreement?</p><br>
                            <textarea type="text" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>


                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>17. Has	 the	applicant	or	any	of	its	affiliates	been denied	 registration	or	expelled	 from	membership	of
                                any	 Government	 agency,	 Securities	 Exchange	 or	 Association	 of	 Securities	 Dealers	 or	 any	 other
                                SRO?</p><br>
                            <textarea type="text" class="form-control" placeholder="If yes, give reasons"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <p>YES</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <p>NO</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>18. Has	your	membership	or	that	of	any	affiliates	in	any	of	the	institutions	mentioned	in	(17)	at	any	time
                                been	revoked,	suspended	withdrawn?.</p><br>
                            <textarea type="text" class="form-control" placeholder="If yes, give reasons"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>19. Has	the	applicant or any of	its	affiliates,	to	the	best of	the	applicant’s	knowledge	and	belief	operated
                                under and carried on business under	 any name other	than as	shown in this
                                application?</p><br>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>20. Is any depository holding any of the assets of the Company/Firm? </p><br>
                            <textarea type="text" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>21. Has any person or company undertaken to act as a guarantor in relation to the financial or other undertakings of your incubator?</p><br>
                            <textarea type="text" class="form-control" placeholder="If yes,give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <p>YES</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <p>NO</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>22. Has a subordinated agreement been executed by the creditor(s) in relation to loans owing by your incubator?</p><br>
                            <textarea type="text" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>23. Has your company/firm or any of its affiliates been subject to any winding up order/receivership arrangement?</p><br>
                            <textarea type="text" class="form-control" placeholder="If yes provide details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <p>YES</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <p>NO</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <h6>24. Has your company/firm or any of its affiliates been involved in any litigation within Ten preceding this application over.</h6>

                    <div class="row bottom-space">


                        <div class="form-group col-md-10">
                            <p>i. filing of any application for registration?</p><br>
                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>ii. your	activity in	capital	market?</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <p>No</p>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>iii. any offence relating to the code of the business of Broker/Dealer, Investment Adviser, Bank or Insurance?	</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>iv.  any criminal offence?</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>v. any civil offence?</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>vi. Where the answer of any of the items 24(i)-(v) is yes give details</label><br>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>25.  Has any of your incubates any fidelity bond for loans?</p><br>
                            <textarea type="text" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="form-group col">
                            <label>i. Describe	modalities	for	handling	application,	processing	them,	collection	and	accounting	for	funds</><br>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>ii. Upload a	copy of	typical	contract with your incubates/start ups</label><br>
                            <input type="file" class="form-control" id="customFile">
                        </div>

                    </div><br>

                    <div class="row">
                         <h6>iii. List	of	clients	incubates/start-ups,	(corporate	clients	only)	being	nurtured	by	you</h6><br>
                        <div class="form-group col">
                            <label>Name</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Type of funding</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                    </div>


                    <div class="row">

                        <div class="form-group col">
                            <label>iv. If	financed	by	loan,	state	number	of	year(s)	for	repayment.</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>v.State	the	interest	charged	on	loans	to	incubatees.</label>
                            <input type="text" class="form-control" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>	Any	other	information	considered	relevant	to	the	nature	of	services	rendered	by	the	company	</label>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <p>26. ANNUAL AUDIT OF THE FIRM/COMPANY/PARTNERSHIP</p>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>i. State the name(s) and address(es) of the auditor(s) of the applicant</label><br>
                            <input type="text" class="form-control" placeholder="Name">
                            <input type="text" class="form-control" placeholder="Address">
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>ii. Has there been any change over the past five years?</label><br>
                            <textarea type="text" class="form-control" placeholder=".If so ,why?"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col">
                            <label>iii. Is the auditing firm, to the best of your knowledge and believe, also responsible for the auditing of any of your affiliates?</label><br>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>

                        <div class="form-group col">
                            <label>Is the auditing firm, to	the	best of	your knowledge	and	relief,	also responsible for	the	auditing	of	any
                                of	the	(SMEs)	being	funded	by	you?	</label><br>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>v. State the basis of remuneration for services rendere</label><br>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>vi. Has there been any adverse comment by the auditors on the value/mix of the securities in your investment portfolio over the past five years?</label><br>
                            <textarea type="text" class="form-control" placeholder=".If so ,why?"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label>Yes</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                        <div class="form-group col-md-1">
                            <label>No</label>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                            </div>
                        </div>

                    </div>


                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>vii. Is the auditing firm or any of the partners or principal officers, to the best of your knowledge and believe, holding any substantial securities in the companies within your investment portfolio, or is any of them holding a directorate seat in the company?</p><br>
                            <textarea type="text" class="form-control" placeholder=""></textarea>
                        </div>
                    </div>


                    <h5 class="text-center">AFFIDAVIT</h5>
                    <p class="text-center">
                        I………………………………………………………………………………………………<br>	Name	in	Full

                        <br>Residing at……………………………………………………………<br>



                    </p>
                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" type="text" placeholder="Full Name">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" type="text" placeholder="Address">
                        </div>

                    </div><br>


                    <p>
                        Hereby make an Oath and state as follows: <br>
                        (1) That I am Director/Partner/officer of the applicant company/firm<br>
                        (2) That I am duly authorized by the company/firm to sign this application;<br>
                        (3) That the statements	of	facts	made	in	this	application	and	any	exhibit	attached	thereto	are	to
                        the	best of	my	knowledge	and	belief	true.<br>
                        (4) That I	have	enclosed	the	appropriate	Registration fees as stipulated	by	the	Securities	and
                        Exchange Commission	(SEC) <br>
                        <span class="ml-auto">Despondent<br>………………………………………………</span><br>
                        <span class="text-center">
                              SWORN at the………………………………………………………court this……………………………day
                              Of………………………………………………year………………………………………………………………<br>
                              BEFORE ME<br>
                              ………………………………………………………………<br>
                              COMMISSIONER OF OATHS<br>
                            </span>

                    </p>

                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" type="text" placeholder="Sworn at the">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" type="text" placeholder="court this">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" type="text" placeholder="day of">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" type="text" placeholder="year">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" type="text" placeholder="Before me">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" type="text" placeholder="Commissioner of oaths">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload Despondent Signature</label><br>
                            <input type="file" class="" id="customFile">
                        </div>
                    </div><br>


                    <h6>i. Each	application	filed must be accompanied by</h6>

                    <div class="row">

                        <div class="form-group col">
                            <label class="" for="customFile">(a) Upload a audited financial statements for the latest one year</label><br>
                            <input type="file" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">(b) Upload a detailed profit and loss for same period under (a);</label><br>
                            <input type="file" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload a copy of the certificate of Incorporation </label><br>
                            <input type="file" class="" id="customFile">
                        </div>
                    </div><br>

                    <div class="row">

                        <div class="form-group col">
                            <label class="" for="customFile">Upload Sworn	undertaking	to	keep	such	records	and	render	such	returns	as	may	be	specified	by	the	Commission	from
                                time	to	time</label><br>
                            <input type="file" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload a copy of the Memorandum and Article of <br>Association/Partnership Deed certified by the Corporate
                                Affair Commission </label><br>
                            <input type="file" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload a Sworn undertaking	to	keep proper	records	and	render	returns;</label><br>
                            <input type="file" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload a Fidelity Bond representing 20%	of	paid up	capital?</label><br>
                            <input type="file" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload a Copy of form CO7 certified by	the	CAC</label><br>
                            <input type="file" class="" id="customFile">
                        </div>
                    </div><br>

                    <h6>ii. An affiliate for the purpose of the Form is a company or Partnership in which the applicant has at
                        least 20% ownership interest however so called.</h6>


                </form>

            </div>
        </div>
        <!--row closed-->

    </div>


@endsection


