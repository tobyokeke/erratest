@extends('cmo.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard.css')}}">
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard_theme_dots.css')}}">
    <style>



    </style>
@endsection

@section('scripts')
    <!--Formvalidation js-->
    <script src="{{url('admin/assets/js/formvalidation.js')}}"></script>

@endsection

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Companies</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Companies</a></li>
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">
        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Company FILE NUMBER -  {{$company->fileNumber}}</h4>
                    @if($company->cmoStatus == 'pendingRegistration')
                        <span class="badge badge-warning" style="float: right;">Pending</span>
                    @endif

                    @if($company->cmoStatus == 'illegalOperator')
                        <span class="badge badge-danger" style="float: right;">Illegal</span>
                    @endif

                </div>
                <div class="card-body users">
                    <div class="form-group row">
                        <label for="inputName" class="col-md-3 col-form-label bold-label">Name</label>
                        <div class="col-md-9 user-details">
                            {{strtoupper($company->operatorName)}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-md-3 col-form-label bold-label">RC NUMBER</label>
                        <div class="col-md-9 user-details">
                            {{$company->rcNumber}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-md-3 col-form-label bold-label">CAC NUMBER</label>
                        <div class="col-md-9 user-details">
                            {{$company->cacNumber}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-md-3 col-form-label bold-label">REGISTRATION DATE</label>
                        <div class="col-md-9 user-details">
                            {{$company->registrationDate}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-md-3 col-form-label bold-label">FIDELITY BOND</label>
                        <div class="col-md-9 user-details">
                            {{$company->fidelityBond}}
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="inputName" class="col-md-3 col-form-label bold-label">PAID UP CAPITAL</label>
                        <div class="col-md-9 user-details">
                            {{$company->paidUpCapital}}
                        </div>
                    </div>


                    @foreach($company->Functions as $function)
                    <div class="form-group row">
                        <label for="inputName" class="col-md-3 col-form-label bold-label">FUNCTION</label>
                        <div class="col-md-9 user-details">
                            {{$function->name}}
                        </div>
                    </div>
                    @endforeach

                    {{--<div class="form-group row">--}}
                        {{--<label for="inputName" class="col-md-3 col-form-label bold-label">Phone</label>--}}
                        {{--<div class="col-md-9 user-details">--}}
                            {{--{{$user->phone}}--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    {{--<div class="form-group row">--}}
                        {{--<label for="inputName" class="col-md-3 col-form-label bold-label">Registered</label>--}}
                        {{--<div class="col-md-9 user-details">--}}
                            {{--{{$user->created_at->toDayDateTimeString()}}--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    <div class="form-group row">
                        <label for="inputName" class="col-md-3 col-form-label bold-label">Applications</label>
                        <div class="col-md-9 user-details">
                            0
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!--row closed-->



@endsection
