@extends('cmo.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Companies</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Companies</a></li>
        </ol>
    </div>
    <!--page-header closed-->



    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Company</h4>

                </div>
                <div class="card-body">
                    <table class="table table-borderless text-nowrap mb-0">
                        <thead>
                        <tr>
                            <th>S/N</th>
                            <th>File Number</th>
                            <th>Name</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $count = 1; ?>

                        @foreach($companies as $company)
                            <tr>
                                <td>{{$count}}</td>
                                <td>
                                    @if(empty($company->fileNumber))
                                        Not Assigned Yet
                                    @else
                                    {{$company->fileNumber}}
                                    @endif
                                </td>
                                <td>{{$company->operatorName}}</td>
                                <td>

                                    @if($company->cmoStatus == 'pendingRegistration')
                                        <span class="badge badge-warning">PENDING</span>
                                    @endif

                                    @if($company->cmoStatus == 'expiredBond')
                                        <span class="badge badge-danger">EXPIRED BOND</span>
                                    @endif

                                    @if($company->cmoStatus == 'illegalOperator')
                                        <span class="badge badge-danger">ILLEGAL</span>
                                    @endif

                                    @if($company->cmoStatus == 'invalidRegistration')
                                        <span class="badge badge-danger" style="background-color: darkred">INVALID</span>
                                    @endif

                                    @if($company->cmoStatus == 'suspendedRegistration')
                                        <span class="badge badge-warning" >SUSPENDED</span>
                                    @endif

                                    @if($company->cmoStatus == 'validRegistration')
                                        <span class="badge badge-warning" >APPROVED</span>
                                    @endif

                                </td>
                                <td>{{$company->created_at->toDayDateTimeString()}}</td>
                                <td>
                                    <a href="{{route('cmo.companies.details',['company' => $company->cid])}}">
                                        <span class="badge badge-pill badge-primary">View</span>
                                    </a>

                                </td>
                            </tr>
                            <?php $count++; ?>
                        @endforeach

                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>

    <!--row closed-->


@endsection
