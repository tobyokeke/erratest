@extends('cmo.layouts.app')

@section('scripts')
    <!--Formvalidation js-->
    <script src="{{url('assets/js/formvalidation.js')}}"></script>
@endsection
@section('content')
    <style>
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
        .page-title{
            margin-left:15px;
        }

    </style>


    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Applications</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Applications</a></li>
        </ol>
    </div>
    <!--page-header closed-->


    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-12">
            <p style="color:white;">

                This form would enable you register a new company. You shall be assigned to this company
                on completion of the registration process.
            </p>
        </div>
    </div>
    <!--page-header closed-->




    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">

            <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h4>COMPANY REGISTRATION</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{route('cmo.companies.add.post')}}" id="form-validation" novalidate method="post">

                            @csrf
                            <div class="">

                                {{--<div class="form-group">--}}
                                    {{--<label >File Number</label>--}}
                                    {{--<input type="text" class="form-control" name="fileNumber" placeholder="Enter file number">--}}
                                {{--</div>--}}

                                <div class="form-group">
                                    <label >Operator Name</label>
                                    <input type="text" class="form-control" name="operatorName" value="{{old('operatorName')}}" placeholder="Enter Operator Name">
                                </div>

                                <div class="form-group">
                                    <label >RC Number</label>
                                    <input type="number" class="form-control" name="rcNumber" placeholder="Enter RC Number">
                                </div>

                                <div class="form-group">
                                    <label >CAC Number</label>
                                    <input type="number" class="form-control" name="cacNumber" placeholder="Enter CAC Number">
                                </div>

                                <div class="form-group">
                                    <label >Registration Date</label>
                                    <input type="date" class="form-control" name="registrationDate" placeholder="Enter Registration Date">
                                </div>

                                <div class="form-group">
                                    <label >Fidelity Bond</label>
                                    <input type="number" step="0.01" class="form-control" name="fidelityBond" placeholder="Enter Fidelity Bond">
                                </div>

                                <div class="form-group">
                                    <label >Paid-Up Capital</label>
                                    <input type="number" step="0.01" class="form-control" name="paidUpCapital" placeholder="Enter Paid-Up Capital">
                                </div>

                                <p>FUNCTIONS</p>
                                <button type="button" id="addButton" class="btn btn-primary">ADD FUNCTION</button>

                                <div class="row col-12" id="functions">
                                    <div id="function" class="col-12 visually-hidden">
                                        <div class="form-group overflow-hidden col-md-4 col-sm-12">
                                            <label>Function</label>

                                            <select name="functions[]" class="form-control select2 w-100" >
                                                <option selected disabled value="null">None</option>
                                                @foreach($functions as $function)
                                                    <option value="{{$function->fid}}">{{$function->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                </div>

                            </div>
                            <button type="submit" class="btn btn-primary mt-1 mb-0">Submit</button>
                        </form>
                    </div>
                </div>
            </div>

        </div>
        <!--row closed-->

        <script>
            $(document).ready(function(){

                $('#addButton').on('click',function(){
                    console.log('clicked');
                    let funct = $('#function');
                    $('#functions').append(funct.html());

                });
            });
        </script>

    </div>
@endsection


