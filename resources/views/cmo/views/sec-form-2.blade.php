@extends('cmo.layouts.app')

@section('content')
    <style>
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Form Sec 2</h4>
            <p style="color:white;">Application form for Registration of Sponsored Individuals under the Investment and Securities act 2007</p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->

    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                    <form>

                        <h5>1.</h5>
                        <div class="row">
                            <div class="col-md-6">
                                <p>State the capital market function for which you require registration:</p>
                            </div>
                            <div class="col-md-6">

                                <p><strong>{{$form2->capital_market}}</strong></p>
                            </div><br>
                            <p><b>N.B; </b>PLEASE NOTE THAT, ALL INFORMATION PROVIDED WILL BE SUBJECT TO VERIFICATION BY THE COMMISSION</p>
                        </div><br>
                        <h5>2. Personal Details</h5>
                        <div class="row">
                            <div class="form-group col">
                                <label>Surname</label>
                                <input class="form-control" type="text" name="surname" placeholder="{{$form2 ->surname}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Other Name(s)</label>
                                <input class="form-control" type="text" name="othernames" placeholder="{{$form2 ->othernames}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Alias</label>
                                <input class="form-control" type="text" name="alias" placeholder="{{$form2 ->alias}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Residential Address</label>
                                <input class="form-control" type="text" name="residential_address" placeholder="{{$form2 ->residential_address}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Office Address</label>
                                <input class="form-control" type="text" name="office_address" placeholder="{{$form2 ->office_address}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Official position in sponsoring company</label>
                                <input class="form-control" name="official_position" type="text" placeholder="{{$form2 ->official_position}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Employment (Part Time/ Full time)</label>
                                <input class="form-control" name="employment" type="text" placeholder="{{$form2 ->employment}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>E-mail</label>
                                <input class="form-control" name="email" type="email" placeholder="{{$form2 ->email}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Date of Birth</label>
                                <input class="form-control" name="dob" type="date" placeholder="{{$form2 ->dob}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Place of Birth</label>
                                <input class="form-control" name="pob" type="text" placeholder="{{$form2 ->pob}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>State of Birth</label>
                                <input class="form-control" name="sob" type="text" placeholder="{{$form2 ->sob}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Nationality</label>
                                <input class="form-control" name="nationality" type="text" placeholder="{{$form2 ->nationality}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>State of Origin</label>
                                <input class="form-control" name="soo" type="text" placeholder="{{$form2 ->soo}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>GSM Number</label>
                                <input class="form-control" name="number" type="text" placeholder="{{$form2 ->number}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Sex (M/F)</label>
                                <input class="form-control" name="sex" type="text" placeholder="{{$form2 ->sex}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Marital Status</label>
                                <input class="form-control" name="marital_status" type="text" placeholder="{{$form2 ->marital_status}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Any visible distinguished mark</label>
                                <input class="form-control" name="visible_mark" type="text" placeholder="{{$form2 ->visible_mark}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Passport/Driver's License Number</label>
                                <input class="form-control" name="license_no" type="text" placeholder="{{$form2 ->license_no}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Fresh/Reissued</label>
                                <input class="form-control" name="fresh" type="text" placeholder="{{$form2 ->fresh}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Place of issue</label>
                                <input class="form-control" name="poi" type="text" placeholder="{{$form2 ->poi}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Date of issue</label>
                                <input class="form-control" name="doi" type="date" placeholder="{{$form2 ->doi}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Date of expiry</label>
                                <input class="form-control" name="doe" type="date" placeholder="{{$form2 ->doe}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Name of Father</label>
                                <input class="form-control" name="father_name" type="text" placeholder="{{$form2 ->father_name}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Name of Mother</label>
                                <input class="form-control" name="mother_name" type="text" placeholder="{{$form2 ->mother_name}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Maiden Name</label>
                                <input class="form-control" name="maiden_name" type="text" placeholder="{{$form2 ->maiden_name}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Name of spouse (If married)</label>
                                <input class="form-control" name="spouse_name" type="text" placeholder="{{$form2->spouse_name}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Employment of spouse</label>
                                <input class="form-control" name="employment_spouse" type="text" placeholder="{{$form2->employment_spouse}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Name of Employer</label>
                                <input class="form-control" name="name_employer" type="text" placeholder="{{$form2->name_employer}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Employer's Address</label>
                                <input class="form-control" name="employer_address" type="text" placeholder="{{$form2->employer_address}}" disabled>
                            </div>
                        </div><br>

                        <h5>3. Bankers</h5>
                        <div class="row">
                            <div class="form-group col">
                                <label>Name of Bankers</label>
                                <input class="form-control" name="banker_name" type="text" placeholder="{{$form2->banker_name}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Address</label>
                                <input class="form-control" name="banker_address" type="text" placeholder="{{$form2->banker_address}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Branch</label>
                                <input class="form-control" name="branch" type="text" placeholder="{{$form2->branch}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Type of account (current account only)</label>
                                <input class="form-control" name="account_type" type="text" placeholder="{{$form2->account_type}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Account Number</label>
                                <input class="form-control" name="account_number" type="text" placeholder="{{$form2->account_number}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Date Opened</label>
                                <input class="form-control" name="date_op" type="text" placeholder="{{$form2->date_op}}" disabled>
                            </div>
                        </div><br>

                        <h5>4. Employment History</h5>
                        <h6>i. Current Employment</h6>
                        <div class="row">
                            <div class="form-group col">
                                <label>Name of employer</label>
                                <input class="form-control" name="name_of_employer" type="text" placeholder="{{$form2->name_of_employer}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Address</label>
                                <input class="form-control" name="emp_address" type="text" placeholder="{{$form2->emp_address}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Date From</label>
                                <input class="form-control" name="date_from" type="text" placeholder="{{$form2->date_from}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Date to</label>
                                <input class="form-control" name="date_to" type="text" placeholder="{{$form2->date_to}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Part time or full time</label>
                                <input class="form-control" name="part_full" type="text" placeholder="{{$form2->part_full}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Position Held</label>
                                <input class="form-control" name="position_held" type="text" placeholder="{{$form2->position_held}}" disabled>
                            </div>
                        </div><br>

                        <h6>ii.Previous Employment</h6>
                        <div class="row">
                            <div class="form-group col">
                                <label>Name of employer</label>
                                <input class="form-control" name="pre_name_employer" type="text" placeholder="{{$form2->pre_name_employer}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Address</label>
                                <input class="form-control" name="pre_address" type="text" placeholder="{{$form2->pre_address}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Date From</label>
                                <input class="form-control" name="pre_date_from" type="text" placeholder="{{$form2->pre_date_from}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Date to</label>
                                <input class="form-control" name="pre_date_to" type="text" placeholder="{{$form2->pre_date_to}}" disabled>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Part time or full time</label>
                                <input class="form-control" name="pre_part_full" type="text" placeholder="{{$form2->pre_part_full}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Position Held</label>
                                <input class="form-control" name="pre_position_held" type="text" placeholder="{{$form2->pre_position_held}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Reason(s) for leaving</label>
                                <textarea class="form-control" name="pre_reasons_leaving" type="text" placeholder="{{$form2->pre_reasons_leaving}}" disabled></textarea>
                            </div>
                        </div><br>

                        <h5>5. Education</h5>
                        <h6>i. Give the name(s) of Educational Institutions(s) attended, Qualifications Obtained and<br>
                             dates from Secondary School to Date  
                        </h6><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Name of institution attended from secondary to date</label>
                                <input class="form-control" name="edu_institution" type="text" placeholder="{{$form2->edu_institution}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Address</label>
                                <input class="form-control" name="edu_address" type="text" placeholder="{{$form2->edu_address}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Date From</label>
                                <input class="form-control" name="edu_date_from" type="text" placeholder="{{$form2->edu_date_from}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Date to</label>
                                <input class="form-control" name="edu_date_to" type="text" placeholder="{{$form2->edu_date_to}}" disabled>
                            </div>

                        </div><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Course of Study</label>
                                <input class="form-control" name="edu_course_study" type="text" placeholder="{{$form2->edu_course_study}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Qualifications Obtained</label>
                                <input class="form-control" name="edu_qualification_obtained" type="text" placeholder="{{$form2->edu_qualification_obtained}}" disabled>
                            </div>
                        </div><br>

                        <h6>Professional Qualifications <br> (If not obtained through regular institutions of learning</h6>
                        <div class="row">
                            <div class="form-group col">
                                <label>Awarding institution and Address</label>
                                <input class="form-control" name="awarding_institute" type="text" placeholder="{{$form2->awarding_institute}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Certificate</label>
                                <input class="form-control" name="certificate" type="text" placeholder="{{$form2->certificate}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Year</label>
                                <input class="form-control" name="year" type="text" placeholder="{{$form2->year}}" disabled>
                            </div>
                        </div><br>

                        <h6>Brokers/Dealers Examination</h6>
                        <p>Apart from educational qualifications, every Broker/Dealer is required to sit and pass<br>
                            Broker/Dealer professional examination conducted by the chartered Institute of<br>
                            Stockbrokers or any other recognized professional body.</p>
                        <div class="row">
                            <div class="form-group col">
                                <label>Date of examinations</label>
                                <input class="form-control" name="bro_date_exam'" type="date" placeholder="{{$form2->bro_date_exam}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Subjects</label>
                                <input class="form-control" name="subject" type="text" placeholder="{{$form2->subject}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Test scores</label>
                                <input class="form-control" name="test_scores" type="text" placeholder="{{$form2->test_scores}}" disabled>
                            </div>
                        </div><br>


                        <h5>6. Reference</h5>
                        <p>Give at least three names but excluding relations and persons associated with the <br>
                            sponsoring firm as referees. Please note that a nominated referee cannot be used by two <br>
                            or more sponsored individuals in the same company
                        </p><br>
                        <div class="row">
                            <div class="form-group col">
                                <label>Name </label>
                                <input class="form-control" name="ref_name" type="text" placeholder="{{$form2->ref_name}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Address</label>
                                <input class="form-control" name="ref_address" type="text" placeholder="{{$form2->ref_address}}" disabled>
                            </div>
                            <div class="form-group col">
                                <label>Occupation</label>
                                <input class="form-control" name="ref_occupation" type="text" placeholder="{{$form2->ref_occupation}}" disabled>
                            </div>

                        </div><br>

                        <h6 class="text-center">Answer YES or NO to Each of the Following Questions, And if the Answer to any question<br>
                            is yes, Supply Further Details.
                            <br>
                            <span class="text-dark">Tick for Yes and Leave empty for No</span>
                             
                        </h6><br>

                        <h6>7. Registration/membership</h6>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>i.  Have you ever been denied registration or expelled from membership of any government agency, Securities Exchange or any National Securities Association?</p><br>
                                <textarea name="registration_details" type="text" class="form-control" placeholder="{{$form2->registration_details}}" disabled></textarea>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input

                                            @if($form2->registration == 'yes')

                                                    selected

                                             @endif
                                            name="registration" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>


                        </div>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>ii.  Have you ever been subject to any disciplinary action by any of the bodies mentioned by 7(i) above?</p><br>
                                <textarea name="registration_details2" type="text" class="form-control" placeholder="{{$form2->registration_details2}}" disabled></textarea>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            @if($form2->registration2 == 'yes')
                                                    selected
                                            @endif
                                            name="registration2" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>


                        </div>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>iii.	Has your membership of the institution(s) mentioned in 7(1) at any time been revoked, suspended or cancelled?</p><br>
                                <textarea name="registration_details3" type="text" class="form-control" placeholder="{{$form2->registration_details3}}" disabled></textarea>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            @if($form2->registration3 =='yes')
                                                    selected
                                            @endif
                                            name="registration3" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>iv.	Has any Broker/Dealer or any other body with whom you have associated been affected in 7(i) (ii) (iii) above? </p><br>
                                <textarea name="registration_details4" type="text" class="form-control" placeholder="{{$form2->registration_details4}}" disabled></textarea>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input

                                            @if($form2->registration4 == 'yes')
                                                    selected
                                            @endif
                                            name="registration4" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>

                        <h6>8. CHANGE OF NAME: (through marriage, <br> divorce or court order, etc)</h6>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p> Have you ever had, used, operated under any name other than name mentioned in item 2 of this form or have you ever been known under other names? </p><br>
                                <textarea name="change_of_name_details" type="text" class="form-control" placeholder="{{$form2->change_of_name_details}}" disabled></textarea>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input

                                            @if($form2->change_of_name == 'yes')

                                                    selected
                                             @endif
                                            name="change_of_name" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>
                        </div>

                        <h6>9. OFFENCES UNDER THE LAW</h6>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>i. Have you been convicted under any federal or state law in Nigeria any offence relating to trading Securities or with any related offence or been a party to any proceeding taken on account of fraud arising out of any trade in or advised in respect of securities?</p><br>
                                <textarea name="offences1_details" type="text" class="form-control" placeholder="{{$form2->offences1_details}}" disabled></textarea>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input

                                            @if($form2->offences1 == 'yes')
                                                selected
                                            @endif
                                            name="offences1" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>ii.	Have you ever been convicted under any Federal or state law contravention or other criminal offences not noted in 9(i) above? </p><br>
                                <textarea name="offences2_details" type="text" class="form-control" placeholder="{{$form2->offences2_details}}" disabled></textarea>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            @if($form2->offences2 == 'yes')
                                                 selected
                                            @endif
                                            name="offences2" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>


                        <h5>10. JUDGEMENT OR GARNISHEE ORDER, BANKRUPTCY</h5>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>Has any judgment or garnishee order been rendered against you or is any judgment or garnishee order outstanding against you in any court of law in Nigeria or elsewhere for damages or other relief of a fraud or for any other reason whatsoever?</p><br>
                                <textarea name="judgement_details" type="text" class="form-control" placeholder="{{$form2->judgement_details}}" disabled></textarea><br>
                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Upload Discharged copy</label>
                                    <input name="judgement_copy" type="file" class="form-control-file" id="exampleFormControlFile1" disabled>
                                </div>
                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            @if($form2->judgement == 'yes')
                                                  selected
                                            @endif
                                            name="judgement" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>

                        <h5>11. SURETY BOND OR FIDELITY BOND</h5>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>Have you ever applied for surety bond or fidelity bond and been refused?</p><br>
                                <textarea name="surety_details" type="text" class="form-control" placeholder="{{$form2->surety_details}}" disabled></textarea><br>

                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            @if($form2->surety == 'yes')
                                                 selected
                                            @endif
                                            name="surety" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>


                        <h5>12.	BUSINESS ACTIVITIES</h5>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>Will you be actively engaged in the business of your sponsoring firm and devote the major portion of your time there to? </p><br>
                                <textarea name="business_activities1" type="text" class="form-control" placeholder="{{$form2->business_activities1}}" disabled></textarea>
                            </div>


                        </div>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>Are you engaged in any other business, or have any other employment for gain except your occupation with your sponsoring firm? </p><br>
                                <textarea name="business_activities2" type="text" class="form-control" placeholder="{{$form2->business_activities2}}" disabled></textarea>
                            </div>


                        </div>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>Are you a partner, Director, Officer, Shareholder or Contributor of capital of partnership or Company other than your sponsoring firm? Having as its principal activity the business of a Broker/Dealer or adviser in Securities? </p><br>
                                <textarea name="business_activities3" type="text" class="form-control" placeholder="{{$form2->business_activities3}}" disabled></textarea>
                            </div>

                        </div>


                        <h5>13.	FOR INVESTMENT ADVISER/FUND/VENTURE FUND MANAGERS ONLY</h5>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>Are your clients required to maintain an account with you?</p><br>
                                <textarea name="investment_adviser_details" type="text" class="form-control" placeholder="{{$form2->investment_adviser_details}}" disabled></textarea><br>

                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            @if($form2->investment_adviser == 'yes')
                                                    selected
                                            @endif
                                            name="investment_adviser" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>

                        <h5>14.</h5>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>14. (a) Do you maintain separate bank account for your client? </p><br>
                                <textarea name="number14a_details" type="text" class="form-control" placeholder="{{$form2->number14a_details}}" disabled></textarea><br>

                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            @if($form2->number14a =='yes')
                                                    selected
                                            @endif

                                            name="number14a" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>

                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>14. (b) How regular are your clients giving statement of account?</p><br>
                                <textarea name="number14b_details" type="text" class="form-control" placeholder="{{$form2->number14b_details}}" disabled></textarea><br>

                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            @if($form2->number14b ==' yes')
                                                    selected

                                             @endif
                                            name="number14b" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>


                        <h5>15.</h5>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>Does the nature of your business include investment supervisory services?</p><br>
                                <textarea name="number15_details" type="text" class="form-control" placeholder="{{$form2->number15_details}}" disabled></textarea><br>

                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            @if($form2->number15 == 'yes')
                                                    selected
                                            @endif
                                            name="number15" class="form-check-input"
                                           type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>


                        <h5>number16.</h5>
                        <div class="row bottom-space">

                            <div class="form-group col-md-6">
                                <p>Give the basis upon which you receive compensation for such services.</p><br>
                                <textarea name="number16_details" type="text" class="form-control" placeholder="{{$form2->number16_details}}" disabled></textarea><br>

                            </div>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input

                                            @if($form2->number16 == 'yes')
                                                    selected
                                            @endif
                                            name="number16" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1" disabled>
                                </div>
                            </div>

                        </div>


                        <h5 class="text-center">AFFIDAVIT</h5>
                        <p class="text-center">
                            I……………………{{$form2->affidavit_name}}……………………………<br>
                            NAME IN FULL<br>
                            Of……………{{$form2->affidavit_of}}………………………
                        </p>
                       <br>


                        <p>
                            Hereby make an oath and state as follows: <br>
                            (1) That I am……………………………………………… the applicant Herein seeking registration with the Securities and Exchange Commission<br>
                            (2) That I have read and understood the questions in the application form as well as the answers given and that to the best of my knowledge and believe the statement of fact made therein and the attachment are true.<br>

                            <span class="ml-auto">DESPONDENT……………………………………………</span><br>
                            <span class="text-center">
                              SWORN to at…………………………{{$form2->first_input}}……………………………court this…………………{{$form2->second_input}}…………..day
                              Of………………………{{$form2->third_input}}………………………year……………………………{{$form2->fourth_input}}…………………………………
                              BEFORE ME
                              ………………………{{$form2->fifth_input}}………………………………………
                              COMMISSIONER OF OATHS
                            </span>

                        </p>
                        <br>

                        <div class="row">
                            <h6>SPONSORING FIRM CERTIFICATION</h6>
                            <p>We hereby certify that the particulars rendered above by the applicant (whose signature is appended below) are to the best of our knowledge, true and correct and we undertake to notify the Securities and Exchange Commission in writing of any material change therein as prescribed by the Act.</p>
                            <div class="form-group col">
                                <label>NAME OF SPONSORING FIRM</label><br>
                                <input name="sponsoring_firm" class="form-control" type="text" placeholder="{{$form2->sponsoring_firm}}" disabled><br>
                            </div>
                            <div class="form-group col">
                                <label class="custom-file-label2" for="customFile2">APPLICANT`S SIGNATURE</label><br>
                                <input name="applicant_sig" type="file" class="custom-file-input2" id="customFile2"><br>
                            </div>
                            <div class="form-group col">
                                <label class="" for="customFile">SIGNATURE OF DIRECTOR /PARTNER/SECRETARY</label><br>
                                <input name="director_sig" type="file" class="" id="customFile">
                            </div>

                        </div>

                    </form>

            </div>
        </div>
        <!--row closed-->

    </div>
@endsection
