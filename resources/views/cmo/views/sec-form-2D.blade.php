@extends('cmo.layouts.app')

@section('content')
    <style>
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Form Sec 2D</h4>
            <p style="color:white;">Application form for fit and persons (Sponsored Individuals, Directors/Partners) for registration in the capital market under the investments and Securities ACT 2007 </p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->

    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">

                <form>

                    <h5>SECTION A</h5>
                    <div class="row">
                        <div class="form-group col">
                            <label>Name in Full </label>
                            <input class="form-control" name="name" type="text" placeholder="{{$form2D->name}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Sponsoring Company</label>
                            <input class="form-control" name="sponsoring_company" type="text" placeholder="{{$form2D->sponsoring_compnay}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Current Residential Address </label>
                            <input class="form-control" name="res_address" type="text" placeholder="{{$form2D->res_address}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Permanent Home Address</label>
                            <input class="form-control" name="perm_address" type="text" placeholder="{{$form2D->perm_address}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Mailing Address</label>
                            <input class="form-control" name="mail_address" type="text" placeholder="{{$form2D->mail_address}}" disabled>
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Official E-mail </label>
                            <input class="form-control" name="official_email" type="email" placeholder="{{$form2D->official_email}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Personal E-mail</label>
                            <input class="form-control" name="personal_email" type="email" placeholder="{{$form2D->personal_email}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>GSM Number </label>
                            <input class="form-control" name="gsm_number" type="text" placeholder="{{$form2D->gsm_number}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Landline Number</label>
                            <input class="form-control" name="landline_number" type="text" placeholder="{{$form2D->landline_number}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Date of Birth </label>
                            <input class="form-control" name="dob" type="date" placeholder="{{$form2D->dob}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Place of Birth</label>
                            <input class="form-control" name="pob" type="text" placeholder="{{$form2D->pob}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>State of origin </label>
                            <input class="form-control" name="soo" type="text" placeholder="{{$form2D->soo}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Nationality</label>
                            <input class="form-control" name="nationality" type="text" placeholder="{{$form2D->nationality}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Sex(M/F)</label>
                            <input class="form-control" name="sex" type="text" placeholder="{{$form2D->sex}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Marital Status</label>
                            <input class="form-control" name="marital_status" type="text" placeholder="{{$form2D->marital_status}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name of Next of Kin </label>
                            <input class="form-control" name="next_kin_name" type="text" placeholder="{{$form2D->next_kin_name}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Next of Kin Telephone Number</label>
                            <input class="form-control" name="next_kin_number" type="text" placeholder="{{$form2D->next_kin_number}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Next	of	Kin(s) Permanent Residential Address</label>
                            <input class="form-control" name="next_kin_address" type="text" placeholder="{{$form2D->next_kin_address}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Relationship	– Son/Daughter, Wife, Father/Mother</label>
                            <input class="form-control" name="next_kin_relationship" type="text" placeholder="{{$form2D->next_kin_relationship}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Others – Specify</label>
                            <input class="form-control" name="next_kin_other" type="text" placeholder="{{$form2D->next_kin_other}}" disabled>
                        </div>

                    </div><br>

                    <h5>SECTION B</h5><br>

                    <p>TICK	THE	APPROPRIATE	BOX	IN THE FOLLOWING QUESTIONS </p>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>i. Have	you	been	refused	the	right	to	or	restricted	from	carrying	on	any	trade,	business or profession for which a specific license, registration or other authorization is required by law	in any jurisdiction?</p><br>
                            <textarea type="text" name="q1_details" class="form-control" placeholder="{{$form2D->q1_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q1 == 'yes')
                                                selected
                                        @endif
                                        name="q1" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>2. Have	you	been	disciplined,	suspended,	or	sanctioned	under	the	ISA	or	Rules	and	Regulations	of	the	Commission,	or	any	other	regulatory	authority	or	Professional	body	or	government	agency,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q2_details" class="form-control" placeholder="{{$form2D->q2_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q2 == 'yes')
                                                selected
                                        @endif
                                        name="q2" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>3.Have	you	been	subject	of	any	complaint	relating	to	activities	that	are	regulated	by	the	Commission,	or	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q3_details" class="form-control" placeholder="{{$form2D->q3_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q3 == 'yes')
                                                selected
                                        @endif
                                        name="q3" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>4.Have	you	been	subjected	to	any	proceedings	of	a	disciplinary	or	civil/criminal	nature	or	been	notified	of	any	potential	proceedings	or	of	any	investigation	which	might	lead	to	proceedings,	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q4_details" class="form-control" placeholder="{{$form2D->q4_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q4 == 'yes')
                                                selected
                                        @endif
                                        name="q4" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>5.Have	you	been	convicted	of	any	offence,	or	currently	being	subjected	to	any	pending	criminal	proceedings	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q5_details" class="form-control" placeholder="{{$form2D->q5_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q5 == 'yes')
                                                selected
                                        @endif
                                        name="q5" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>6.Have	you	ever	been	convicted	of	any	offence	relating	to	fraud,	misrepresentation	or	dishonesty	in	any	civil	or	criminal	proceeding	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q6_details" class="form-control" placeholder="{{$form2D->q6_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q6 == 'yes')
                                                selected
                                        @endif
                                        name="q6" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>7.State	clearly	if	there	is	any	pending	proceeding	relating	to	fraud,	dishonesty	and	misrepresentation	in	any	court	of	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q7_details" class="form-control" placeholder="{{$form2D->q7_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D-> q7 == 'yes')
                                                selected
                                        @endif
                                        name="q7" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>8.Have	you	had	any	civil/criminal	penalty,	enforcement	action	taken	against	you	by	the	Commission	or	any	other	regulatory	authority	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q8_details" class="form-control" placeholder="{{$form2D->q8_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q8 == 'yes')
                                                selected
                                        @endif
                                        name="q8" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>9.Have	you	had	any	civil/criminal	penalty,	enforcement	action	taken	against	you	by	the	Commission	or	any	other	regulatory	authority	under	any	law	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q9_details" class="form-control" placeholder="{{$form2D->q9_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q9 == 'yes')
                                                selected
                                        @endif
                                        name="q9" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>10. Have	you	been	subject	of	any	investigation	or	disciplinary	proceedings	or	been	issued	a	warning	or	reprimand	by	the	Commission	or	any	other	regulatory	authority,	an	operator	of	a	market	or	clearing	facility,	any	professional	body	or	government	agency,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q10_details" class="form-control" placeholder="{{$form2D->q10_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q10 =='yes')
                                                selected
                                        @endif
                                        name="q10" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>11. Have	you	been	refused	a	fidelity	or	surety	bond,	whether	in	Nigeria	or	elsewhere?	</p><br>
                            <textarea type="text" name="q11_details" class="form-control" placeholder="{{$form2D->q11_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q11 == 'yes')
                                                selected
                                        @endif
                                        name="q11" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>12. Have	you	demonstrated	unwillingness	to	comply	with	any	regulatory	requirement	or	to	uphold	any	professional	and	ethical	standards,	whether	in	Nigeria	or	elsewhere?	</p><br>
                            <textarea type="text" name="q12_details" class="form-control" placeholder="{{$form2D->q12_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q12 == 'yes')
                                                selected
                                        @endif
                                        name="q12" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>13.Have	you	been	untruthful	or	provided	false	or	misleading	information	to	the	Commission,	investors,	and	general	public	or	been	uncooperative	in	any	dealings	with	the	Commission,	investors,	general	public	or	any	other	regulatory	authority	in	any	jurisdiction?</p><br>
                            <textarea type="text" name="q13_details" class="form-control" placeholder="{{$form2D->q13_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q13 == 'yes')
                                                selected
                                        @endif
                                        name="q13" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>14.Have	you	been	a	director,	partner,	shareholder,	staff	or	concerned	in	the	management	of business	that	has	been	censured,	disciplined,	prosecuted	or	convicted	of	a	civil/criminal	offence,	or	been	the	subject	of	any	disciplinary	or	civil/criminal	investigation	or	proceeding,	in	Nigeria	or	elsewhere,	in	relation	to	any	matter	that	took	place	while	you	were	a	director,	partner,	shareholder,	staff	or	concerned	in	the	management	of	the	Business?	</p><br>
                            <textarea type="text" name="q14_details" class="form-control" placeholder="{{$form2D->q14_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q14 == 'yes')
                                                selected
                                        @endif
                                        name="q14" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>15. Have	you	been	a	director,	partner,	shareholder,	staff	or	concerned	in	the	management	of	business	that	has	been	suspended	or	refused	membership	or	registration	by	the	Commission	or	any	other	regulatory	authority,	an	operator	of	a	market	or	clearing	facility,	any	professional	body	or	government	agency?</p><br>
                            <textarea type="text" name="q15_details" class="form-control" placeholder="{{$form2D->q15_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q15 == 'yes')
                                                selected
                                        @endif
                                        name="q15" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>16. Have	you	been	a	director,	partner,	shareholder,	staff	or	concerned	in	the	management	of	a	business	that	has	gone	into	insolvency,	liquidation,	administration	or	receivership,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q16_details" class="form-control" placeholder="{{$form2D->q16_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q16 == 'yes')
                                                selected
                                        @endif
                                        name="q16" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>17.Have	you	been	dismissed	or	asked	to	resign	from	—<br>	(i)	an	office;<br>	(ii)	employment;<br>	(iii)	a	position	of	trust;	or;	<br>(iv)	A	fiduciary	appointment	or	similar	position,	whether	in	Nigeria	or	elsewhere	in	the	last	10	years?</p><br>
                            <textarea type="text" name="q17_details" class="form-control" placeholder="{{$form2D->q17_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q17 == 'yes')
                                                selected
                                        @endif
                                        name="q17" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>18. Have	you	been	subjected	to	disciplinary	proceedings	by	current	or	former	employer(s),	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q18_details" class="form-control" placeholder="{{$form2D->q18_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q18 == 'yes')
                                                selected
                                        @endif
                                        name="q18" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <p>19. Have	you	been	disqualified	from	acting	as	a	director	or	in	any	managerial	capacity,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q19_details" class="form-control" placeholder="{{$form2D->q19_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D->q19 == 'yes')
                                                selected
                                        @endif

                                        name="q19" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>20.Have	you	been	an	officer	found	liable	for	an	offence	committed	by	a	body	corporate	as	a	result	of	the	offence	having	proved	to	have	been	committed	with	the	consent	or	connivance	of,	or	neglect	attributable	to,	you,	whether	in	Nigeria	or	elsewhere?</p><br>
                            <textarea type="text" name="q20_details" class="form-control" placeholder="{{$form2D->q20_details}}" disabled></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2D-> q20 == 'yes')
                                                selected
                                        @endif
                                        name="q20" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <h5 class="text-center">Sworn Declaration<br>(AFFIDAVIT)</h5>
                    <p class="text-center">
                        I……………………{{$form2D->affidavit_name}}…………………………<br>	(Name	in	Full)

                        <br>Of………{{$form2D->affidavit_address}}……<br>	(Address)



                    </p>
                   <br>


                    <p>
                        Hereby make an Oath and state as follows: <br>
                        (1) That I…………{{$form2D->affidavit_full_name}}……………the	applicant<br> (Name	in	Full)<br>
                        herein	seek	registration	with	the	Securities	and	Exchange	Commission,	Nigeria.<br>
                        (2)  That I	have read and understood the questions in this form	as well	as	the	answers	I	have	given	and	that	to	the	best	of	my	knowledge	and	belief	the	statements	made	herein	and	the	attachments	are	true <br>
                        (3) That	I	swear	to	this	affidavit	in	good	faith.<br>
                        <span class="ml-auto">Despondent………………………………………………</span><br>
                        <span class="text-center">
                              SWORN to at………{{$form2D->first_name}}…………court this………{{$form2D->second_input}}…………..day
                              Of…………{{$form2D->third_input}}……………year…………{{$form2D->fourth_input}}……………
                              BEFORE ME
                              ……………{{$form2D->fifth_input}}……………
                              COMMISSIONER OF OATHS
                            </span>

                    </p>

                   <br>
                </form>
            </div>
        </div>
        <!--row closed-->

    </div>
@endsection

