@extends('cmo.layouts.app')

@section('content')
    <style>
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Form Sec 2B</h4>
            <p style="color:white;">Application for transfer/change of status of registered individuals changing employment under the Investment and Securities act 2007</p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->

    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                @foreach($secForm2B as $form2B)
                <form >
                    <h5>Applicant Company</h5>
                    <div class="row">
                        <div class="form-group col">
                            <label>Name of Applicant </label>
                            <input class="form-control" name="applicant_name" type="text" placeholder="{{$form2B->applicant_name}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Applicant was registered as</label>
                            <input class="form-control" name="applicant_reg_as" type="text" placeholder="{{$form2B->applicant_reg_as}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Present Residential Address </label>
                            <input class="form-control" name="res_address" type="text" placeholder="{{$form2B->res_address}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Name of Current Employer</label>
                            <input class="form-control" name="employer_name" type="text" placeholder="{{$form2B->employer_name}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Current Business Address</label>
                            <input class="form-control" name="bus_address" type="text" placeholder="{{$form2B->bus_address}}" disabled>
                        </div>
                    </div><br>
                    <div class="row bottom-space">

                        <div class="form-group col-md-4">
                            <p>Applicant is applying for<br>(please tick whichever is applicable) </p>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Transfer/Change of Status</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2B->applying_for == 'transfer')
                                                selected
                                        @endif
                                        name="applying_for" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>


                    </div>

                    <h6>A.Transfer</h6>
                    <div class="row">
                        <div class="form-group col">
                            <label>Name of Previous Employer</label>
                            <input class="form-control" name="trans_previous_employer" type="text" placeholder="{{$form2B->trans_previous_employer}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Date of Termination / Resignation<br>
                                (Copy of letter should be attached)
                            </label>
                            <input class="form-control" name="date_of_resignation" type="text" placeholder="{{$form2B->date_of_resignation}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload copy of resignation letter</label><br>
                            <input type="file" class="" name="resig_letter" id="customFile" disabled>
                        </div>
                    </div><br>

                    <h6>B.Change of Status</h6>
                    <div class="row">
                        <div class="form-group col">
                            <label>Current Registration</label>
                            <input class="form-control" name="reg_sought" type="text" placeholder="{{$form2B->reg_sought}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>Wish to be registered as<br>
                            </label>
                            <input class="form-control" name="sub_officer_period_service" type="text" placeholder="{{$form2B->sub_officer_period_service}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>APPLICABLE EXAM WRITING</label>
                            <input class="form-control" name="applicable_exam" type="text" placeholder="{{$form2B->applicable_exam}}" disabled>
                        </div>
                        <div class="form-group col">
                            <label>DATE PASSED</label>
                            <input class="form-control" name="date_passed" type="text" placeholder="{{$form2B->date_passed}}" disabled>
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label class="" for="customFile">Please attach proof of passing</label><br>
                            <input type="file" class="" name="proof_passing" id="customFile" disabled>
                        </div>
                    </div><br>

                    <p class="text-center">
                        HAVE THERE BEEN CHANGES IN THE INFORMATION GIVEN IN<br>
                        QUESTIONS 9 THROUGH 16 (See Reverse) OF FORM SEC2 FOR REGISTRATION/APPROVAL<br>
                        PREVIOUSLY FILED BY YOU AND APPROVED BY THE COMMISSION?<br>
                        CIRCLE ONE, YES/NO,  IF YES, ATTACH FULL PARTICULARS.<br>
                        The undersigned hereby certify that the foregoing statements are true<br>
                        and correct to the best of our Knowledge, information and <br>
                        belief We hereby undertake to notify the Securities and Exchange Commission<br>
                        in writing, of any change therein within the period prescribed by the conditions<br>
                        granting Registration.
                    </p><br>
                    <div class="row">


                        <div class="form-group col">
                            <label>Date Signed</label>
                            <input class="form-control" name="date_signed" type="text" placeholder="{{$form2B->date_signed}}" disabled>
                        </div>

                        <div class="form-group col">
                            <label class="" for="customFile">Upload Signature of Applicant</label><br>
                            <input type="file" class="" name="applicant_sig" id="customFile" disabled>
                        </div>



                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name of Director, Partner or Shareholder of the Company</label>
                            <input class="form-control" name="director_name" type="text" placeholder="{{$form2B->director_name}}" disabled>
                        </div>

                        <div class="form-group col">
                            <label class="" for="customFile">Upload Signature of Director, <br>Partner or Shareholder of the Company</label><br>
                            <input type="file" class="" name="dir_sig" id="customFile" disabled>
                        </div>
                    </div><br>




                    <h5 class="text-center">AFFIDAVIT</h5>
                    <p class="text-center">
                        I……………………{{$form2B->affidavit_name}}………………………………Of………………………{{$form2B->affidavit_of}}………………………<br>

                        ……………………………………………………………………………

                    </p>
                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_name" type="text" placeholder="Full Name">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_of" type="text" placeholder="Of">
                        </div>
                    </div><br>


                    <p>
                        Hereby make an Oath and state as follows: <br>
                        (1) That I am the applicant  (or a Director, Officer or Partner of the applicant) Herein for registration and I signed the application.<br>
                        (2) That the statement of fact made in the application are true to the best of my knowledge and belief <br>

                        <span class="ml-auto">Signature of Despondent………………………………………………</span><br>
                        <span class="text-center">
                              SWORN to at………………{{$form2B->first_input}}…………………court this…………{{$form2B->second_input}}…………..day
                              Of………………{{$form2B->third_input}}……………year…………………{{$form2B->fourth_input}}……………………
                              BEFORE ME
                              …………………{{$form2B->fifth_input}}……………………
                              COMMISSIONER OF OATHS
                            </span>

                    </p>

                   <br>

                    <p>NOTE;</p><br>
                    <p>The following areas are addressed in the Form SEC 2 and constitutes question 9 through 16 respectively: </p><br>

                    <div class="row bottom-space">

                        <div class="form-group col-md-8">
                            <p>9. REFUSAL, SUSPENTION, CANCELLATION OR DISCIPLINARY MEASURE(S): </p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2B->refusal == 'yes')
                                                selected
                                        @endif
                                        name="refusal" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-8">
                            <p>10. CHANGE OF NAME</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2B->change_of_name == 'yes')
                                                selected
                                        @endif
                                        name="change_of_name" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>

                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-8">
                            <p>11. OFFENCES UNDER THE LAW: </p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2B->offences == 'yes')
                                                selected
                                        @endif
                                        name="offences" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-8">
                            <p>12: CIVIL PROCEDURES  </p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2B->civil_procedures == 'yes')
                                                selected
                                        @endif
                                        name="civil_procedures" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>


                    </div>
                    <div class="row bottom-space">

                        <div class="form-group col-md-8">
                            <p>13. BANKRUPTCY: </p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2B->baknkruptcy == 'yes')
                                                selected
                                        @endif
                                        name="bankruptcy" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>


                    </div>
                    <div class="row bottom-space">

                        <div class="form-group col-md-8">
                            <p>14. JUDGEMENT OR GARNISHMENT </p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2B->judgement == 'yes')
                                                selected
                                        @endif
                                        name="judgement" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>



                    </div>
                    <div class="row bottom-space">

                        <div class="form-group col-md-8">
                            <p>15  SURETY OR FIDELITY BOND </p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2B->surety == 'yes')
                                                selected
                                        @endif
                                        name="surety" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>


                    </div>
                    <div class="row bottom-space">

                        <div class="form-group col-md-8">
                            <p>16. BUSINESS ACTIVITIES</p><br>

                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        @if($form2B->business_activities == 'yes')
                                                selected
                                        @endif
                                        name="business_activities" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1" disabled>
                            </div>
                        </div>

                    </div>

                </form>
                @endforeach
            </div>
        </div>
        <!--row closed-->

    </div>
@endsection

