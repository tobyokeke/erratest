@extends('cmo.layouts.app')

@section('content')
    <style xmlns="http://www.w3.org/1999/html">
        .visually-hidden {
            position: absolute;
            width: 1px;
            height: 1px;
            left: -10000px;
            overflow: hidden;
        }
        input[type="text"] {
            width: 100%;
            box-sizing: border-box;
            -webkit-box-sizing:border-box;
            -moz-box-sizing: border-box;
        }
    </style>
    <!--page-header open-->
    <div class="page-header">

        <div class="col-md-10">

            <h4 class="page-title">Form Sec 3A</h4>
            <p style="color:white;">Application form for registration as a rating agency under the investments and securities Act 2007.</p>
        </div>

        <div class="col-md-2">
            <p  style="color:white;">Home <span>/ Dashboard</span></p>


        </div>
    </div>
    <!--page-header closed-->

    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card" style="padding:50px; ">
                <form method="post" enctype="multipart/form-data" action="{{route('cmo.applications.post-sec-form-3A')}}">
                    {{ csrf_field() }}
                    <div class="row">
                        <div class="form-group col">
                            <label>1. Registered Name </label>
                            <input class="form-control" name="reg_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>2. Registered Address </label>
                            <input class="form-control" name="reg_address" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>3. Mailing Address(Postal office address) </label>
                            <input class="form-control" name="mail_address" type="text" placeholder="">
                        </div>


                    </div><br>

                    <div class="row">

                        <div class="form-group col">
                            <label>Fax No.</label>
                            <input class="form-control" name="fax_no" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Email</label>
                            <input class="form-control" name="email" type="email" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Website Address</label>
                            <input class="form-control" name="website" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Telephone Number</label>
                            <input class="form-control" name="telephone" type="text" placeholder="">
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>4. State if Private/Public Company/Partnership</label>
                            <input class="form-control" name="state_if" type="text" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>5. Date of Incorporation/Registration </label>
                            <input class="form-control" name="date_incorporation" type="date" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>(a). Objects	(Main  and	Ancillary)</label>
                            <input class="form-control" name="object" type="text" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>(b).(i)	Is	the	Company	engaged	in	other	activities?</label>
                            <div class="form-group col-md-1">
                                <label class="text-center">Yes/No</label>
                                <div class="form-check text-center">
                                    <input
                                            name="b_i" class="form-check-input"
                                            type="checkbox" value="" id="defaultCheck1">
                                </div>
                            </div>
                            <label>(ii)If yes give details</label>
                            <textarea class="form-control" type="text" placeholder=""> </textarea>
                        </div>
                    </div><br>


                    <div class="row">
                        <div class="form-group col">
                            <label>6.Indicate category to which	the	Company	belongs</label>
                            <input class="form-control" name="q6" type="text" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>(a) New Business Undertaking</label>
                            <div class="form-check text-center">
                                <label class="text-center">Yes/No</label>
                                <input
                                        name="q_6a" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                            <label>(b) Company already in the Business of undertaking rating activities</label>

                            <div class="form-check text-center">
                                <label class="text-center">Yes/No</label>
                                <input
                                        name="q_6b" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>


                        </div>
                    </div><br>

                    <h6>7. Promoters</h6><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>(i)Names of promoter </label>
                            <input class="form-control" name="promoter_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address of promoter</label>
                            <input class="form-control" name="promoter_address" type="text" placeholder="">
                        </div>



                    </div><br>

                    <h6>. (ii) Indicting Sharehodling in the company</h6>

                    <p>(a) Nigerian</p><br>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="nig_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="nig_address" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Amount (N)</label>
                            <input class="form-control" name="nig_am" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>% Held</label>
                            <input class="form-control" name="nig_held" type="text" placeholder="">
                        </div>

                    </div><br>

                    <p>(b) Foreign</p><br>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="fore_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="fore_address" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Amount (N)</label>
                            <input class="form-control" name="fore_am" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>% Held</label>
                            <input class="form-control" name="fore_held" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>8. a) Professional/Educational background	of promoters</h6>

                    <div class="row">
                        <div class="form-group col-md-5">
                            <label>Name and address of institution attended </label>
                            <textarea class="form-control" name="pro_name" type="text" placeholder=""></textarea>
                        </div>

                        <div class="form-group col">
                            <label>Date From</label>
                            <input class="form-control" name="pro_date_from" type="date" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>Date To</label>
                            <input class="form-control" name="pro_date_to" type="date" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>Course of study </label>
                            <input class="form-control" name="pro_course" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Qualification Obtained </label>
                            <input class="form-control" name="pro_qua" type="text" placeholder="">
                        </div>


                    </div><br>


                    <h6>b) Financial Soundness/Integrity (Include current bank reference)</h6>

                    <div class="row bottom-space">

                        <div class="form-group col-md-6">
                            <label>(i)Have you	been found guilty of Financial impropriety?	 </label>
                            <textarea type="text" name="q8_b_i_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q8_b_i" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>(ii) Has	your employment ever been terminated? </label>
                            <textarea type="text" name="q8_b_ii_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q8_b_ii" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                    </div>


                    <h6>9. Company's Capital Structure</h6>

                    <div class="row">
                        <div class="form-group col">
                            <label>i. Authorized </label>
                            <input class="form-control" name="authorized" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Shares of N </label>
                            <input class="form-control" name="share_of_n1" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Each amounting to N</label>
                            <input class="form-control" name="each_am1" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>ii. Issued</label>
                            <input class="form-control" name="issued" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Shares of N </label>
                            <input class="form-control" name="share_of_n2" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Each amounting to N  </label>
                            <input class="form-control" name="each_am2" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>iii. Paid up</label>
                            <input class="form-control" name="paid_up" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Shares of N </label>
                            <input class="form-control" name="share_of_n3" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Each amounting to N  </label>
                            <input class="form-control" name="each_am3" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>10. Names and Addresses of Members/Partners holding 5% or<br> more of the capital of the Firm/Company</h6>

                    <p>(a) Nigerian</p><br>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="nig_name2" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="nig_address2" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Amount (N)</label>
                            <input class="form-control" name="nig_am2" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>% Held</label>
                            <input class="form-control" name="nig_held2" type="text" placeholder="">
                        </div>

                    </div><br>

                    <p>(b) Foreign</p><br>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="fore_name2" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="fore_address2" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Amount (N)</label>
                            <input class="form-control" name="fore_am2" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>% Held</label>
                            <input class="form-control" name="fore_held2" type="text" placeholder="">
                        </div>

                    </div><br>


                    <h6> 11. Particulars of Directories/Partners</h6>

                    <div class="row">

                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="part_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="part_address" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Qualification</label>
                            <input class="form-control" name="part_qua" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Experience</label>
                            <input class="form-control" name="part_exp" type="text" placeholder="">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Previous Employment </label>
                            <input class="form-control" name="pre_employment" type="text" placeholder="If any">
                        </div>
                        <div class="form-group col">
                            <label>Date</label>
                            <input class="form-control" name="pre_date" type="date" placeholder="If any">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>Shareholding in company</label>
                            <input class="form-control" name="pre_share_comp" type="text" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>Directorship in any other company</label>
                            <input class="form-control" name="pre_directorship" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>12. Particulars of Principal Officers/Executive and Senior Staff</h6>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name </label>
                            <input class="form-control" name="exec_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Designation</label>
                            <input class="form-control" name="exec_designation" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Qualifications </label>
                            <input class="form-control" name="exec_qua" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Previous Positon Held</label>
                            <input class="form-control" name="exec_position_held" type="text" placeholder="">
                        </div>


                    </div><br>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label>Experience</label>
                            <textarea class="form-control" name="exec_exp" type="text" placeholder=""></textarea>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Date of Appointment </label>
                            <input class="form-control" name="exec_date_ap" type="date" placeholder="">
                        </div>
                        <div class="form-group col-md-2">
                            <label>Functional Area </label>
                            <input class="form-control" name="exec_function_area" type="date" placeholder="">
                        </div>

                    </div><br>

                    <h6>13. Particulars of Rating Committee</h6>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name of members</label>
                            <input class="form-control" name="rating_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Sectoral Focus/Specialization</label>
                            <input class="form-control" name="rating_focus" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Experience in Rating Companies</label>
                            <input class="form-control" name="rating_exp" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Other Related Experiences</label>
                            <input class="form-control" name="rating_related_exp" type="text" placeholder="">
                        </div>


                    </div><br>

                    <h6>14.State qualification of members if not listed	in	12	above
                    <br>Operational	Facilities
                    </h6>

                    <div class="row">
                                <div class="form-group col">
                                    <label>a) Indicate	what	facilities	are	provided</label>
                                    <input class="form-control" name="q14a" type="text" placeholder="">
                                </div>
                                <div class="form-group col">
                                    <label>b) Information and Computer facilities for research and data bank </label>
                                    <input class="form-control" name="q14b" type="text" placeholder="">
                                </div>
                                <div class="form-group col">
                                    <label>c) Indicate	further	plan	for	additional/improved	facilities -</label>
                                    <input class="form-control" name="q14c" type="text" placeholder="">
                                </div>
                                <div class="form-group col">
                                    <label>d) Any other information.</label>
                                    <input class="form-control" name="q14d" type="text" placeholder="">
                                </div>


                            </div><br>


                    <h6>15.Particulars of Associate Companies</h6>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="q15_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address </label>
                            <input class="form-control" name="q15_address" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Nature of Business</label>
                            <input class="form-control" name="q15_nature_of_business" type="text" placeholder="">
                        </div>

                    </div><br>


                    <div class="row">
                        <div class="form-group col">
                            <label>(i)Nature of Interest in the associate/affiliate</label>
                            <input class="form-control" name="q15_i" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>ii) Nature of interest promoter(s)	in the associate</label>
                            <input class="form-control" name="q15_ii" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>iii) Details	of	technical/management agreement subsisting between you and any of the
                                associates/affiliate (if any)</label>
                            <input class="form-control" name="q15_iii" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>State the	basis of fees charge</label>
                            <input class="form-control" name="q15_iv" type="text" placeholder="">
                        </div>

                    </div><br>


                    <h6>16. State the basis of fees	charged</h6>

                    <div class="row">
                        <div class="form-group col">
                            <label>(a)</label>
                            <input class="form-control" name="q16a" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>(b) </label>
                            <input class="form-control" name="q16b" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>(c)</label>
                            <input class="form-control" name="q16c" type="text" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>(d)</label>
                            <input class="form-control" name="q16d" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>17. List Rating Methods to be used</h6>

                    <div class="row">
                        <div class="form-group col-md-5">
                            <label>(a)</label>
                            <input class="form-control" name="q17a" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-6">
                            <label>(b) </label>
                            <input class="form-control" name="q17b" type="text" placeholder="">
                        </div>
                        <div class="form-group col-md-5">
                            <label>(c)</label>
                            <input class="form-control" name="q17c" type="text" placeholder="">
                        </div>

                        <div class="form-group col-md-5">
                            <label>(d)</label>
                            <input class="form-control" name="q17d" type="text" placeholder="">
                        </div>

                    </div><br>


                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>18.Has the applicant or	any	of	its	associates/affiliates been denied registration or expelled	from membership
                                of any government agency, securities exchange or National Association	of Securities Dealers?</label>
                            <textarea type="text" name="q18_details" class="form-control" placeholder="If yes, give reasons"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q18" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>19. Has	your	membership	or	that	of	any	of	your	associate/affiliates	in	any	of	the	institutions	mentioned	in
                                (18)	at	any	time being	revoked,	suspended	or	cancelled?</label>
                            <textarea type="text" name="q19_details" class="form-control" placeholder="If yes, give reasons"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q19" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>20.Has	the	applicant	or	any	of	its affiliates	to	the	best	of	the	applicant`s	knowledge	and	belief	operated
                                under	or	carried	on	business	under	any	name	other	than	the	name	shown	in	this	application?</label>
                            <textarea type="text" name="q20_details" class="form-control" placeholder="If yes, give reasons"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q20" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>


                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>21. Is any depository holding any of	the	assets	of	the	company?</label>
                            <textarea type="text" name="21_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q21" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>22. Has	any	person	or	company	undertaken	to	act	as	a	guarantor	in	relation	to	the	financial	or	other
                                undertakings of	the	applicant</label>
                            <textarea type="text" name="q22_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q22" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>23. Has	a	subordinated	agreement	been	executed	by	the	creditor	(s)	in	relation	to	loans	owing	by	the
                                applicant?</label>
                            <textarea type="text" name="q23_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q23" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>24. Has	your	company	or	any	of	its	associates/affiliate	been	subject	of	any	winding	up	order	or	receivership
                                arrangement? </label>
                            <textarea type="text" name="q24_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q24" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>25. Has	the	applicant	any	relationship	with	or	interest	in	any	of	the	companies	it	proposes	to	rate	the
                                securities?	 </label>
                            <textarea type="text" name="q25_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q25" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                    </div>

                    <h6>26. Has	your	company	or	any	of	its	associates/affiliates	been involved	in	any	litigations	within	10	years
                        preceding	this	application	in	respect</h6>

                    <div class="row">
                        <div class="form-group col">
                            <label>i) Filing of	any	application	for	registration?</label>
                            <input class="form-control" name="q26_i" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>ii) Purchase	or	sales of securities? </label>
                            <input class="form-control" name="q26_ii" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>iii) Any	offence	relating	to	the	business	of	a	rating	agency	or	other	investments	services	business?</label>
                            <input class="form-control" name="q26_iii" type="text" placeholder="">
                        </div>



                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label>iv) Any criminal offence?</label>
                            <input class="form-control" name="q26_iv" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>v) Any civil	offence?</label>
                            <input class="form-control" name="q26_v" type="text" placeholder="">
                        </div>

                    </div><br>


                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>vi) Where	the	answer	to	any	of	the	items	in	26	(i)	– (v)</label>
                            <textarea type="text" name="q26_vi_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q26_vi" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <label>27. Has	your Firm or any of	its	associates/affiliates	been	refused	any	fidelity	bond?</label>
                            <textarea type="text" name="q27_details" class="form-control" placeholder="If yes, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q27" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                    </div>



                    <h6>28.(i)	state	nominal	and	market	values	of	your	investments in	securities	as	at	the	date	of	this	application</h6>
                    <table border="1" style="table-layout: fixed; width: 100%;">

                        <tr>
                            <th scope="col" class="text-center">Security</th>
                            <th scope="col" class="text-center">Nominal Value (N)</th>
                            <th scope="col" class="text-center">Market Value (N)</th>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Ordinary Shares</th>
                            <td><input name="ord_1" type="text"></td>
                            <td><input name="ord_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Preferences Shares</th>
                            <td><input name="pref_1" type="text"></td>
                            <td><input name="pref_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Debentures</th>
                            <td><input name="deb_1" type="text"></td>
                            <td><input name="deb_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Government Bonds</th>
                            <td><input name="gov_1" type="text"></td>
                            <td><input name="gov_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Other Fixed Interest Bearing Securities</th>
                            <td><input name="oth_1" type="text"></td>
                            <td><input name="oth_2" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Mutual Funds (Open-ended)</th>
                            <td><input name="mut_1" type="text"></td>
                            <td><input name="mut_2" type="text"></td>

                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Investment Trust (close-end)</th>
                            <td><input name="open_1" type="text"></td>
                            <td><input name="open_2" type="text"></td>

                        </tr>



                    </table><br>

                    <p>(ii) List Names of companies</p>

                    <div class="row">
                        <div class="form-group col">
                            <label>First Company </label>
                            <input class="form-control" name="first_company" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Second Company</label>
                            <input class="form-control" name="second_company" type="text" placeholder="">
                        </div>

                        <div class="form-group col">
                            <label>Third Company</label>
                            <input class="form-control" name="third_company" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>(iii).State the build-up of the following items over the last five years or since incorporation if less than five years</h6>
                    <table border="1" style="table-layout: fixed; width: 100%;">

                        <tr>
                            <th scope="col" class="text-center">Debtors</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>
                            <th scope="col" class="text-center">Year (N)</th>



                        </tr>

                        <tr>
                            <th scope="row" class="text-center"><strong>Debtors</strong></th>
                            <td><input name="debt_1" type="text"></td>
                            <td><input name="debt_2" type="text"></td>
                            <td><input name="debt_3" type="text"></td>
                            <td><input name="debt_4" type="text"></td>
                            <td><input name="debt_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Short Term Loans (Assets)</th>
                            <td><input name="sho_1" type="text"></td>
                            <td><input name="sho_2" type="text"></td>
                            <td><input name="sho_3" type="text"></td>
                            <td><input name="sho_4" type="text"></td>
                            <td><input name="sho_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Investments (At year and market)</th>
                            <td><input name="inv_1" type="text"></td>
                            <td><input name="inv_2" type="text"></td>
                            <td><input name="inv_3" type="text"></td>
                            <td><input name="inv_4" type="text"></td>
                            <td><input name="inv_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Quoted</th>
                            <td><input name="quo_1" type="text"></td>
                            <td><input name="quo_2" type="text"></td>
                            <td><input name="quo_3" type="text"></td>
                            <td><input name="quo_4" type="text"></td>
                            <td><input name="quo_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Unquoted</th>
                            <td><input name="unquo_1" type="text"></td>
                            <td><input name="unquo_2" type="text"></td>
                            <td><input name="unquo_3" type="text"></td>
                            <td><input name="unquo_4" type="text"></td>
                            <td><input name="unquo_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Other Short Term Assets</th>
                            <td><input name="oth_sho_1" type="text"></td>
                            <td><input name="oth_sho_2" type="text"></td>
                            <td><input name="oth_sho_3" type="text"></td>
                            <td><input name="oth_sho_4" type="text"></td>
                            <td><input name="oth_sho_5" type="text"></td>


                        </tr>



                        <tr>
                            <th scope="row" class="text-center"><strong>Creditors</strong></th>
                            <td><input name="cred_1" type="text"></td>
                            <td><input name="cred_2" type="text"></td>
                            <td><input name="cred_3" type="text"></td>
                            <td><input name="cred_4" type="text"></td>
                            <td><input name="cred_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Short Term Loans (Liabilities)</th>
                            <td><input name="lia_1" type="text"></td>
                            <td><input name="lia_2" type="text"></td>
                            <td><input name="lia_3" type="text"></td>
                            <td><input name="lia_4" type="text"></td>
                            <td><input name="lia_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Other Short term Liabilities</th>
                            <td><input name="oth_lia_1" type="text"></td>
                            <td><input name="oth_lia_2" type="text"></td>
                            <td><input name="oth_lia_3" type="text"></td>
                            <td><input name="oth_lia_4" type="text"></td>
                            <td><input name="oth_lia_5" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Client Deposits</th>
                            <td><input name="dep_1" type="text"></td>
                            <td><input name="dep_2" type="text"></td>
                            <td><input name="dep_3" type="text"></td>
                            <td><input name="dep_4" type="text"></td>
                            <td><input name="dep_5" type="text"></td>


                        </tr>


                    </table><br>


                    <h6>29.(I)	state	the	name(s)	and	address(es)	of	auditors(s) of	the	applicant</h6><br>

                    <div class="row">
                        <div class="col">
                            <label>Name</label>
                            <input name="q29_i_name" class="form-control" type="text">
                        </div>
                        <div class="col">
                            <label>Address</label>
                            <input name="q29_i_address" class="form-control" type="text">
                        </div>

                    </div><br>


                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>(ii)	Has	there	been	any	change	of	auditors	over	the	past	five	years?</p><br>
                            <textarea type="text" name="q29_ii_details" class="form-control" placeholder="If yes, why?"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q29_ii" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>
                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>(iii)is	the	auditing firm also responsible or the auditing of any of your associates/affiliates?</p><br>
                            <textarea type="text" name="q29_iii_details" class="form-control" placeholder="If yes, give name(s)"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q29_iii" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row">
                        <div class="col">
                            <label>(a)</label>
                            <input class="form-control" name="q29_a" type="text">
                        </div>
                        <div class="col">
                            <label>(b)</label>
                            <input class="form-control" name="q29_b" type="text">
                        </div>

                        <div class="col">
                            <label>(c)</label>
                            <input class="form-control" name="q29_c" type="text">
                        </div>

                    </div><br>

                    <div class="row">
                        <div class="col">
                            <label>(iv)	State the basis of remuneration for the services rendered</label>
                            <input class="form-control" name="q29_iv" type="text">
                        </div>
                    </div><br>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>(v)	Has	there	been	any	adverse	comment	by	the	auditors	on	the	value/mix	of	the	securities	in	your	investment
                                portfolio	over	the	past	five	(years)?	</p><br>
                            <textarea type="text" name="q29_v_details" class="form-control" placeholder="If so, why"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q29_v" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <div class="row bottom-space">

                        <div class="form-group col-md-10">
                            <p>(vi)	is	the	auditing	firm	or	any	of	its	partners	or	principal	officers	holding	any	substantial	securities	in	the
                                companies	within	your	investment	portfolio	or	is	any	of	them	holding	a	directorate	seat	in	the	company?</p><br>
                            <textarea type="text" name="q29_vi_details" class="form-control" placeholder="If so, give details"></textarea>
                        </div>
                        <div class="form-group col-md-1">
                            <label class="text-center">Yes/No</label>
                            <div class="form-check text-center">
                                <input
                                        name="q29_vi" class="form-check-input"
                                        type="checkbox" value="" id="defaultCheck1">
                            </div>
                        </div>

                    </div>

                    <h6>30. Name and address of the	bankers	of the applicant company</h6>

                    <div class="row">
                        <div class="form-group col">
                            <label>Name</label>
                            <input class="form-control" name="q30_name" type="text" placeholder="">
                        </div>
                        <div class="form-group col">
                            <label>Address</label>
                            <input class="form-control" name="q30_address" type="text" placeholder="">
                        </div>

                    </div><br>

                    <h6>31. Financial Information</h6><br>

                    <p>(i) Net worth</p><br>

                    <table border="1" style="table-layout: fixed; width: 100%;">

                        <tr>
                            <th scope="col" class="text-center">S/No</th>
                            <th scope="col" class="text-center">Items</th>
                            <th scope="col" class="text-center"></th>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">a.</th>
                            <th scope="row" class="text-center">Paid-up	Capital</th>
                            <td><input name="paid_up" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">b.</th>
                            <th scope="row" class="text-center">Reserves (Excluding	revaluation	surplus)</th>
                            <td><input name="res_1" type="text"></td>
                            <td><input name="res_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">Total</th>
                            <th scope="row" class="text-center">(a) +(b)</th>
                            <td><input name="total_1" type="text"></td>
                            <td><input name="total_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">c.</th>
                            <th scope="row" class="text-center">Accumulated	Losses</th>
                            <td><input name="acc_1" type="text"></td>
                            <td><input name="acc_2" type="text"></td>


                        </tr>
                        <tr>
                            <th scope="row" class="text-center">d.</th>
                            <th scope="row" class="text-center">Differed revenue/expenditure not written off</th>
                            <td><input name="dif_1" type="text"></td>
                            <td><input name="dif_2" type="text"></td>


                        </tr>

                        <tr>
                            <th scope="row" class="text-center">Net worth</th>
                            <th scope="row" class="text-center">(a) +	(b)	– (c)	– (d)</th>
                            <td><input name="net_1" type="text"></td>
                            <td><input name="net_2" type="text"></td>

                        </tr>

                    </table><br>

                    <h6>(ii) Please	enclose	audited	annual account	for	the	last three	years.	Where un audited reports are
                        submitted give reasons.</h6>
                    <div class="row">
                        <div class="col">
                            <label>Give reasons</label>
                            <input class="form-control" type="text">
                        </div>
                    </div>


                    <h5 class="text-center">AFFIDAVIT</h5>
                    <p class="text-center">
                        I………………………………………………………………………………………………<br>	Name	in	Full

                        <br>Residing at……………………………………………………………<br>



                    </p>
                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_name" type="text" placeholder="Full Name">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="affidavit_address" type="text" placeholder="Address">
                        </div>

                    </div><br>


                    <p>
                        Hereby make an Oath and state as follows: <br>
                        (i) That I am Director/Partner/officer of the applicant company/firm<br>
                        (ii) That I am duly authorized by the company/firm to sign this application;<br>
                        (iii) That	the	information	supplied	in	this	application	and	exhibits	attached	there	to	are	to	the	best	of
                        my	knowledge	and	belief	true;<br>
                        (iv)that I	swear	to	this	affidavit	in	good	faith.<br>
                        <span class="ml-auto">Despondent<br>………………………………………………</span><br>
                        <span class="text-center">
                              SWORN at the………………………………………………………court this……………………………day
                              Of………………………………………………year………………………………………………………………<br>
                              BEFORE ME<br>
                              ………………………………………………………………<br>
                              COMMISSIONER OF OATHS<br>
                            </span>

                    </p>

                    <div class="row">
                        <div class="form-group col">
                            <input class="form-control" name="first_input" type="text" placeholder="Sworn at the">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="second_input" type="text" placeholder="court this">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="third_input" type="text" placeholder="day of">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="fourth_input" type="text" placeholder="year">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="fifth_input" type="text" placeholder="Before me">
                        </div>
                        <div class="form-group col">
                            <input class="form-control" name="commish" type="text" placeholder="Commissioner of oaths">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload Despondent Signature</label><br>
                            <input type="file" name="despondent_sig" class="" id="customFile">
                        </div>
                    </div><br>

                   <h6>Each	application	filed	must	be	accompanied	by</h6>

                    <div class="row">

                        <div class="form-group col">
                            <label class="" for="customFile">Upload a copy of the	certificate of	incorporation certified by the	Corporate Affairs Commission.</label><br>
                            <input type="file" name="cac" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload a copy of the	Memorandum	and	Articles	of	Association	certified<br>	by	the	Corporate	Affairs	Commission	and
                                which	shall	among	others,	include	the	power	to	act	as	a	rating	agency.</label><br>
                            <input type="file" name="memo" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload 	Profile	of	the	company	covering activities,	organizational	structure<br>	and	detailed	future expansion	programme</label><br>
                            <input type="file" name="pro" class="" id="customFile">
                        </div>
                    </div><br>

                    <div class="row">
                        <div class="form-group col">
                            <label class="">Write an undertaking to	inform	the	Commission	of	any	material change	contained	in	the	application</label><br>
                            <input type="text" name="under" class="form-control">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload Existing or	proposed bye-laws	or	rules,	guidelines	and	code of	conduct</label><br>
                            <input type="file" name="bye_laws" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload Sworn	undertaking	to	keep	such	records	and	render	such	returns	as	may	be	specified	by	the	Commission	from
                                time	to	time</label><br>
                            <input type="file" name="sworn" class="" id="customFile">
                        </div>
                        <div class="form-group col">
                            <label class="" for="customFile">Upload a chartered	accountants	certificate	certifying	the	continuous net	worth	of	not	less than	the	minimum
                                paid-up	capital	of	the	company</label><br>
                            <input type="file" name="chart" class="" id="customFile">
                        </div>
                    </div>

                    <div class="row text-center">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                </form>

            </div>
        </div>
        <!--row closed-->

    </div>


@endsection


