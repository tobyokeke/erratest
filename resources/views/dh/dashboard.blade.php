@extends('dh.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Dashboard</h4>
        <ol class="breadcrumb">
            {{--<li class="breadcrumb-item"><a href="#" class="text-light-color">Home</a></li>--}}
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">
        <div class="col-lg-6 col-xl-6 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Applications
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">{{$allApplications}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <p class="title">Today</p>
                            <p class="data">{{$applicationsToday}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Week</p>
                            <p class="data">{{$applicationsThisWeek}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Month</p>
                            <p class="data">{{$applicationsThisMonth}}</p>
                        </div>

                    </div>
                </div>
                {{--<div class="card-footer">--}}
                {{--<div class="float-left">--}}
                {{--<p class="mb-0">--}}
                {{--<span class="">--}}
                {{--2.5%--}}
                {{--</span>--}}
                {{--last month--}}
                {{--</p>--}}
                {{--</div>--}}
                {{--<div class="float-right">--}}
                {{--<i class="fa fa-arrow-circle-o-up ml-1 text-success"></i>--}}
                {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col-lg-6 col-xl-6 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Unassigned Applications
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">{{$allUnassignedApplications}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <p class="title">Today</p>
                            <p class="data">{{$unassignedApplicationsToday}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Week</p>
                            <p class="data">{{$unassignedApplicationsThisWeek}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Month</p>
                            <p class="data">{{$unassignedApplicationsThisMonth}}</p>
                        </div>

                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-xl-12 col-md-12 col-12">
            <div class="card dashboard-header">
                <div class="card-footer">
                    <div class="float-left">
                        <p class="mb-0">
                            <b>View application updates</b>
                        </p>
                    </div>
                    <div class="float-right">
                        <a href="" class="btn btn-primary">Click Here</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row closed-->




    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Recent Applications</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless text-nowrap mb-0">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>ID</th>
                                <th>Function(s)</th>
                                <th>Assigned To</th>
                                <th>Started</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(count($applications) <= 0)
                                <tr>
                                    <td colspan="4" align="center">No Applications yet.</td>
                                </tr>
                            @endif
                            <?php $count = 1; ?>
                            @foreach($applications as $application)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$application->aid}}</td>
                                    <td>
                                        @foreach($application->Functions as $function)
                                            {{$function->name}},
                                        @endforeach
                                    </td>
                                    <td>
                                        @if(isset($application->AssignedTo))
                                            {{$application->AssignedTo->name}}
                                            @else
                                            Not Assigned Yet
                                        @endif
                                    </td>
                                    <td>{{$application->created_at->diffForHumans()}}</td>
                                    <td>
                                        <a href="{{route('dh.application.details',['application' => $application->aid])}}">
                                            <span class="badge badge-pill badge-primary">View</span>
                                        </a>

                                    </td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row closed-->


@endsection
