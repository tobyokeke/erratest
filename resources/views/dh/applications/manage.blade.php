@extends('dh.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Applications</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Applications</a></li>
        </ol>
    </div>
    <!--page-header closed-->



    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Applications</h4>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless text-nowrap mb-0">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>ID</th>
                                <th>Function(s)</th>
                                <th>Assigned To</th>
                                <th>Started</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>

                            @if(count($applications) <= 0)
                                <tr>
                                    <td colspan="4" align="center">No Applications yet.</td>
                                </tr>
                            @endif
                            <?php $count = 1; ?>
                            @foreach($applications as $application)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$application->aid}}</td>
                                    <td>
                                        @foreach($application->Functions as $function)
                                            {{$function->name}},
                                        @endforeach
                                    </td>
                                    <td>
                                        @if(isset($application->AssignedTo))
                                            {{$application->AssignedTo->name}}
                                        @else
                                            Not Assigned Yet
                                        @endif
                                    </td>
                                    <td>{{$application->created_at->diffForHumans()}}</td>
                                    <td>
                                        <a href="{{route('dh.application.details',['application' => $application->aid])}}">
                                            <span class="badge badge-pill badge-primary">View</span>
                                        </a>

                                    </td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>


                    {{$applications->links()}}
                </div>
            </div>
        </div>
    </div>
    </div>
    <!--row closed-->


@endsection
