@extends('cmo.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard.css')}}">
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard_theme_dots.css')}}">
    <style>

        .padit{
            padding:7px 15px;
        }


    </style>
@endsection

@section('scripts')
    <!--Formvalidation js-->
    <script src="{{url('admin/assets/js/formvalidation.js')}}"></script>

@endsection

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Profile</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Profile</a></li>
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">

    <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">

        <div class="card">
            <div class="card-header">
                {{--<h4>General Elements</h4>--}}

                @if($user->status == 'pending')
                    <span style="float: right" class="badge badge-warning">{{$user->status}}</span>
                @endif

                @if($user->status == 'invalid' || $user->status == 'suspended')
                    <span style="float: right" class="badge badge-danger">{{$user->status}}</span>
                @endif

                @if($user->status == 'valid')
                    <span style="float: right" class="badge badge-success">{{$user->status}}</span>
                @endif

            </div>
            <div class="card-body">
                <form class="form-horizontal" method="post" action="{{route('profile.edit.post')}}" >

                    @csrf
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Firstname</label>
                        <div class="col-md-9 padit">
                            {{$user->firstName}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Lastname</label>
                        <div class="col-md-9 padit">
                            {{$user->lastName}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">BVN</label>
                        <div class="col-md-9 padit">
                            {{$user->bvn}}
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Middle Name</label>
                        <div class="col-md-9 padit">
                            {{$user->middleName}}
                        </div>
                    </div>
                    {{--<div class="form-group row">--}}
                        {{--<label class="col-md-3 col-form-label">Function</label>--}}
                        {{--<div class="col-md-9">--}}
                            {{--<input type="text" class="form-control" name="function" value="">--}}
                        {{--</div>--}}
                    {{--</div>--}}
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Job Responsibility</label>
                        <div class="col-md-9">
                            <div class="form-group">
                                    <select name="jobResponsibility" class="form-control select2 w-100" >
                                        <option value="complianceOfficer"
                                                @if($user->jobResponsibility == 'complianceOfficer') selected @endif
                                        >Compliance Officer</option>
                                        <option value="executiveDirector"
                                                @if($user->jobResponsibility == 'executiveDirector') selected @endif
                                        >Executive Director</option>
                                        <option value="independentDirector"
                                                @if($user->jobResponsibility == 'independentDirector') selected @endif
                                        >Independent Director</option>
                                        <option value="managingDirector"
                                                @if($user->jobResponsibility == 'managingDirector') selected @endif
                                        >Managing Director</option>
                                        <option value="nonExecutiveDirector"
                                                @if($user->jobResponsibility == 'nonExecutiveDirector') selected @endif
                                        >Non-Executive Director</option>
                                        <option value="branchOfficeHead"
                                                @if($user->jobResponsibility == 'branchOfficeHead') selected @endif
                                        >Branch Office Head</option>
                                        <option value="sponsoredIndividual"
                                                @if($user->jobResponsibility == 'sponsoredIndividual') selected @endif
                                        >Sponsored Individual</option>

                                    </select>

                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">State</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="state" value="{{$user->state}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Country</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="country" value="{{$user->country}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Phone</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control" name="phone" value="{{$user->phone}}">
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Address Line 1</label>
                        <div class="col-md-9">
                            <textarea class="form-control" rows="6" name="addressLine1">{{$user->addressLine1}}</textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label class="col-md-3 col-form-label">Address Line 2</label>
                        <div class="col-md-9">
                            <textarea class="form-control" rows="6" name="addressLine2">{{$user->addressLine2}}</textarea>
                        </div>
                    </div>


                    <button type="submit" class="btn btn-success">Submit</button>
                </form>
            </div>
        </div>


        <div class="card">
            <div class="card-header">
                COMPANY AFFILIATION

                    <a href="{{route('cmo.companies.add')}}" style="float: right" class="badge badge-success">REGISTER COMPANY</a>

            </div>

            <div class="card-body">

                <table class="table table-striped">
                    <tr>
                        <th>COMPANY NAME</th>
                        <th>STATUS</th>
                        <th>START DATE</th>
                        <th>END DATE</th>
                    </tr>


                    @foreach(auth()->user()->Companies as $company)
                        <tr>
                            <td>{{$company->name}}</td>
                            <td>{{$company->status}}</td>
                            <td>
                                @if(!empty($company->startDate))
                                {{$company->startDate->toDayDateTimeString()}}
                                @endif
                            </td>
                            <td>
                                @if(!empty($company->startDate))
                                {{$company->endDate->toDayDateTimeString()}}
                                @endif
                            </td>
                        </tr>
                    @endforeach

                </table>
            </div>
        </div>
    </div>

    </div>

@endsection
