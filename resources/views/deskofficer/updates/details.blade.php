@extends('dh.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Updates for Application #{{$application->aid}}</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Updates</a></li>
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">
        <div class="col-md-12">
            <ul class="cbp_tmtimeline">


                @if(count($updates) <= 0)
                    <li style="color:white;">No Updates Yet</li>
                @endif
                @foreach($updates as $update)
                <li>
                    <time class="cbp_tmtime text-white" datetime="{{$update->created_at}}"> <span>{{$update->created_at->diffForHumans()}}</span></time>
                    <div class="cbp_tmicon bg-primary"><i class="zmdi zmdi-label"></i></div>
                    <div class="cbp_tmlabel">
                        <h2><a href="javascript:void(0);">{{$update->User->name}}</a> <span class="text-muted">posted and update on {{$update->created_at->toDayDateTimeString()}} </span></h2>
                        <p class="text-sm">

                            {{$update->update}}
                        </p>
                    </div>
                </li>
                @endforeach



            </ul>


            <!-- update insertion form -->
            <div class="col-9 offset-3">
                <form method="post" action="{{route('deskOfficer.application.updates.post',['aid' => $application->aid])}}">
                    @csrf
                    <div class="form-group col-12 ">
                            <textarea rows="6" class="form-control input-lg" maxlength="2000" name="update" placeholder="Enter your update"></textarea>

                            <button type="submit" style="margin-top:10px;" class="btn btn-success">SUBMIT</button>
                    </div>


                </form>
            </div>
            <!-- end update insertion form -->



        </div>
    </div>

    <div class="row">


    </div>




@endsection
