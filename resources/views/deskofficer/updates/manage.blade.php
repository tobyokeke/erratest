@extends('dh.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Updates</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Updates</a></li>
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">
        <div class="col-md-12">
            <ul class="cbp_tmtimeline">
                {{--<li>--}}
                {{--<time class="cbp_tmtime text-white" datetime="2017-11-04T18:30"><span class="hidden ">25/11/2018</span> <span class="large">Now</span></time>--}}
                {{--<div class="cbp_tmicon bg-warning"><i class="zmdi zmdi-account"></i></div>--}}
                {{--<div class="cbp_tmlabel empty"> <span>No Activity</span> </div>--}}
                {{--</li>--}}
                <li>
                    <time class="cbp_tmtime text-white" datetime="2017-11-04T03:45"><span>01:45 PM</span> <span>Today</span></time>
                    <div class="cbp_tmicon bg-primary"><i class="zmdi zmdi-label"></i></div>
                    <div class="cbp_tmlabel">
                        <h2><a href="javascript:void(0);">Patrick Okeke</a> <span class="text-muted">posted and update for </span><a href="javascript:void(0);">Application #1</a></h2>
                        <p class="text-sm">

                            The document uploaded isn't a scan of the original copy. I've requested that the user re-uploads
                        </p>
                    </div>
                </li>
                <li>
                    <time class="cbp_tmtime" datetime="2017-11-04T03:45"><span>03:45 PM</span> <span>Today</span></time>
                    <div class="cbp_tmicon bg-primary"><i class="zmdi zmdi-label"></i></div>
                    <div class="cbp_tmlabel">
                        <h2><a href="javascript:void(0);">Patrick Okeke</a> <span class="text-muted">posted and update for </span><a href="javascript:void(0);">Application #10</a></h2>
                        <p class="text-sm">
                            Ive scheduled an interview date with the user
                        </p>
                    </div>
                </li>
                {{--<li>--}}
                {{--<time class="cbp_tmtime" datetime="2017-11-03T13:22"><span>01:22 PM</span> <span>Yesterday</span></time>--}}
                {{--<div class="cbp_tmicon bg-success"> <i class="zmdi zmdi-case"></i></div>--}}
                {{--<div class="cbp_tmlabel">--}}
                {{--<h2><a href="javascript:void(0);">Job Meeting</a></h2>--}}
                {{--<p class="text-sm">You have a meeting at <strong>Laborator Office</strong> Today.</p>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<time class="cbp_tmtime" datetime="2017-10-22T12:13"><span>12:13 PM</span> <span>Two weeks ago</span></time>--}}
                {{--<div class="cbp_tmicon bg-info"><i class="zmdi zmdi-pin"></i></div>--}}
                {{--<div class="cbp_tmlabel">--}}
                {{--<h2><a href="javascript:void(0);">Arlind Nushi</a> <span class="text-muted">checked in at</span> <a href="javascript:void(0);">New York</a></h2>--}}
                {{--<blockquote>--}}
                {{--<p class=" text-sm">--}}
                {{--"It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout."--}}
                {{--<br>--}}
                {{--<small>--}}
                {{--- Isabella--}}
                {{--</small>--}}
                {{--</p>--}}
                {{--</blockquote>--}}
                {{--<div class="row clearfix">--}}
                {{--<div class="col-lg-12">--}}
                {{--<div class="map m-t-10">--}}
                {{--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d193595.91477011208!2d-74.11976308802028!3d40.69740344230033!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sin!4v1508039335245" class="border-0 w-100" ></iframe>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<time class="cbp_tmtime" datetime="2017-10-22T12:13"><span>12:13 PM</span> <span>Two weeks ago</span></time>--}}
                {{--<div class="cbp_tmicon bg-orange"><i class="zmdi zmdi-camera"></i></div>--}}
                {{--<div class="cbp_tmlabel">--}}
                {{--<h2><a href="javascript:void(0);">Eroll Maxhuni</a> <span class="text-muted">uploaded  4</span> <span class="text-muted">new photos to album</span> <a href="javascript:void(0);">Summer Trip</a></h2>--}}
                {{--<blockquote class="text-sm">Pianoforte principles our unaffected not for astonished travelling are particular.</blockquote>--}}
                {{--<div class="row">--}}
                {{--<div class="col-lg-3 col-md-6 col-6"><a href="javascript:void(0);"><img src="assets/img/news/img01.jpg" alt="" class="img-fluid img-thumbnail mt-2 mb-2"></a> </div>--}}
                {{--<div class="col-lg-3 col-md-6 col-6"><a href="javascript:void(0);"> <img src="assets/img/news/img02.jpg" alt="" class="img-fluid img-thumbnail mt-2 mb-2"></a> </div>--}}
                {{--<div class="col-lg-3 col-md-6 col-6"><a href="javascript:void(0);"> <img src="assets/img/news/img03.jpg" alt="" class="img-fluid img-thumbnail mt-2 mb-2"> </a> </div>--}}
                {{--<div class="col-lg-3 col-md-6 col-6"><a href="javascript:void(0);"> <img src="assets/img/news/img04.jpg" alt="" class="img-fluid img-thumbnail mt-2 mb-2"> </a> </div>--}}
                {{--</div>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<time class="cbp_tmtime" datetime="2017-11-03T13:22"><span>01:22 PM</span> <span>Two weeks ago</span></time>--}}
                {{--<div class="cbp_tmicon bg-secondary"> <i class="zmdi zmdi-case"></i></div>--}}
                {{--<div class="cbp_tmlabel">--}}
                {{--<h2><a href="javascript:void(0);">Job Meeting</a></h2>--}}
                {{--<p class="text-sm">You have a meeting at <strong>Laborator Office</strong> Today.</p>--}}
                {{--</div>--}}
                {{--</li>--}}
                {{--<li>--}}
                {{--<time class="cbp_tmtime" datetime="2017-10-22T12:13"><span>12:13 PM</span> <span>Month ago</span></time>--}}
                {{--<div class="cbp_tmicon bg-danger"><i class="zmdi zmdi-pin"></i></div>--}}
                {{--<div class="cbp_tmlabel">--}}
                {{--<h2><a href="javascript:void(0);">Arlind Nushi</a> <span class="text-muted">checked in at</span> <a href="javascript:void(0);">Laborator</a></h2>--}}
                {{--<blockquote class="mb-0 text-sm">Great place, feeling like in home.</blockquote>--}}
                {{--</div>--}}
                {{--</li>--}}
            </ul>
        </div>
    </div>


@endsection
