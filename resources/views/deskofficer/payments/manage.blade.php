@extends('deskofficer.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Payments</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Payments</a></li>
        </ol>
    </div>
    <!--page-header closed-->



    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Recent Payments</h4>

                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless text-nowrap mb-0">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Amount</th>
                                <th>Application ID</th>
                                <th>Paid By</th>
                                <th>Date</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$payment->amount}}</td>
                                    <td>
                                        <a href="{{route('deskOfficer.application.details',['application' => $payment->aid])}}">
                                            {{$payment->aid}}
                                        </a>
                                    </td>
                                    <td>{{$payment->User->name}}</td>
                                    <td>{{$payment->created_at->toDayDateTimeString()}}</td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach

                            </tbody>
                        </table>

                        {{$payments->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row closed-->


@endsection
