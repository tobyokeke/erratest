<div class="header">
    <!--nav open-->
    <nav class="navbar navbar-expand-lg main-navbar">
        <div class="container">
            <a id="horizontal-navtoggle" class="d-md-none d-lg-none d-xl-none animated-arrow hor-toggle">
                <span></span>
            </a>
            <a class="header-brand" href="">
                <img src="{{url('admin/assets/img/logo.png')}}" class="header-brand-img" alt="ERRA logo">
            </a>


            <ul class="navbar-nav navbar-right">


                <li class="dropdown">
                    <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg d-flex">
										<span class="mr-3 mt-2 d-none d-lg-block ">
											<span class="text-white">Hello,<span
                                                        class="ml-1"> {{auth()->user()->name}}</span></span>
										</span>

                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <div class=" dropdown-header noti-title text-center border-bottom pb-3">
                            <h5 class="text-capitalize text-dark mb-1">{{auth()->user()->name}}</h5>
                            <h5 class="text-capitalize text-dark subMenuMessage">{{auth()->user()->role}}</h5>

                        </div>
                        <a class="dropdown-item" href="{{route('passwords.change')}}">
                            <i class="mdi mdi-account-outline mr-2"></i>
                            <span>Change Password</span>
                        </a>

                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{route('logout')}}">
                            <i class="mdi  mdi-logout-variant mr-2"></i>
                            <span>Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </nav>
    <!--nav closed-->
</div>

<!--Horizontal-menu-->
<div class="horizontal-main hor-menu clearfix">
    <div class="horizontal-mainwrapper container clearfix">
        <nav class="horizontalMenu clearfix">
            <ul class="horizontalMenu-list">
                <li aria-haspopup="true">
                    <a href="{{route('deskOfficer.dashboard')}}" class="">
                        <i class="fa fa-dashboard"></i>
                        Dashboard
                    </a>
                </li>

                <li aria-haspopup="true">
                    <a href="#" class="sub-icon">
                        <i class="fa fa-laptop"></i>
                        Applications
                    </a>
                    <ul class="sub-menu">
                        {{--                        <li aria-haspopup="true"><a href="{{route('admin.users.add')}}">Add</a></li>--}}
                        <li aria-haspopup="true">
                            <a href="{{route('deskOfficer.applications.manage')}}">Manage</a>
                        </li>
                    </ul>
                </li>

                <li aria-haspopup="true">
                    <a href="#" class="sub-icon">
                        <i class="fa fa-laptop"></i>
                        Payments
                    </a>
                    <ul class="sub-menu">
                        {{--                        <li aria-haspopup="true"><a href="{{route('admin.users.add')}}">Add</a></li>--}}
                        <li aria-haspopup="true">
                            <a href="{{route('deskOfficer.payments')}}">Manage</a>
                        </li>
                    </ul>
                </li>

                <li aria-haspopup="true">
                    <a href="#" class="sub-icon">
                        <i class="fa fa-laptop"></i>
                        Interviews
                    </a>
                    <ul class="sub-menu">
                        {{--                        <li aria-haspopup="true"><a href="{{route('admin.users.add')}}">Add</a></li>--}}
                        <li aria-haspopup="true">
                            <a href="{{route('deskOfficer.interviews')}}">Manage</a>
                        </li>
                    </ul>
                </li>

            </ul>
        </nav>
    </div>
</div>
