@extends('deskofficer.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Dashboard</h4>
        <ol class="breadcrumb">
            {{--<li class="breadcrumb-item"><a href="#" class="text-light-color">Home</a></li>--}}
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">
        <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Assigned Applications
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">{{$allApplications}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <p class="title">Today</p>
                            <p class="data">{{$applicationsToday}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Week</p>
                            <p class="data">{{$applicationsThisWeek}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Month</p>
                            <p class="data">{{$applicationsThisMonth}}</p>
                        </div>

                    </div>
                </div>
                {{--<div class="card-footer">--}}
                    {{--<div class="float-left">--}}
                        {{--<p class="mb-0">--}}
                            {{--<span class="">--}}
                                {{--2.5%--}}
                            {{--</span>--}}
                            {{--last month--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="float-right">--}}
                        {{--<i class="fa fa-arrow-circle-o-up ml-1 text-success"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Approved
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">{{$approvedApplications}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <p class="title">Rejected Applications</p>
                            <p class="data">{{$rejectedApplications}}</p>
                        </div>

                        {{--<div class="col-4">--}}
                            {{--<p class="title">Week</p>--}}
                            {{--<p class="data">{{$usersThisWeek}}</p>--}}
                        {{--</div>--}}

                        {{--<div class="col-4">--}}
                            {{--<p class="title">Month</p>--}}
                            {{--<p class="data">{{$usersThisMonth}}</p>--}}
                        {{--</div>--}}

                    </div>
                </div>
                {{--<div class="card-footer">--}}
                    {{--<div class="float-left">--}}
                        {{--<p class="mb-0">--}}
                            {{--<span class="">--}}
                                {{--2.5%--}}
                            {{--</span>--}}
                            {{--last month--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="float-right">--}}
                        {{--<i class="fa fa-arrow-circle-o-up ml-1 text-success"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Upcoming Interviews
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">{{$allUsers}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <p class="title">Today</p>
                            <p class="data">{{$usersToday}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Week</p>
                            <p class="data">{{$usersThisWeek}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Month</p>
                            <p class="data">{{$usersThisMonth}}</p>
                        </div>

                    </div>
                </div>
                {{--<div class="card-footer">--}}
                    {{--<div class="float-left">--}}
                        {{--<p class="mb-0">--}}
                            {{--<span class="">--}}
                                {{--2.5%--}}
                            {{--</span>--}}
                            {{--last month--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="float-right">--}}
                        {{--<i class="fa fa-arrow-circle-o-up ml-1 text-success"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Functions
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">{{$allFunctions}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <p class="title">Today</p>
                            <p class="data">{{$functionsToday}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Week</p>
                            <p class="data">{{$functionsThisWeek}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Month</p>
                            <p class="data">{{$functionsThisMonth}}</p>
                        </div>

                    </div>
                </div>
                {{--<div class="card-footer">--}}
                    {{--<div class="float-left">--}}
                        {{--<p class="mb-0">--}}
                            {{--<span class="">--}}
                                {{--2.5%--}}
                            {{--</span>--}}
                            {{--last month--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="float-right">--}}
                        {{--<i class="fa fa-arrow-circle-o-up ml-1 text-success"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <!--row closed-->




    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Recent Applications</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless text-nowrap mb-0">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>ID</th>
                                <th>Function</th>
                                <th>Company</th>
                                <th>Status</th>
                                <th>Started Application</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($applications as $application)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$application->aid}}</td>
                                    <td>
                                        @foreach($application->Functions as $function)
                                            {{$function->name}},
                                        @endforeach
                                    </td>
                                    <td>James Holdings</td>
                                    <td>
                                        @if($application->status == 'pending')
                                            <span class="badge badge-warning">PENDING </span>
                                        @endif

                                        @if($application->status == 'submitted')
                                            <span class="badge badge-secondary">SUBMITTED </span>
                                        @endif

                                        @if($application->status == 'approved')
                                            <span class="badge badge-success">APPROVED </span>
                                        @endif

                                        @if($application->status == 'rejected')
                                            <span class="badge badge-danger">REJECTED </span>
                                        @endif

                                    </td>
                                    <td>{{$application->created_at->diffForHumans()}}</td>
                                    <td>
                                        <a href="{{route('deskOfficer.application.details',['$application' => $application->aid])}}">
                                            <span class="badge badge-pill badge-primary">View</span>
                                        </a>

                                    </td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row closed-->


@endsection
