@extends('deskofficer.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard.css')}}">
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard_theme_dots.css')}}">
    <style>

    </style>
@endsection

@section('scripts')
    <!--Form Wizard js-->
    <script src="{{url('admin/assets/plugins/formwizard/jquery.smartWizard.js')}}"></script>

    <script>
        (function($) {
            "use strict";

            // Toolbar extra buttons
            var btnFinish = $('<button></button>').text('Finish')
                .addClass('btn btn-success')
                .on('click', function(){ alert('Finish Clicked'); });
            var btnCancel = $('<button></button>').text('Cancel')
                .addClass('btn btn-danger')
                .on('click', function(){ $('#smartwizard').smartWizard("reset"); });


            // Smart Wizard
            $('#smartwizard').smartWizard({
                selected: 0,
                theme: 'default',
                transitionEffect:'fade',
                showStepURLhash: true,
                toolbarSettings: {
                    toolbarButtonPosition: 'end',
                    toolbarExtraButtons: [btnFinish, btnCancel]
                }
            });

            // Arrows Smart Wizard 1
            $('#smartwizard-1').smartWizard({
                selected: 0,
                theme: 'arrows',
                transitionEffect:'fade',
                showStepURLhash: false,
                toolbarSettings: {
                    toolbarExtraButtons: [btnFinish, btnCancel]
                }
            });

            // Circles Smart Wizard 1
            $('#smartwizard-2').smartWizard({
                selected: 0,
                theme: 'circles',
                transitionEffect:'fade',
                showStepURLhash: false,
                toolbarSettings: {
                    toolbarExtraButtons: [btnFinish, btnCancel]
                }
            });

            // Dots Smart Wizard 1
            $('#smartwizard-3').smartWizard({
                selected: 0,
                theme: 'dots',
                transitionEffect:'fade',
                showStepURLhash: false,
                toolbarSettings: {
                    // toolbarExtraButtons: [btnFinish, btnCancel]
                }
            });

        })(jQuery);

    </script>

@endsection

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Applications</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Applications</a></li>
        </ol>
    </div>
    <!--page-header closed-->

    <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">

        <div class="card">
            <div class="card-header">
                APPLICATION ID - #{{$application->aid}}

                @if($application->status == 'pending')
                    <span class="badge badge-warning pull-right">PENDING </span>
                @endif

                @if($application->status == 'submitted')
                    <span class="badge badge-secondary pull-right">SUBMITTED </span>
                @endif

                @if($application->status == 'approved')
                    <span class="badge badge-success pull-right">APPROVED </span>
                @endif

                @if($application->status == 'rejected')
                    <span class="badge badge-danger pull-right">REJECTED </span>
                @endif

            </div>
            <div class="card-body users row">

                <div class="col-6">
                    CREATED ON - {{$application->created_at->toDayDateTimeString()}} <br>
                    AMOUNT PAID - &#x20A6;{{number_format($application->Payment->amount,2)}} <a href="{{route('cmo.payment.details',['payment' => $application->Payment->pid])}}"  class="badge badge-primary">VIEW BREAKDOWN</a><br>
                    ASSIGNED TO -
                    @if(isset($application->assignedTo ))
                        {{$application->AssignedTo->name}}
                    @else
                        NOT ASSIGNED YET
                    @endif

                <br>

                <a href="{{route('deskOfficer.application.updates',['application' => $application->aid])}}" class="btn btn-primary">UPDATES</a>

                </div>

            </div>
        </div>


        <!--row open-->
        <div class="row">
            @foreach($application->Functions as $function)
                <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12">
                    <div class="card">
                        <div class="card-header">
                            FUNCTION - {{$function->name}}
                        </div>
                        <div class="card-body users">
                            @foreach($function->Forms as $form)

                                <div class="row form-div">
                                    <div class="col-md-8">{{$form->name}}</div>
                                    <div class="col-md-4">
                                        <a href="{{route('cmo.view.' . str_slug($form->name) , \App\applicationSubmission::where('aid',$application->aid)->where('fid',$function->fid)->where('uid',$application->uid)->where('name',$form->name)->first() )}}" class="btn btn-secondary" >VIEW</a>
                                    </div>
                                </div>

                            @endforeach

                            <b>Attached Documents</b>
                            <hr>

                            @if(count($application->FunctionDocuments($function->fid)) <= 0 ) No Documents @endif
                            @foreach($application->FunctionDocuments($function->fid) as $document)

                                <form method="post" action="{{route('deskOfficer.application.documents.review.post')}}">

                                @csrf
                                <input type="hidden" name="adid" value="{{$document->adid}}">
                                <div class="row mb-2">
                                    <div class="col-md-6">
                                        <a target="_blank" href="{{$document->url}}">
                                            {{$document->name}}
                                        </a>

                                        @if($document->status == 'pending')
                                            <span class="badge badge-warning pull-right">PENDING </span>
                                        @endif

                                        @if($document->status == 'approved')
                                            <span class="badge badge-success pull-right">APPROVED </span>
                                        @endif

                                        @if($document->status == 'rejected')
                                            <span class="badge badge-danger pull-right">REJECTED </span>
                                        @endif


                                    </div>
                                    <div class="col-md-4">
                                        @if($document->status == 'pending')
                                        <input type="text" class="form-control" name="review_message" required placeholder="Enter Review Message">
                                            @else
                                            {{$document->review_message}}
                                        @endif

                                    </div>
                                    <div class="col-md-2">
                                        @if($document->status == 'pending')
                                            <button class="badge badge-success" style="cursor: pointer;" value="approved" name="status" type="submit"><i  class="mdi mdi-check"></i> </button>
                                            <button class="badge badge-danger" style="cursor: pointer;" value="rejected" name="status" type="submit"><i class="mdi mdi-cancel"></i> </button>
                                        @endif
                                    </div>
                                </div>
                                </form>
                            @endforeach

                            @if($application->status == 'pending')

                                <br><br>
                                <b>Attach a document</b>
                                <hr>
                                <form method="post" action="{{route('cmo.applications.document.post')}}" enctype="multipart/form-data">

                                    <input type="hidden" name="fid" value="{{$function->fid}}">
                                    <input type="hidden" name='aid' value="{{$application->aid}}">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-12">
                                            <label >Document Name</label>
                                            <select class="form-control" name="name">
                                                <option>CAC Document</option>
                                                <option>CV</option>
                                            </select>
                                        </div>

                                        <div style="margin-top:10px;" class="row col-12">
                                            <div class="col-md-8">
                                                <input type="file" name="document">
                                            </div>
                                            <div class="col-md-4">
                                                <button class="btn upload btn-success"  type="submit"><i style="color: white;" class="mdi mdi-upload"></i> </button>
                                            </div>
                                        </div>

                                    </div>
                                </form>

                            @endif

                        </div>


                    </div>

                </div>
            @endforeach

        </div>
        <!--row closed-->


@endsection
