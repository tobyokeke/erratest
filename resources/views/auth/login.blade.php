<html>
<head>
    <title>Login | SECURITIES AND EXCHANGE COMMISSION</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">


    <!--Icons css-->
    <link rel="stylesheet" href="{{url('assets/css/icons.css')}}">

</head>

<body class="row">

<style>
    .input-group span {
        position: absolute;
        margin-left: 5px;
        height: 25px;
        display: flex;
        align-items: center;
    }

    .sidebar img {
        width: 100%;
        height: 100%;
    }

    h4{

        font-family: 'Poppins', sans-serif;
        text-align: center;
        font-style: normal;
        font-weight: 600;
        line-height: normal;
        font-size: 48px;
        letter-spacing: 0.004em;

        color: #1A75BC;

    }

    p.sub{
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: 600;
        line-height: normal;
        font-size: 18px;
        text-align: center;
        letter-spacing: 0.004em;

        color: #C4C4C4;
    }

    .logo{
        margin-top:10vh;
        margin-bottom: 60px !important;
    }


    .logo img{

        width: 100% !important;
        max-width: 350px;


    }

    .input-lg{
        padding:15px;
        height:72px;
        background-color: #F4F8F7;
        border:none;

    }

    input::placeholder{
        font-family: 'Poppins', sans-serif;
        font-weight: 600;
        line-height: normal;
        font-size: 22px;
        letter-spacing: 0.004em;
        color: #C4C4C4;

    }

    button{
        background: #1A75BC;
        border: 2px solid #FFFFFF  !important;
        box-sizing: border-box  !important;
        border-radius: 10px  !important;
        max-width: 300px  !important;
        margin:auto  !important;
        padding:20px !important;
    }

    h3{

        font-style: normal;
        font-weight: 600;
        font-size: 48px;
        letter-spacing: 0.004em;
        color: #FFFFFF;
        width:3vw !important;
        text-align: center !important;

    }

    .sidebar p{

        font-family: Poppins;
        font-style: normal;
        font-weight: 600;
        font-size: 22px;
        text-align: center;
        letter-spacing: 0.004em;
        color: rgba(255, 255, 255, 0.73);

    }

    .data h3{

        width: 100% !important;
    }

    p.side{
        padding: 30px;
        margin: auto;
    }

    .data{
        position: absolute;
        top:40vh;
        left:20px;
        margin:auto !important;
    }

    .sign-up-button:hover{
        background-color: transparent;
        width: 200px;
        padding: 15px;
        border: 2px solid white;
        color: white;
        border-radius: 40px;
        font-weight: 600;
    }

    .sign-up-button{
        width: 200px;
        padding: 15px;
        border: 2px solid white;
        color: white;
        border-radius: 40px;
        font-weight: 600;
    }

    .btn{
        background-color: #1A75BC !important;
    }
</style>
<div class="sidebar col-md-4 d-none d-sm-block d-md-block d-lg-block d-xl-block">

    <div class="data">
        <h3 align="center">
            New User
        </h3>

        <p class="side">
            Create your account with us and login for authentication.<br>
            <a href="{{route('register')}}" class="btn btn-outline-primary sign-up-button">REGISTER</a>

        </p>


    </div>
    <img src="{{url('imgs/signinbackground.png')}}">

</div>

<div class="main col-sm-10 col-md-8">
    <div class="col-sm-10 offset-sm-1 col-md-6  offset-md-3">

        <div class="logo" align="center">
            <img src="{{url('imgs/logolight.png')}}">
        </div>

        <h4>Sign in to portal</h4>
        <p class="sub">Enter your login details to access the portal</p>

        <form class="form" action="{{route('login')}}" method="post">

            @csrf
            <div class="form-group">
                <div class="input-group">
                <span>
                    <i class="fa fa-lock"></i>
                </span>
                    <input class="form-control input-lg" type="text" name="email" placeholder="Email">
                </div>
                @if($errors->has('email'))

                    <small id="passwordHelpBlock" class="form-text text-center text-danger">
                        {{$errors->first('email')}}
                    </small>

                @endif


            </div>

            <div class="form-group">
                <div class="input-group">
                <span>
                    <i class="fa fa-lock"></i>
                </span>
                    <input class="form-control input-lg" type="password" name="password" placeholder="Password">
                </div>
            </div>


            <button type="submit" class="btn btn-block btn-primary">Sign In</button>
        </form>

    </div>

</div>
</body>
</html>
