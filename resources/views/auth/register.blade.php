<html>
<head>
    <title>REGISTER | SECURITIES AND EXCHANGE COMMISSION</title>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.css">
    <link href="https://fonts.googleapis.com/css?family=Poppins" rel="stylesheet">


    <!--Icons css-->
    <link rel="stylesheet" href="{{url('assets/css/icons.css')}}">
    <script src="{{url('assets/js/jquery.min.js')}}"></script>
</head>

<body class="row">

<style>
    .hidden{
        display:none !important;
    }
    .input-group span {
        position: absolute;
        margin-left: 5px;
        height: 25px;
        display: flex;
        align-items: center;
    }

    .sidebar img {
        width: 100%;
        height: 100%;
    }

    h4{

        font-family: 'Poppins', sans-serif;
        text-align: center;
        font-style: normal;
        font-weight: 600;
        line-height: normal;
        font-size: 48px;
        letter-spacing: 0.004em;

        color: #1A75BC;

    }

    p.sub{
        font-family: 'Poppins', sans-serif;
        font-style: normal;
        font-weight: 600;
        line-height: normal;
        font-size: 18px;
        text-align: center;
        letter-spacing: 0.004em;

        color: #C4C4C4;
    }

    .logo{
        margin-top:10vh;
        margin-bottom: 60px !important;
    }


    .logo img{

        width: 100% !important;
        max-width: 350px;


    }

    .input-lg{
        padding:15px;
        height:72px;
        background-color: #F4F8F7;
        border:none;

    }

    input::placeholder{
        font-family: 'Poppins', sans-serif;
        font-weight: 600;
        line-height: normal;
        font-size: 22px;
        letter-spacing: 0.004em;
        color: #C4C4C4;

    }

    button{
        background: #1A75BC;
        border: 2px solid #FFFFFF  !important;
        box-sizing: border-box  !important;
        border-radius: 10px  !important;
        max-width: 300px  !important;
        margin:auto  !important;
        padding:20px !important;
    }

    h3{

        font-style: normal;
        font-weight: 600;
        font-size: 48px;
        letter-spacing: 0.004em;
        color: #FFFFFF;
        width:3vw !important;
        text-align: center !important;

    }
    .sidebar p{

        font-family: Poppins;
        font-style: normal;
        font-weight: 600;
        font-size: 22px;
        text-align: center;
        letter-spacing: 0.004em;
        color: rgba(255, 255, 255, 0.73);

    }

    .data h3{

        width: 100% !important;
    }

    p.side{
        padding: 30px;
        margin: auto;
    }

    .data{
        position: absolute;
        top:40vh;
        left:20px;
        margin:auto !important;
    }

    .sign-up-button:hover{
        background-color: transparent;
        width: 200px;
        padding: 15px;
        border: 2px solid white;
        color: white;
        border-radius: 40px;
        font-weight: 600;
    }

    .sign-up-button{
        width: 200px;
        padding: 15px;
        border: 2px solid white;
        color: white;
        border-radius: 40px;
        font-weight: 600;
    }

    .btn{
        background-color: #1A75BC !important;
    }
</style>
<div class="sidebar col-md-4 d-none d-sm-block d-md-block d-lg-block d-xl-block">

    <div class="data">
        <h3 align="center">
            New User
        </h3>

        <p class="side">
            Create your account with us and login for authentication.<br>
            <a href="{{route('login')}}" class="btn btn-outline-primary sign-up-button">LOGIN</a>

        </p>

    </div>
    <img src="{{url('imgs/signinbackground.png')}}">

</div>

<div class="main col-sm-10 col-md-8">
    <div class="col-sm-10 offset-sm-1 col-md-6  offset-md-3">

        <div class="logo" align="center">
            <img src="{{url('imgs/logolight.png')}}">
        </div>

        <h4>Sign Up to access the portal</h4>
        <p class="sub">You are registering to be a sponsored individual</p>

        @include('notification')

        <form class="form" action="{{route('register')}}" method="post">

            @csrf

            <input type="hidden" name="fname" id="fnameHidden">
            <input type="hidden" name="sname" id="snameHidden">
            <input type="hidden" name="mname" id="mnameHidden">
            <div class="form-group">
                <div class="input-group">
                <span>
                    <i class="fa fa-lock"></i>
                </span>
                    <input class="form-control input-lg" id="bvn" type="text" pattern="[0-9]+" value="{{request()->input('bvn')}}" name="bvn" placeholder="BVN">
                    <img id="loading" class="hidden" src="{{url('imgs/loading.gif')}}" style="height: 50px;">
                </div>

            </div>

            <!-- BVN VALIDATION CODE -->
            {{--<script>--}}
                {{--$(document).ready(function(){--}}

                    {{--var loading = $('#loading');--}}
                    {{--var submit = $('#submit');--}}
                    {{--$('#bvn').on('blur',function () {--}}
                        {{--console.log('blur');--}}

                        {{--loading.removeClass('hidden');--}}


                        {{--$.ajax({--}}
                            {{--url:"{{env('BVN_VERIFICATION_URL')}}" + '/' + $('#bvn').val(),--}}
                            {{--method: "get",--}}
                            {{--data:{--}}
                                {{--'seckey' : "{{env('SECRET_KEY')}}"--}}
                            {{--},--}}
                            {{--success: function (response) {--}}
                                {{--console.log(response);--}}
                                {{--loading.addClass('hidden');--}}
                                {{--submit.attr('disabled',false);--}}

                                {{--$('#fname').val(response.data.first_name);--}}
                                {{--$('#sname').val(response.data.last_name);--}}
                                {{--$('#fnameHidden').val(response.data.first_name);--}}
                                {{--$('#snameHidden').val(response.data.last_name);--}}
                                {{--$('#mnameHidden').val(response.data.middle_name);--}}
                            {{--},--}}
                            {{--error: function (error) {--}}
                                {{--loading.addClass('hidden');--}}
                                {{--console.log(error.responseJSON.message);--}}
                            {{--}--}}
                        {{--});--}}

                    {{--});--}}


                {{--});--}}
            {{--</script>--}}

            <!-- END BVN VALIDATION CODE -->

            <div class="form-group">
                <div class="input-group">
                <span>
                    <i class="fa fa-lock"></i>
                </span>
                    <input class="form-control input-lg" id="fname" type="text" value="{{request()->input('fname')}}" name="fname" placeholder="Firstname" >
                </div>
                @if($errors->has('fname'))
                    <small id="passwordHelpBlock" class="form-text text-center text-danger">
                        {{$errors->first('fname')}}
                    </small>

                @endif
            </div>

            <div class="form-group">
                <div class="input-group">
                <span>
                    <i class="fa fa-lock"></i>
                </span>
                    <input class="form-control input-lg" id="sname" type="text" value="{{request()->input('sname')}}" name="sname" placeholder="Surname" >
                </div>
                @if($errors->has('sname'))
                    <small id="passwordHelpBlock" class="form-text text-center text-danger">
                        {{$errors->first('sname')}}
                    </small>

                @endif
            </div>

            <div class="form-group">
                <div class="input-group">
                <span>
                    <i class="fa fa-lock"></i>
                </span>
                    <input class="form-control input-lg" type="text" value="{{request()->input('phone')}}" name="phone" placeholder="Phone">
                </div>
                @if($errors->has('phone'))
                    <small id="passwordHelpBlock" class="form-text text-center text-danger">
                        {{$errors->first('phone')}}
                    </small>

                @endif
            </div>

            <div class="form-group">
                <div class="input-group">
                <span>
                    <i class="fa fa-lock"></i>
                </span>
                    <input class="form-control input-lg" type="text" value="{{request()->input('email')}}" name="email" placeholder="Email">
                </div>
                @if($errors->has('email'))
                    <small id="passwordHelpBlock" class="form-text text-center text-danger">
                        {{$errors->first('email')}}
                    </small>

                @endif
            </div>

            <div class="form-group">
                <div class="input-group">
                <span>
                    <i class="fa fa-lock"></i>
                </span>
                    <input class="form-control input-lg" type="password" name="password" placeholder="Password">
                </div>
                @if($errors->has('password'))
                    <small id="passwordHelpBlock" class="form-text text-center text-danger">
                        {{$errors->first('password')}}
                    </small>

                @endif
            </div>

            <div class="form-group">
                <div class="input-group">
                <span>
                    <i class="fa fa-lock"></i>
                </span>
                    <input class="form-control input-lg" type="password" name="password_confirmation" placeholder="Confirm Password">
                </div>
                @if($errors->has('password_confirmation'))
                    <small id="passwordHelpBlock" class="form-text text-center text-danger">
                        {{$errors->first('password_confirmation')}}
                    </small>

                @endif
            </div>


            <button type="submit" id="submit"  class="btn btn-block btn-primary">Sign Up</button>
        </form>

    </div>

</div>
</body>
</html>

