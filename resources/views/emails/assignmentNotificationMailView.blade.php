You have been assigned to a new application

Application ID : {{$application->aid}}<br>
Applicant : {{$application->User->name}}<br>
Phone : {{$application->User->phone}}<br>
Email : {{$application->User->email}}<br>


Function(s) : <br>

@foreach($application->Functions as $function)
    {{$function->name}} <br>
@endforeach


Please click <a href="{{route('deskOfficer.application.details',['aid' => $application->aid])}}">here</a>
to review this application

