A document you uploaded for application #{{$document->aid}} was rejected. <br>
Please find details below : <br>

Name of Document : {{$document->name}} <br>
Uploaded on : {{$document->created_at->toDayDateTimeString()}}<br>
Message from SEC: {{$document->review_message}}<br>

Please click <a href="{{route('cmo.application.details',['aid' => $document->aid])}}">here</a> to upload the document again.
