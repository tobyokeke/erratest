<!DOCTYPE html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
    <title>Admin Dashboard | ERRA SEC</title>

    <!--Favicon -->
    <link rel="icon" href="{{url('admin/assets/img/logo.png')}}" type="image/png"/>

    <!--Bootstrap.min css-->
    <link rel="stylesheet" href="{{url('admin/assets/plugins/bootstrap/css/bootstrap.min.css')}}">

    <!--Style css-->
    <link rel="stylesheet" href="{{url('admin/assets/css/style.css')}}">

    <!--Icons css-->
    <link rel="stylesheet" href="{{url('admin/assets/css/icons.css')}}">

    <!--mCustomScrollbar css-->
    <link rel="stylesheet" href="{{url('admin/assets/plugins/scroll-bar/jquery.mCustomScrollbar.css')}}">

    <!--Sidemenu css-->
    <link href="{{url('admin/assets/plugins/horizontal-menu/dropdown-effects/fade-down.css')}}" rel="stylesheet">
    <link href="{{url('admin/assets/plugins/horizontal-menu/webslidemenu.css')}}" rel="stylesheet">

    <!--Chartist css-->
    <link rel="stylesheet" href="{{url('admin/assets/plugins/chartist/chartist.css')}}">
    <link rel="stylesheet" href="{{url('admin/assets/plugins/chartist/chartist-plugin-tooltip.css')}}">

    <!--Full calendar css-->
    <link rel="stylesheet" href="{{url('admin/assets/plugins/fullcalendar/stylesheet1.css')}}">

    <!--morris css-->
    <link rel="stylesheet" href="{{url('admin/assets/plugins/morris/morris.css')}}">

    @yield('styles')
</head>

<body class="app ">

<!--Header Style -->
<div class="wave -three"></div>

<!--loader -->
<div id="spinner"></div>

<!--app open-->
<div id="app" class="page">
    <div class="main-wrapper" >


    @include('admin.navs.top')

    <!--app-content open-->




        <div class="container content-area">
            @yield('content')
        </div>


        <!--app-content closed-->

        <!--Footer-->
        <footer class="main-footer">
            <div class="text-center">
                {{--Copyright &copy; Innovius Limited. Developed By <a href="https://innovius-group.com/">Innovius</a>--}}
            </div>
        </footer>
        <!--Footer-->

    </div>
</div>
<!--app closed-->



<!-- Back to top -->
<a href="#top" id="back-to-top" ><i class="fa fa-angle-up"></i></a>

<!-- Popup-chat -->
<a href="#" id="addClass"><i class="ti-comment"></i></a>

<!--Jquery.min js-->
<script src="{{url('admin/assets/js/jquery.min.js')}}"></script>

<!--popper js-->
<script src="{{url('admin/assets/js/popper.js')}}"></script>

<!--Bootstrap.min js-->
<script src="{{url('admin/assets/plugins/bootstrap/js/bootstrap.min.js')}}"></script>

<!--Tooltip js-->
<script src="{{url('admin/assets/js/tooltip.js')}}"></script>

<!-- Jquery star rating-->
<script src="{{url('admin/assets/plugins/rating/jquery.rating-stars.js')}}"></script>

<!--Jquery.nicescroll.min js-->
<script src="{{url('admin/assets/plugins/nicescroll/jquery.nicescroll.min.js')}}"></script>

<!--Scroll-up-bar.min js-->
<script src="{{url('admin/assets/plugins/scroll-up-bar/dist/scroll-up-bar.min.js')}}"></script>

<!--Horizontalmenu js-->
<script src="{{url('admin/assets/plugins/horizontal-menu/webslidemenu.js')}}"></script>

<!--mCustomScrollbar js-->
<script src="{{url('admin/assets/plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js')}}"></script>

<!-- jQuery Sparklines -->
<script src="{{url('admin/assets/plugins/jquery-sparkline/dist/jquery.sparkline.js')}}"></script>

<!--Jquery.knob js-->
<script src="{{url('admin/assets/plugins/othercharts/jquery.knob.js')}}"></script>

<!--Jquery.sparkline js-->
<script src="{{url('admin/assets/plugins/othercharts/jquery.sparkline.min.js')}}"></script>

<!--Chart js-->
<script src="{{url('admin/assets/js/chart.min.js')}}"></script>

<!--Dashboard js-->
<script src="{{url('admin/assets/js/dashboard4.js')}}"></script>

<!--Other Charts js-->
<script src="{{url('admin/assets/plugins/othercharts/jquery.sparkline.min.js')}}"></script>
<script src="{{url('admin/assets/js/othercharts.js')}}"></script>

<!--Sparkline js-->
<script src="{{url('admin/assets/js/sparkline.js')}}"></script>

<!--Showmore js-->
<script src="{{url('admin/assets/js/jquery.showmore.js')}}"></script>

<!--Scripts js-->
<script src="{{url('admin/assets/js/scripts.js')}}"></script>

<script src="{{url('admin/assets/plugins/accordion-Wizard-Form/jquery.accordion-wizard.min.js')}}"></script>


@yield('scripts')

<script>
    $(document).ready(function(){

        //accordion-wizard
        var options = {
            mode: 'wizard',
            autoButtonsNextClass: 'btn btn-primary float-right',
            autoButtonsPrevClass: 'btn btn-warning',
            stepNumberClass: 'badge badge-pill badge-primary mr-1',
            onSubmit: function() {
                alert('Form submitted!');
                return true;
            }
        }
        $( "#form" ).accWizard(options);

    });
</script>


</body>
</html>
