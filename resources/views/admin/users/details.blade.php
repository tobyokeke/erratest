@extends('admin.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard.css')}}">
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard_theme_dots.css')}}">
    <style>



    </style>
@endsection

@section('scripts')
    <!--Formvalidation js-->
    <script src="{{url('admin/assets/js/formvalidation.js')}}"></script>

@endsection

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Users</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Users</a></li>
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">
        <div class="col-lg-12 col-xl-6 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>User ID {{$user->uid}}</h4>
                </div>
                <div class="card-body users">
                    <div class="form-group row">
                        <label for="inputName" class="col-md-3 col-form-label bold-label">Role</label>
                        <div class="col-md-9 user-details">
                            {{strtoupper($user->role)}}
                        </div>
                    </div>

                    <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Name</label>
                            <div class="col-md-9 user-details">
                                {{$user->name}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Email</label>
                            <div class="col-md-9 user-details">
                                {{$user->email}}
                                    @if($user->isEmailVerified == 1)
                                    <span class="badge badge-success" style="float: right;">Verified</span>
                                    @else
                                    <span class="badge badge-danger" style="float: right;">Not Verified</span>
                                    @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Phone</label>
                            <div class="col-md-9 user-details">
                                {{$user->phone}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Registered</label>
                            <div class="col-md-9 user-details">
                                {{$user->created_at->toDayDateTimeString()}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Applications</label>
                            <div class="col-md-9 user-details">
                                0
                            </div>
                        </div>
                </div>
            </div>
        </div>

        <div class="col-lg-12 col-xl-6 col-md-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Edit User</h4>
                </div>
                <div class="card-body">
                    <form method="post" id="vertical-validation" action="{{route('admin.users.edit.post',['user' => $user->uid])}}" novalidate>

                        @csrf
                        <div class="">
                            <div class="form-group">
                                <div class="form-group overflow-hidden">
                                    <label>Role</label>
                                    <select name="role" class="form-control select2 w-100" >

                                        <option
                                                @if($user->role == 'administrator') selected @endif
                                              >Administrator</option>
                                        <option
                                                @if($user->role == 'cmo') selected @endif
                                        >CMO</option>
                                        <option value="deskofficer"
                                                @if($user->role == 'deskofficer') selected @endif
                                        >DESK OFFICER</option>
                                        <option
                                                @if($user->role == 'dh') selected @endif
                                        >DH</option>
                                        <option
                                                @if($user->role == 'hod') selected @endif
                                        >HOD</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" name="email" class="form-control" id="exampleInputEmail1"  value="{{$user->email}}" placeholder="Enter email" required>
                                <div class="invalid-feedback">
                                    Please Enter Valid Email Address
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="phone">Phone</label>
                                <input type="tel" name="phone" minlength="11" maxlength="11" class="form-control" id="exampleInputEmail1"  value="{{$user->phone}}" placeholder="Enter phone" required>
                                <div class="invalid-feedback">
                                    Please Enter Valid Phone Number
                                </div>
                            </div>



                        </div>
                        <button type="submit" class="btn btn-primary mt-1 mb-0">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!--row closed-->


    {{--<div class="row">--}}
        {{--<div class="col-12">--}}
            {{--<div class="card">--}}
                {{--<div class="card-header">--}}
                    {{--<h4>Dots Form Wizard</h4>--}}
                {{--</div>--}}
                {{--<div class="card-body">--}}
                    {{--<div id="smartwizard-3">--}}
                        {{--<ul>--}}
                            {{--<li><a href="#step-10">User Details</a></li>--}}
                            {{--<li><a href="#step-11">Edit User</a></li>--}}
                            {{--<li><a href="#step-12">End</a></li>--}}
                        {{--</ul>--}}
                        {{--<div>--}}
                            {{--<div id="step-10" class="">--}}

                                {{--<div class="row">--}}
                                    {{--<div class="big col-6">--}}
                                        {{--Name:--}}
                                    {{--</div>--}}
                                    {{--<div class="col-6">{{$user->name}}</div>--}}
                                {{--</div>--}}

                             {{--</div>--}}
                            {{--<div id="step-11" class="">--}}
                                {{--<form >--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label>User Name</label>--}}
                                        {{--<input type="text" class="form-control" id="inputtext" placeholder="Enter User Name">--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label>Email address</label>--}}
                                        {{--<input type="email" class="form-control" id="exampleInputEmail8" placeholder="Enter email address">--}}
                                    {{--</div>--}}
                                    {{--<div class="form-group">--}}
                                        {{--<label>Password</label>--}}
                                        {{--<input type="password" class="form-control" id="exampleInputPassword9" placeholder="Password">--}}
                                    {{--</div>--}}
                                    {{--<div class="checkbox">--}}
                                        {{--<div class="custom-checkbox custom-control">--}}
                                            {{--<input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox1">--}}
                                            {{--<label for="checkbox1" class="custom-control-label">Check me Out</label>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</form>--}}
                            {{--</div>--}}
                            {{--<div id="step-12" class="">--}}
                                {{--<div class="checkbox">--}}
                                    {{--<div class="custom-checkbox custom-control">--}}
                                        {{--<input type="checkbox" data-checkboxes="mygroup" class="custom-control-input" id="checkbox2">--}}
                                        {{--<label for="checkbox2" class="custom-control-label">I agree terms & Conditions</label>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}


@endsection
