@extends('admin.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Dashboard</h4>
        <ol class="breadcrumb">
            {{--<li class="breadcrumb-item"><a href="#" class="text-light-color">Home</a></li>--}}
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">
        <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Users
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">{{$allUsers}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <p class="title">Today</p>
                            <p class="data">{{$usersToday}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Week</p>
                            <p class="data">{{$usersThisWeek}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Month</p>
                            <p class="data">{{$usersThisMonth}}</p>
                        </div>

                    </div>
                </div>
                {{--<div class="card-footer">--}}
                    {{--<div class="float-left">--}}
                        {{--<p class="mb-0">--}}
                            {{--<span class="">--}}
                                {{--2.5%--}}
                            {{--</span>--}}
                            {{--last month--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="float-right">--}}
                        {{--<i class="fa fa-arrow-circle-o-up ml-1 text-success"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Functions
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">{{$allFunctions}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <p class="title">Today</p>
                            <p class="data">{{$functionsToday}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Week</p>
                            <p class="data">{{$functionsThisWeek}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Month</p>
                            <p class="data">{{$functionsThisMonth}}</p>
                        </div>

                    </div>
                </div>
                {{--<div class="card-footer">--}}
                    {{--<div class="float-left">--}}
                        {{--<p class="mb-0">--}}
                            {{--<span class="">--}}
                                {{--2.5%--}}
                            {{--</span>--}}
                            {{--last month--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="float-right">--}}
                        {{--<i class="fa fa-arrow-circle-o-up ml-1 text-success"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Applications
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">{{$allApplications}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <p class="title">Today</p>
                            <p class="data">{{$applicationsToday}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Week</p>
                            <p class="data">{{$applicationsThisWeek}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Month</p>
                            <p class="data">{{$applicationsThisMonth}}</p>
                        </div>

                    </div>
                </div>
                {{--<div class="card-footer">--}}
                    {{--<div class="float-left">--}}
                        {{--<p class="mb-0">--}}
                            {{--<span class="">--}}
                                {{--2.5%--}}
                            {{--</span>--}}
                            {{--last month--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="float-right">--}}
                        {{--<i class="fa fa-arrow-circle-o-up ml-1 text-success"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
        <div class="col-lg-6 col-xl-3 col-md-6 col-12">
            <div class="card dashboard-header">
                <div class="card-body text-center">
                    <p class="text-muted mb-1">
                        Revenue
                    </p>
                    <div class="row">
                        <h4 class="mt-2 mb-3">&#x20A6;{{$allPayments}}</h4>
                    </div>

                    <div class="row">
                        <div class="col-4">
                            <p class="title">Today</p>
                            <p class="data">{{$paymentsToday}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Week</p>
                            <p class="data">{{$paymentsThisWeek}}</p>
                        </div>

                        <div class="col-4">
                            <p class="title">Month</p>
                            <p class="data">{{$paymentsThisMonth}}</p>
                        </div>

                    </div>
                </div>
                {{--<div class="card-footer">--}}
                    {{--<div class="float-left">--}}
                        {{--<p class="mb-0">--}}
                            {{--<span class="">--}}
                                {{--2.5%--}}
                            {{--</span>--}}
                            {{--last month--}}
                        {{--</p>--}}
                    {{--</div>--}}
                    {{--<div class="float-right">--}}
                        {{--<i class="fa fa-arrow-circle-o-up ml-1 text-success"></i>--}}
                    {{--</div>--}}
                {{--</div>--}}
            </div>
        </div>
    </div>
    <!--row closed-->




    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card">
                <div class="card-header">
                    <h4>Recent Users</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless text-nowrap mb-0">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Name</th>
                                <th>Role</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Registered</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($users as $user)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$user->name}}</td>
                                    <td>{{$user->role}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>{{$user->created_at->diffForHumans()}}</td>
                                    <td>
                                        <a href="{{route('admin.users.details',['user' => $user->uid])}}">
                                            <span class="badge badge-pill badge-primary">View</span>
                                        </a>

                                    </td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row closed-->


@endsection
