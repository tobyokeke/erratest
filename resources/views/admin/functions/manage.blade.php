@extends('admin.layouts.app')

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Functions</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Functions</a></li>
        </ol>
    </div>
    <!--page-header closed-->



    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card">
                {{--<div class="card-header">--}}
                    {{--<h4>Recent Users</h4>--}}

                {{--</div>--}}
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless text-nowrap mb-0">
                            <thead>
                            <tr>
                                <th>S/N</th>
                                <th>Name</th>
                                <th>Required Forms</th>
                                <th>Date Created</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $count = 1; ?>
                            @foreach($functions as $function)
                                <tr>
                                    <td>{{$count}}</td>
                                    <td>{{$function->name}}</td>
                                    <td>
                                        @foreach($function->Forms as $form)
                                        {{$form->name}},
                                        @endforeach

                                    </td>

                                    <td>{{$function->created_at->diffForHumans()}}</td>
                                    <td>
                                        <a href="{{route('functions.details',['function' => $function->fid])}}">
                                            <span class="badge badge-pill badge-primary">View</span>
                                        </a>
                                    </td>
                                </tr>
                                <?php $count++; ?>
                            @endforeach

                            </tbody>
                        </table>

                        {{$functions->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--row closed-->


@endsection
