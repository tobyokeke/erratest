@extends('admin.layouts.app')

@section('styles')
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard.css')}}">
    <link rel="stylesheet" href="{{url('admin/assets/plugins/formwizard/smart_wizard_theme_dots.css')}}">
    <style>



    </style>
@endsection

@section('scripts')
    <!--Formvalidation js-->
    <script src="{{url('admin/assets/js/formvalidation.js')}}"></script>

@endsection

@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Functions</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Functions</a></li>
        </ol>
    </div>
    <!--page-header closed-->


    <!--row open-->
    <div class="row">

        <div class="col-lg-12 col-xl-12 col-md-12 col-sm-12 row">

            <div class="col-md-6 col-lg-6 col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Function - {{$function->name}}</h4>
                    </div>
                    <div class="card-body users">
                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Application Fee</label>
                            <div class="col-md-9 user-details">
                                {{strtoupper($function->applicationFee)}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Processing Fee</label>
                            <div class="col-md-9 user-details">
                                {{strtoupper($function->processingFee)}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Registration Fee</label>
                            <div class="col-md-9 user-details">
                                {{strtoupper($function->registrationFee)}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Standalone</label>
                            <div class="col-md-9 user-details">
                                @if($function->isStandalone == 1) Yes @else No @endif
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Status</label>
                            <div class="col-md-9 user-details">
                                @if($function->status == 'active')
                                    <span class="badge badge-success" >Active</span>
                                @else
                                    <span class="badge badge-danger" >Disabled</span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Created By</label>
                            <div class="col-md-9 user-details">
                                {{$function->User->name}}
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Registered</label>
                            <div class="col-md-9 user-details">
                                {{$function->created_at->toDayDateTimeString()}}
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="inputName" class="col-md-3 col-form-label bold-label">Applications</label>
                            <div class="col-md-9 user-details">
                                0
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-6 col-xl-6">
                <div class="card">
                    <div class="card-header">
                        <h4>Description</h4>
                    </div>

                    <div class="card-body users">
                        {!! $function->description !!}
                    </div>
                </div>

            </div>

        </div>

    </div>
    <!--row closed-->

@endsection
