@extends('admin.layouts.app')

@section('styles')
    <style>
        .amount{
            font-size: 60px;
            color:green;
        }
    </style>
@endsection
@section('content')

    <!--page-header open-->
    <div class="page-header">
        <h4 class="page-title">Functions</h4>
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="#" class="text-light-color">Functions</a></li>
        </ol>
    </div>
    <!--page-header closed-->



    <!--row open-->
    <div class="row">
        <div class="col-12 col-sm-12">
            @include('notification')
            <div class="card">
                <div class="card-header">
                    <h4>Add A New Function</h4>

                </div>
                <div class="card-body">
                    <p>
                        Creates a new function that can be applied for.
                    </p>


                    <form class="form-horizontal" method="post" action="{{route('functions.add.post')}}" >

                    <div class="row">


                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">Functions</div>
                                <div class="card-body">


                                        @csrf
                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label">Name</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="name" value="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label">Application Fee</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="applicationFee" value="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label">Processing Fee</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="processingFee" value="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label">Registration Fee</label>
                                            <div class="col-md-8">
                                                <input type="text" class="form-control" name="registrationFee" value="">
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label class="col-md-4 col-form-label">Description</label>
                                            <div class="col-md-8">
                                                <textarea class="form-control" rows="6" name="description"></textarea>
                                            </div>
                                        </div>

                                       <div class="form-group row">
                                           <label class="col-md-4 col-form-label">Standalone</label>
                                           <div class="col-md-8">
                                               <input type="checkbox" name="isStandalone">
                                           </div>
                                       </div>


                                </div>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="card">
                                <div class="card-header">Choose Forms Required</div>

                                <div class="card-body">

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 1</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 1">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 2</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 2">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 2A</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 2A">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 2B</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 2B">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 2C</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 2C">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 2D</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 2D">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 3</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 3">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 3A</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 3A">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 3B</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 3B">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 4</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 4">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 4A</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 4A">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 5</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 5">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 5A</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 5A">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 5B</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 5B">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 5C</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 5C">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 6</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 6">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 6A</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 6A">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 6A 1</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 6A 1">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 6A 2</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 6A 2">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 6A 3</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 6A 3">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 6A 4</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 6A 4">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 6A 5</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 6A 5">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 7</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 7">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 8</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 8">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 9A</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 9A">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM 9B</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM 9B">
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-8">SEC FORM MT1</div>
                                        <div class="col-md-2">
                                            <input type="checkbox" name="forms[]" value="SEC FORM MT1">
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <button type="submit" class="pull-right btn btn-success">Submit</button>
                        </div>

                    </div>

                    </form>


                </div>
            </div>
        </div>
    </div>
    <!--row closed-->


@endsection
