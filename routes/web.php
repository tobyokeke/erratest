<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;


Auth::routes();

Route::get('/','HomeController@dashboard');

// redirect to dashboard
Route::get('/home', function(){
    return redirect('dashboard');
})->name('home');

Route::get('dashboard','HomeController@dashboard')->name('dashboard');


Route::get('change-password','HomeController@changePassword')->name('passwords.change');
Route::get('edit-profile','HomeController@editProfile')->name('profile.edit');
Route::post('edit-profile','HomeController@postEditProfile')->name('profile.edit.post');

// administrator role
Route::group(['prefix' => 'admin'],function(){
    Route::get('dashboard','AdminController@dashboard')->name('admin.dashboard');
    Route::get('users/add','AdminController@addUser')->name('admin.users.add');
    Route::get('users','AdminController@users')->name('admin.users.manage');
    Route::get('user/{user}','AdminController@userDetails')->name('admin.users.details');
    Route::post('user/{user}','AdminController@postEditUser')->name('admin.users.edit.post');

    Route::get('functions','AdminController@functions')->name('functions.manage');
    Route::get('function/add','AdminController@addFunction')->name('functions.add');
    Route::post('function/add','AdminController@postAddFunction')->name('functions.add.post');
    Route::get('function/{function}','AdminController@functionDetails')->name('functions.details');
});



// cmo role
Route::group(['prefix' => 'cmo'],function(){
    Route::get('dashboard','CmoController@dashboard')->name('cmo.dashboard');
    Route::get('application/start','CmoController@startApplication')->name('cmo.applications.start');
    Route::get('application/add','CmoController@addApplication')->name('cmo.applications.add');
    Route::get('application/{application}/submit','CmoController@submit')->name('cmo.application.submit');

    //for applications
    Route::get('application/document/delete/{adid}','CmoController@deleteDocument')->name('cmo.applications.document.delete');
    Route::post('application/document','CmoController@postApplicationDocument')->name('cmo.applications.document.post');

    Route::get('applications','CmoController@applications')->name('cmo.applications.manage');
    Route::get('applicationA','CmoController@applications1')->name('cmo.applications.section1');
    Route::get('applicationB','CmoController@applications2')->name('cmo.applications.section2');


    Route::get('sec-form-1','FormController@secForm1')->name('cmo.applications.sec-form-1');
    Route::post('sec-form-1','FormController@postSecForm1')->name('cmo.applications.post-sec-form-1');

    Route::get('sec-form-2','FormController@secForm2')->name('cmo.applications.sec-form-2');
    Route::post('sec-form-2','FormController@postSecForm2')->name('cmo.applications.post-sec-form-2');

    Route::get('sec-form-2a','FormController@secForm2A')->name('cmo.applications.sec-form-2A');
    Route::post('sec-form-2a','FormController@postSecForm2A')->name('cmo.applications.post-sec-form-2A');

    Route::get('sec-form-2b','FormController@secForm2B')->name('cmo.applications.sec-form-2B');
    Route::post('sec-form-2b','FormController@postSecForm2B')->name('cmo.applications.post-sec-form-2B');

    Route::get('sec-form-2c','FormController@secForm2C')->name('cmo.applications.sec-form-2C');
    Route::post('sec-form-2c','FormController@postSecForm2C')->name('cmo.applications.post-sec-form-2C');

    Route::get('sec-form-2d','FormController@secForm2D')->name('cmo.applications.sec-form-2D');
    Route::post('sec-form-2d','FormController@postSecForm2D')->name('cmo.applications.post-sec-form-2D');

    Route::get('sec-form-3','FormController@SecForm3')->name('cmo.applications.sec-form-3');
    Route::post('sec-form-3','FormController@postSecForm3')->name('cmo.applications.post-sec-form-3');

    Route::get('sec-form-3a','FormController@secForm3A')->name('cmo.applications.sec-form-3A');
    Route::post('sec-form-3a','FormController@postSecForm3A')->name('cmo.applications.post-sec-form-3A');

    Route::get('sec-form-3b','FormController@secForm3B')->name('cmo.applications.sec-form-3B');
    Route::post('sec-form-3b','FormController@postSecForm3B')->name('cmo.applications.post-sec-form-3B');



    Route::get('application/view/sec-form-1/{secForm1}','ViewController@secForm1')->name('cmo.view.sec-form-1');

    Route::get('application/view/sec-form-2/{secForm2}','ViewController@secForm2')->name('cmo.view.sec-form-2');
    Route::get('application/view/sec-form-2a/{secForm2A}','ViewController@secForm2A')->name('cmo.view.sec-form-2a');
    Route::get('application/view/sec-form-2b/{secForm2B}','ViewController@secForm2B')->name('cmo.view.sec-form-2b');
    Route::get('application/view/sec-form-2d/{secForm2D}','ViewController@secForm2D')->name('cmo.view.sec-form-2d');

    Route::get('application/view/sec-form-3/{secForm3}','ViewController@secForm3')->name('cmo.view.sec-form-3');
    Route::get('application/view/sec-form-3a/{secForm3A}','ViewController@secForm3A')->name('cmo.view.sec-form-3a');
    Route::get('application/view/sec-form-3b/{secForm3B}','ViewController@secForm3B')->name('cmo.view.sec-form-3b');


    Route::get('application/start','CmoController@startApplication')->name('cmo.applications.start');
    Route::get('application/add','CmoController@addApplication')->name('cmo.applications.add');
    Route::get('application/{application}','CmoController@applicationDetails')->name('cmo.application.details');
    Route::get('payment/{payment}','CmoController@paymentDetails')->name('cmo.payment.details');
    Route::get('company/add','CmoController@addCompany')->name('cmo.companies.add');
    Route::post('company/add','CmoController@postAddCompany')->name('cmo.companies.add.post');
    Route::get('companies','CmoController@companies')->name('cmo.companies.manage');
    Route::get('company/{company}','CmoController@companyDetails')->name('cmo.companies.details');

    Route::post('pay','CmoController@pay');
    Route::post('create-application','CmoController@createApplication');

    Route::get('users','CmoController@users')->name('cmo.users');
    Route::get('user/add','CmoController@addUser')->name('cmo.users.add');
    Route::get('user/transfer','CmoController@transferUser')->name('cmo.users.transfer');
    Route::post('user/add','CmoController@postAddUser')->name('cmo.users.add.post');

});



//dh role
Route::group(['prefix' => 'dh'],function(){
    Route::get('dashboard','DhController@dashboard')->name('dh.dashboard');
    Route::get('updates','DhController@updates')->name('dh.updates');
    Route::get('applications','DhController@applications')->name('dh.applications.manage');
    Route::get('application/{application}','DhController@applicationDetails')->name('dh.application.details');
    Route::get('application/update/{application}','DhController@applicationUpdates')->name('dh.application.updates');

    Route::post('application/assign/{application}','DhController@postAssignApplication')->name('dh.applications.assign.post');
});

//hod role
Route::group(['prefix' => 'hod'],function(){
    Route::get('dashboard','CmoController@dashboard')->name('hod.dashboard');
});

//legal role
Route::group(['prefix' => 'legal'],function(){
    Route::get('dashboard','LegalController@dashboard')->name('legal.dashboard');
    Route::get('applications','LegalController@applications')->name('legal.applications.manage');
    Route::get('application/{application}','LegalController@applicationDetails')->name('legal.application.details');

});

//desk officer role
Route::group(['prefix' => 'deskofficer'],function(){
    Route::get('dashboard','DeskOfficerController@dashboard')->name('deskOfficer.dashboard');
    Route::get('applications','DeskOfficerController@applications')->name('deskOfficer.applications.manage');
    Route::get('application/{application}','DeskOfficerController@applicationDetails')->name('deskOfficer.application.details');
    Route::get('application/update/{application}','DeskOfficerController@applicationUpdates')->name('deskOfficer.application.updates');
    Route::post('application/update/{application}','DeskOfficerController@postApplicationUpdate')->name('deskOfficer.application.updates.post');
    Route::post('application/document/review','DeskOfficerController@postReviewDocument')->name('deskOfficer.application.documents.review.post');
    Route::get('payments','DeskOfficerController@payments')->name('deskOfficer.payments');
    Route::get('interviews','DeskOfficerController@interviews')->name('deskOfficer.interviews');

});


Route::get('verify-email/{email}/{token}','PublicController@verifyEmail')->name('verifyEmail');

Route::get('logout','HomeController@logout');
