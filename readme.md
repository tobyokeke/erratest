

## ERRA

Web portal for SEC Electronic Registration Returns and Analysis



## Frameworks Used
- Laravel Framework v5.8
- VueJS v2
- Php Unit

## Services Used

- Bugsnag



Maintained By Peter Olumolu and Toby Okeke


## Questions?

Please email Toby - toby.okeke@gmail.com



## License

This is property of the Securities and Exchange Commision of Nigeria.
